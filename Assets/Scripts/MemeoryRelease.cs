﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CCARPlatformFramework.CCConstants;
using CCARPlatformFramework.CCDataHandling;
using CCARPlatformFramework.CCTools;

public class MemeoryRelease : MonoBehaviour {

	// Use this for initialization

	void Start()
	{
		MemoryRelease ();
	}


	void MemoryRelease ()
	{
		CCAssetBundleManager.unLoadAllBundle ();
		Resources.UnloadUnusedAssets ();
		System.GC.Collect();


	}

	void OnDestroy()
	{
		CCAssetBundleManager.unLoadAllBundle();
		Resources.UnloadUnusedAssets();
		System.GC.Collect();
	}

}
