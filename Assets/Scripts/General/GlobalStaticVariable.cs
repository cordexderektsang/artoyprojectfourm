﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System.IO;
using CCARPlatformFramework.CCConstants;
using CCARPlatformFramework.CCDataHandling;
using CCARPlatformFramework.CCTools;

public class GlobalStaticVariable : MonoBehaviour {

	#region ScriptVariable
	public static List <CoreCustomTrackableEventHandler> AllTrackingItem = new List <CoreCustomTrackableEventHandler>();
	public static bool IsStarDiplay = true;
	public static List<ImageTargetInfor> allMarker = new List<ImageTargetInfor>();
	public static GameObject RayhitObject = null;
	public static int currentIndex = 0;
	public static List <Texture2D> listOfUiTexture  = new List<Texture2D>();
	public static bool finishDownload = false;
	public static bool isCmsDownload = false;

	#endregion

	#region ResetVariable


	public static void ResetAllGlobalVariables ()
	{
		AllTrackingItem = new List <CoreCustomTrackableEventHandler>();
		allMarker = new List <ImageTargetInfor>();
		RayhitObject = null;

	}
	#endregion

	#region FileTyes

	public const string AssetBundle_EXT = ".assetbundle";
	public const string JSON_EXT = ".json";
	public const string XML_EXT = ".xml";
	public const string DAT_EXT = ".dat";
	public const string TXT_EXT = ".txt";
	public const string CHECKSUM_EXT = ".cksum";
	public const string PNG = ".png";
	public const string JPG = ".jpg";
	public const string PHP = ".php";
	public const string Temp = "Temp";


	#endregion

	#region ParticleType


	public const string Transition = "Transition";
	public const string SpecialEffect = "SpecialEffect";


	#endregion

	#region MarkerObjectName


	public const string MarkerRenderObject = "MarkerRenderObject";
	public static string CurrentOnClickSceneName = "";


	#endregion

	public static int NumberOfItemToBeDownloadForScene = 0;
	public static int NumberOfItemToBeDownloadForAppStart = 0;

	#region MarkerFunction

	#region MarkerObjectName
	public const string rainbowElement = "RainBow(Clone)";
	public const string rainbowvuElement = "RainBowVuMarker(Clone)";

	#endregion

	/// <summary>
	/// Returns the target image tracker infor.
	/// </summary>
	/// <returns>The target image tracker infor.</returns>
	/// <param name="target">Target.</param>
	public static ImageTargetInfor ReturnTargetImageTrackerInfor (string target)
	{
		foreach (ImageTargetInfor marker in GlobalStaticVariable.allMarker)
		{
			if(marker.markerTrackableName.Equals(target))
			{
				return marker;
			}

		}
		return null;

	}

	/// <summary>
	/// Stops the extend tracking.
	/// </summary>
	public static void StopExtendTracking ()
	{

		foreach (ImageTargetInfor ImageData in GlobalStaticVariable.allMarker)
		{
			((ImageTarget)ImageData.trackableEventScript.Trackable).StopExtendedTracking();

		}

	}

	/// <summary>
	/// Starts the extend tracking.
	/// </summary>
	public static void StartExtendTracking ()
	{

		foreach (ImageTargetInfor ImageData in GlobalStaticVariable.allMarker)
		{
			((ImageTarget)ImageData.trackableEventScript.Trackable).StartExtendedTracking();

		}

	}

	#endregion

	/*public static void LoadUiBundle ()
	{
		listOfUiTexture = new List<Texture2D> ();
		Debug.Log ("load ui bundle " + LoadJson.LocalUIJsonDataReference.Count);
		foreach (UIData texture in LoadJson.LocalUIJsonDataReference) {
			Debug.Log (texture.UIPath + " : " +  texture.UIFileName );
		//	if (string.IsNullOrEmpty (texture.UIPath) && string.IsNullOrEmpty (texture.UIFileName)) {
				AssetBundle currentBuddle = CCAssetBundleManager.GetAssetBundle (texture.UIPath, texture.UIFileName);
				string[] listOfTexutre = CCAssetBundleManager.ReturnAllAssetNameInBundle (currentBuddle);
				foreach (string name in listOfTexutre) {
					Texture2D textureReference = CCAssetBundleManager.GetTexture2DFromAssetBundle (currentBuddle, name);
					listOfUiTexture.Add (textureReference);
				}
				ListArrayTexture ();
			//}
			//ListAllBundleTexture (listOfTexutre);
		}

	}
	public static void ListArrayTexture (){

		Debug.Log ("======= Array " + listOfUiTexture.Count + "=======" );
		foreach(Texture2D data in listOfUiTexture)
		{

			Debug.Log (data.name);

		}

	}

	public static Texture2D GetTextre (string target)
	{
		
		foreach(Texture2D data in listOfUiTexture)
		{
			if (data.name.Equals (target)) {

				return data;
			}

		}

		return null;
	}

	public static void ListAllBundleTexture (string[] listOfTexutre)
	{
		foreach (string name in listOfTexutre)
		{
			Debug.Log(name);
		}

	}

	public static Texture2D targetTexture (string target){



		return null;
	}*/

}
