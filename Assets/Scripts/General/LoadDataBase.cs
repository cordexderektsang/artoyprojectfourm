﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LoadDataBase : MonoBehaviour
{
	public string dataBaseURl;
	public string LanguageType;
	public const string Id = "Index Id";
	public const string Split = ":";
	public string text;
	public string[] arrayOfText;
	//public /*static*/ List<CSS> CssData;


	// Use this for initialization
	void Start()
	{
		if (!GlobalStaticVariable.isCmsDownload)
		{
			LanguageType = CoreLanguage.LanguagePref;
			if (string.IsNullOrEmpty(LanguageType))
			{
				LanguageType = CoreLanguage.DefaultLanguage;
			}

			StartCoroutine(GetDataBaseInfor());
		}
		else {
			string cmsText = WriteReadFile.ReturnTextFile();
			ReadLocalJsonForMarkerAndObject(cmsText);
		}
	}

	// Update is called once per frame

	IEnumerator GetDataBaseInfor()
	{
		string link = dataBaseURl + LanguageType;
		Debug.Log("===DataBase Link====" + link + "====");
		WWW data = new WWW(link);
		yield return data;
		text = data.text;
		text = text.TrimStart();
		//Debug.Log(text);
		ReadLocalJsonForMarkerAndObject(text);
		GlobalStaticVariable.isCmsDownload = true;
		//SplitData();
	}


	private void ReadLocalJsonForMarkerAndObject(string metadataPathForMarkerJson)
	{

		string JSONText = metadataPathForMarkerJson;
		Debug.Log(JSONText);

		if (!string.IsNullOrEmpty(JSONText))
		{
			WriteReadFile.WriteString(JSONText);
			IList JSONData = (IList)(MiniJSON.Json.Deserialize(JSONText));
			for (int o = 0; o < JSONData.Count; o++)
			{
				IDictionary markerFileData = (IDictionary)JSONData[o];
				//foreach (string data in markerFileData.Values)
				//{
					//Debug.Log():
					//CSS CssJsonData = new CSS();
					//CssJsonData.topicName = markerFileData["TopicName"].ToString();
					//CssJsonData.information = markerFileData["Information"].ToString();
					//CssData.Add(CssJsonData);
					CoreLanguage.ChangeText(markerFileData["TopicName"].ToString(), markerFileData["Information"].ToString());

				//}
	
			}


		}



	}

}

[System.Serializable]
public class CSS
{

	public string topicName;
	public string information;

}
