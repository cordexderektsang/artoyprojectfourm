﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingPageTexture : MonoBehaviour {
	public string uiName;
	private int width;
	private int height;

	// Use this for initialization
	public void CallForLoadingTextureSearchByName () {
		

			UITexture textureComponenet = GetComponent<UITexture> ();
			width = textureComponenet.width;
			height = textureComponenet.height;
			//Debug.Log ("=====" +GlobalStaticVariable.GetTextre (uiName));
			//textureComponenet.mainTexture = GlobalStaticVariable.GetTextre (uiName);
			textureComponenet.width = width;
			textureComponenet.height = height;

		
	}

	public void CallForLoadingTextureSearchByTexture (Texture  textrue) {


			UITexture textureComponenet = GetComponent<UITexture> ();
			width = textureComponenet.width;
			height = textureComponenet.height;
			textureComponenet.mainTexture = textrue;
			textureComponenet.width = width;
			textureComponenet.height = height;
	}





	

}
