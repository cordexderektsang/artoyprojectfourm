﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayBackGroundAudio : MonoBehaviour {

	private AudioSource audioSource;
	// Use this for initialization
	void Start () 
	{

		audioSource = GetComponent<AudioSource> ();
		float newValue = 	((float)CoreAudioSetting.VolumeValue) / ((float) 100);

		audioSource.volume = newValue;
		AudioClip target = AudioListController.instance.PickARandomSound(AudioListController.BackGround);
		audioSource.enabled = true;
		playOnce (target);
	
	}


	public void playOnce  (AudioClip target = null)
	{
		if (target != null) {
			audioSource.clip = target;
			audioSource.Play ();
			audioSource.loop = true;
		} else {
			Debug.Log (" no back ground musice");
		}
	
	}
	

}
