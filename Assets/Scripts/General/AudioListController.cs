﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;

public class AudioListController : MonoBehaviour {
	public List<AudioClip> clips_list = new List<AudioClip> ();
	private static AudioListController _obj;
	public static bool IsPlaying = false;
	public const string BackGround = "Background";

	private void Awake ()
	{
		CoreAudioSetting.SetUpAllAudio ();

	}

	public static AudioListController instance
	{
		get
		{
			if(_obj == null)
			{
				_obj = GameObject.Find ("AudioSourceClickEffect").GetComponent<AudioListController> ();
			}

			return _obj;
		}
		set
		{
			_obj = value;
		}
	}


	public AudioClip GetAudioClip (string name) 
	{
		AudioClip getClip = null;
		getClip = clips_list.FindAll(delegate (AudioClip clip) { return clip.name == name; })[0];
		return getClip;
	}

		
	public 	List<AudioClip> GetGroupAudioClip (string name) 
	{
		List<AudioClip> sounds = new List<AudioClip> ();
		foreach (AudioClip sound in clips_list)
		{
			string soundLenght = sound.name.Trim();
			if (soundLenght.Contains (name) ) {
				sounds.Add (sound);
			}

		}
		return sounds;
	}

	public AudioClip PickARandomSound (string name)
	{
		List<AudioClip> sounds = GetGroupAudioClip (name);
		int ran = Random.Range (0, sounds.Count);
		return sounds[ran];
	}


	public float getClipLength(string name)
	{
		AudioClip _clip = clips_list.Find(delegate (AudioClip clip) { return clip.name.Equals(name); });
		return _clip.length;
	}

	/*public void PickARandomSoundAndPlaySound (string name)
	{
		List<AudioClip> sounds = GetGroupAudioClip (name);
		int ran = Random.Range (0, sounds.Count);
		if (sounds [ran] != null) {
			IsPlaying = true;
			GetComponent<AudioSource> ().PlayOneShot (sounds [ran]);
			StartCoroutine ("countTime", sounds [ran].length);
		}
	}

	public void StopSoundAndPlaySound ()
	{

		GetComponent<AudioSource> ().Stop ();
		stopAll ();

	}

	public void playOnce (string name, AudioSource target = null)
	{
		IsPlaying = true;

			AudioClip _clip = null;
	
				// hard code demo use
	
				_clip =  GetAudioClip(name);
		if (target != null) {
			target.PlayOneShot (_clip);
		} else {
				GetComponent<AudioSource>().PlayOneShot (_clip);
				StartCoroutine ("countTime", _clip.length);
		}
	
	}

	[System.Reflection.Obfuscation(ApplyToMembers=false)]
	private IEnumerator countTime (float t)
	{
		yield return new WaitForSeconds (t);
		IsPlaying = false;
	}

	public void stopAll ()
	{
		StopCoroutine ("countTime");
		GetComponent<AudioSource>().enabled = false;
		GetComponent<AudioSource>().enabled = true;
		IsPlaying = false;
	}*/

}
