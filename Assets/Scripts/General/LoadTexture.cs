﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadTexture : MonoBehaviour {
	public string uiName;
	private int width;
	private int height;

	// Use this for initialization
	void Start () {

		UITexture textureComponenet = GetComponent<UITexture> ();
		width = textureComponenet.width;
		height = textureComponenet.height;
	//	textureComponenet.mainTexture = GlobalStaticVariable.GetTextre(uiName);
		textureComponenet.width = width;
		textureComponenet.height = height;
		
	}
	

}
