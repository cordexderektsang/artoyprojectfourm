﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class AudioPlayScript : MonoBehaviour {

	private AudioListController audioListController;
	public bool IsPlaying = false;
	private AudioSource thisAudioSource;
	public string status = "";
	public CoreCustomTrackableEventHandler coreCustomTrackableEventHandler;
	public float timeDelay = 1;


	public void Awake ()
	{

		audioListController = GameObject.Find ("AudioSourceClickEffect").GetComponent<AudioListController> ();
		thisAudioSource = GetComponent<AudioSource> ();
		SetupVolumeMute ();
		SetupBackGroundMute ();
		float backGroundValue = 	((float)CoreAudioSetting.BackGroundValue) / ((float) 100);
		float volumeValue = 	((float)CoreAudioSetting.VolumeValue) / ((float) 100);
		if (this.gameObject.tag.Equals (AudioListController.BackGround)) {
			thisAudioSource.volume = backGroundValue;
		} else {
			thisAudioSource.volume = volumeValue;
		}

		thisAudioSource.loop = true;


	}


	public void SetUpTrackComponent(CoreCustomTrackableEventHandler eventHandle)
	{


			coreCustomTrackableEventHandler = eventHandle;
			//Debug.Log("lalal comp " + coreCustomTrackableEventHandler);

	}

	public void SetupVolumeMute ()
	{

		if (CoreAudioSetting.MuteValueForVolume.Equals(ButtomFlow.Mute)) {

			CoreAudioSetting.OnOffAllSound ();
		} 


	}


	public void SetupBackGroundMute ()
	{

		if (CoreAudioSetting.MuteValueForBackGround.Equals(ButtomFlow.Mute)) {

			CoreAudioSetting.OnOffAllSound ();

		} 
	}




	public void GetAudio (string name, string status, bool isLoop)
	{
		if (name.Equals(GlobalStaticVariable.rainbowElement) || name.Equals(GlobalStaticVariable.rainbowvuElement))
		{
			if (!IsPlaying)
			{
				Debug.Log("Get Audio: " + name + status );
				List<AudioClip> sounds = audioListController.GetGroupAudioClip(name);
				Debug.Log("Get sounds : " +sounds.Count);
				AudioClip sound = ReturnStatusAudio(sounds, status);
				if (sound != null)
				{
					IsPlaying = true;
					if (isLoop)
					{
						thisAudioSource.clip = sound;
						thisAudioSource.Play();
					}
					else {
						thisAudioSource.PlayOneShot(sound);
					}
					StartCoroutine("countTime", sound.length);
				}
				else {

					stopAll();
				}
			}
		} 
		if (coreCustomTrackableEventHandler == null)
		{
			return;
		}
		if (!IsPlaying && coreCustomTrackableEventHandler.isTrack)
		{
			Debug.Log("Get Audio: " + name+status + " : isLoop " + isLoop);
			List<AudioClip> sounds = audioListController.GetGroupAudioClip(name);
			AudioClip sound = ReturnStatusAudio(sounds, status);
			if (sound != null)
			{
				IsPlaying = true;
				if (isLoop)
				{
					thisAudioSource.clip = sound;
					thisAudioSource.Play();
				}
				else {
					thisAudioSource.PlayOneShot(sound);
				}
				StartCoroutine("countTime", sound.length );
			}
			else {

				stopAll();
			}
		}
	}
		

	private AudioClip ReturnStatusAudio ( List<AudioClip> sounds , string status)
	{

		foreach (AudioClip data in sounds){
//			Debug.Log (data.name);
			if (data.name.Contains (status)) {
		

				return data;


			}

		}
		Debug.Log ("Target audio not find");
		return null;
	}

	public void SetAudioToLoops(bool isloop)
	{
		thisAudioSource.loop = isloop;

	}

	private IEnumerator countTime (float t)
	{
		yield return new WaitForSeconds (t);
		IsPlaying = false;

	}


	public void stopAll ()
	{
		if (thisAudioSource != null) {
			Debug.Log ("stop Audio For" + this.gameObject.name);
			StopCoroutine ("countTime");
			thisAudioSource.clip = null;
			thisAudioSource.enabled = false;
			thisAudioSource.enabled = true;
			IsPlaying = false;
		}
	}
}
