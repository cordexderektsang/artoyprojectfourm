﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTouch : MonoBehaviour {

	public UIRoot uiroot;
	public Camera nguiCam;
	public GameObject star;
	public float timeForStar= 0.5f;

	public AudioClip onClickEmpty_clip = null;
	public AudioClip onClickHit_clip = null;
	public AudioSource audioSoruce;

	private bool IsPlaying = false;
	// Use this for initialization
	private float countTimer = 0;
	private int screenWidth;
	private int screenHeight;
	private int uirootWidth;
	private int uirootHeight;
	private bool isOverUI = false;
	private Vector3 lastPos = Vector3.zero;
	private bool onClick= false;
	private const string block = "block";


	// Use this for initialization
	public void InputTouchStart () 
	{
		screenWidth = Screen.width;
		screenHeight = Screen.height;
		uirootWidth = uiroot.manualWidth;
		uirootHeight = uiroot.manualHeight;
		audioSoruce = GameObject.Find ("AudioSourceClickEffect").GetComponent<AudioSource>();
	}

	// Update is called once per frame
	public void InputTouchUpdate () 
	{
		if(audioSoruce == null){
			audioSoruce = GameObject.Find ("AudioSourceClickEffect").GetComponent<AudioSource>();
		}

		if (Input.GetMouseButtonDown (0) /*&& !UICamera.isOverUI*/) {
			Debug.Log (GlobalStaticVariable.RayhitObject);
			if (GlobalStaticVariable.RayhitObject != null && GlobalStaticVariable.RayhitObject.tag.Equals(block))
			{
				PlayClickSound(false);
	
			} else
			if (GlobalStaticVariable.RayhitObject == null) {
				PlayClickSound (UICamera.isOverUI);
			} else {
				PlayClickSound (true);
			}
		}
		if (Input.touchCount > 0) {
			StarEffect ();
		}

	}
	public  void playClickSound ()
	{
		audioSoruce.PlayOneShot (onClickHit_clip);
	}

	public void StarEffect ()
	{
		if (countTimer < timeForStar) {
			if (!onClick) {
				lastPos = Input.GetTouch(0).position;
				onClick = true;
			}
		countTimer += Time.deltaTime;

		} else {
			
			if(Mathf.Abs(lastPos.x - Input.GetTouch(0).position.x) > 5 || Mathf.Abs(lastPos.y - Input.GetTouch(0).position.y )> 5 ){
			GameObject referenceStar = Instantiate (star);
			referenceStar.transform.parent = uiroot.gameObject.transform;
			//	Debug.Log (ReturnNguiPosition ());
			referenceStar.transform.localPosition = ReturnNguiPosition ();
			countTimer = 0;
			}
			onClick = false;

		}

	}



	public Vector2 ReturnNguiPosition ()
	{


		Vector2 touchPosition = UICamera.lastTouchPosition;

		Vector3 mousePosition = nguiCam.ScreenToWorldPoint(Input.GetTouch(0).position);
		//Debug.Log (uiroot.manualWidth + " : " + mousePosition.x);
		return new Vector3(mousePosition.x * (uiroot.manualWidth / 1.1f), mousePosition.y * (uiroot.manualHeight / 2f));


	}




	// Update is called once per frame
	private void PlayClickSound(bool hit)
	{
		if (!IsPlaying)
		{
			IsPlaying = true;
			if (hit) {
				audioSoruce.PlayOneShot (onClickHit_clip);
				StartCoroutine ("countTime", onClickHit_clip.length);
			} else {
				audioSoruce.PlayOneShot (onClickEmpty_clip);
				StartCoroutine ("countTime", onClickEmpty_clip.length);

			}
		}
	}
	private IEnumerator countTime(float t)
	{
		yield return new WaitForSeconds(t);
		IsPlaying = false;
	}


}
