﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetDeviceRegion : MonoBehaviour {
	public static string systemLanguage = "";
	public const string defaultLanguage = "English";
	// Use this for initialization


	public static void GetDeviceLanguage () 
	{
		systemLanguage =  Application.systemLanguage.ToString ();
		Debug.Log("=======Detecting Device Language is :============= " + systemLanguage);
		if (string.IsNullOrEmpty (systemLanguage)) {
			systemLanguage = defaultLanguage;
		
		}

	}
	

}
