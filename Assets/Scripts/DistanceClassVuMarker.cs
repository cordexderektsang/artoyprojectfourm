﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;
using UnityEngine.UI;

public class DistanceClassVuMarker : MonoBehaviour {

	#region public
	public float defaultDistanceForDefaultScale = 1;// default distance for scale 1
	public GameObject rainBow;// hard core object
	//public SetUpImageTarget setUpImageTarget;



	#endregion

	#region private

	//private AlphabetCoreSystem alphabetCoreSystem;// reference script for alphabetCoreSystem
	public GameObject positionOneObject;// store current first tracked object
	public GameObject positionTwoObject;// store current second tracked object
	public GameObject cloub1;// store current first tracked object
	public GameObject cloub2;// store current second tracked object
	public CoreCustomTrackableEventHandler positionOneTrackScript;// store track event script for reference one
	public CoreCustomTrackableEventHandler positionTwoTrackScript;// store track event script for reference two
	private bool isEnable = false;// single call control for render on and off
	//private const string interaction = "Interaction";
	//private const string clear = "Clear";
	private Vector3 maxPoint;
	private Vector3 minPoint;
	private float counter;
	private float maxTime = 0.3f;
	//private GameObject empty ;
	private bool deactiveCloub = false;
	public SwitchCam switchCam;
	private bool isAudioPlayed = false;
	public AudioSource audio;
	public AudioClip sound;



	#endregion


	public void Start ()
	{

		//empty = Instantiate(new GameObject());
	}

	/// <summary>
	/// Update object position and scale base on distance
	/// </summary>
	public  void DistanaceVuMarkerUpdate ()
	{
		if (rainBow == null)
		{
			rainBow = switchCam.rainbowVu;
		}
		MarkerDistanaceCalculateFunction ();
	}
	#region call Back For Frame Id Hit


	public void ActiveRainBow ()
	{
		//SetUpTheFirstTwoTarget (GlobalStaticVariable.AllTrackingItem);	

		//	if(!deactiveCloub){
			//if(positionOneObject != null && positionTwoObject != null ){
			//if( positionOneObject.transform.childCount > 0){
			cloub1 = positionOneObject.transform.GetChild(1).gameObject;
			//}
			//if( positionTwoObject.transform.childCount > 0){

			cloub2= positionTwoObject.transform.GetChild(1).gameObject;
			//}
		//}
				cloub1.SetActive (false);
				cloub2.SetActive (false);
				//deactiveCloub = true;
			//Debug.Log("=====lalala===");
			//}
			if (!isAudioPlayed && sound != null)
			{
				audio.PlayOneShot(sound);
				isAudioPlayed = true;
			}
			
			UpdateTwoMarkersPosition ();


	}




	public void Deactive ()
	{
		positionOneObject = null;
		positionTwoObject = null;
		positionOneTrackScript = null;
		positionTwoTrackScript = null;
		SetRender (false, rainBow);
		isEnable = false;


	}

	// check whether eventhandle id same as define id
	public bool isIdRightForRainBow (){

		/*if(positionOneObject != null && positionTwoObject != null ){
			if( positionOneObject.transform.childCount > 0){
			cloub1 = positionOneObject.transform.GetChild(0).gameObject;
			}
			if( positionTwoObject.transform.childCount > 0){

			cloub2= positionTwoObject.transform.GetChild(0).gameObject;
			}
		}
		if(cloub1 == null || cloub2 == null){
			return false;
		}else */
		if (positionOneObject == null || positionTwoObject == null)
		{
			//Debug.Log("nothing");
			return false;
		}
		positionOneTrackScript = positionOneObject.GetComponent<CoreCustomTrackableEventHandler>();
		positionTwoTrackScript = positionTwoObject.GetComponent<CoreCustomTrackableEventHandler>();
		/*if(positionOneTrackScript == null || positionTwoTrackScript == null){
			Debug.Log("no script");
				return false;
		}else*/
		if (positionOneTrackScript.gameObject.name.Equals(positionTwoTrackScript.gameObject.name)){
			//Debug.Log("object equal" + positionOneTrackScript.gameObject.name + " : " + positionOneTrackScript.gameObject.name);
			return false;


		} else
		if((positionOneTrackScript.isTrack && positionTwoTrackScript.isTrack && positionTwoTrackScript.vuId == 7 &&  positionOneTrackScript.vuId == 8 )
			|| (positionOneTrackScript.isTrack && positionTwoTrackScript.isTrack && positionTwoTrackScript.vuId == 8 &&  positionOneTrackScript.vuId == 7)
			|| (positionOneTrackScript.isTrack && positionTwoTrackScript.isTrack && positionTwoTrackScript.vuId == 7 && positionOneTrackScript.vuId == 7) 
		||(positionOneTrackScript.isTrack && positionTwoTrackScript.isTrack && positionTwoTrackScript.vuId == 8 && positionOneTrackScript.vuId == 8))
		{

			return true;

		} else {
//					Debug.Log("other");
			return false;
		}

	}
	/// <summary>
	/// if word hit fail, assign the first two tracked object into reference variable for update use
	/// if 5 castle
	/// if 2 scale with rainbow
	/// </summary>
	/// <param name="_list">List.</param>
	public void MarkerDistanaceCalculateFunction ()
	{
		if (rainBow == null)
		{
			return;
		}
	
		if ( isIdRightForRainBow()) {
	
			ActiveRainBow ();
		} else {
	
				//empty.transform.parent = null;
				rainBow.transform.parent = null;
				SetRender (false, rainBow);
				if (cloub1 != null ) {
					cloub1.SetActive (true);
					cloub1.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
					
				}
				if( cloub2 != null){
					cloub2.SetActive (true);
					cloub2.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
					
				}
				cloub1 = null;
				cloub2 = null;
				deactiveCloub = false;
			if (isAudioPlayed)
			{
				audio.Stop();
				audio.clip = null;
				audio.enabled = false;
				audio.enabled = true;
				isAudioPlayed = false;
			}
			
			
		}
			
	}
		

	public bool Untrack ()
	{

		if (GlobalStaticVariable.AllTrackingItem.Count > 2) {
			return false;
			counter = 0;
		} else {
			if (counter < maxTime) {
				
				counter += Time.deltaTime;
				return false;
			} else {
				return true;
			}
		}
	}

	#endregion

	
	



	public void unAttachRainbow ()
	{

		rainBow.transform.parent = null;
		//empty.transform.parent = null;

	}

	

	#region core Position Update Fucntion



	/// <summary>
	/// Update Object position and scale base on two markers distance
	/// as well as disable render base on current for those two markers
	/// </summary>
	private void UpdateTwoMarkersPosition ()
	{

		if ((positionOneTrackScript.isTrack && positionTwoTrackScript.isTrack) /*|| (positionOneTrackScript == null && positionTwoTrackScript == null)*/) {

			Transform leftM = null;
			Transform rightM = null;
			if (positionOneObject.transform.position.x < positionTwoTrackScript.transform.position.x) 
			{
				leftM = positionOneTrackScript.gameObject.transform;
				rightM = positionTwoTrackScript.gameObject.transform;
			} else {

				rightM = positionOneTrackScript.gameObject.transform;
				leftM = positionTwoTrackScript.gameObject.transform;
			}

		//	Debug.Log ("rightM " + rightM.transform.localPosition.ToString ("f4"));
			//Debug.Log ("leftM " + leftM.transform.localPosition.ToString ("f4"));
			rainBow.transform.parent = leftM;
			float distance = GetDistance (positionOneObject, positionTwoObject);
			//float distanceX =GetDistanceX (positionOneObject, positionTwoObject);
			//float distanceY =GetDistanceY (positionOneObject, positionTwoObject);
			//float distanceZ =GetDistanceZ (positionOneObject, positionTwoObject);
			//float dX = leftM.transform.position.x - rightM.transform.position.x;
			//float dZ = leftM.transform.position.z - rightM.transform.position.z;
			//float dY = leftM.transform.position.y - rightM.transform.position.y;
			//float angleX = Mathf.Rad2Deg*Mathf.Atan(distanceY/distanceX);
			//float angleX = Mathf.Rad2Deg * Mathf.Atan2(dX, dY);
			//float angleX = Mathf.Rad2Deg*Mathf.Atan(dY/dX)*-1;
			//float offsetAngleX = 13;        
			//angleX = (angleX - offsetAngleX) * -1;

			//vectors = new Vector3[2];
			//vectors [0] = positionOneObject.transform.position;
			//vectors [1] = positionTwoObject.transform.position;
			//Vector3 centerPoint = CenterOfVectors (vectors);
			//Vector3 offSetcenterPoint = new Vector3 (centerPoint.x,centerPoint.y,centerPoint.z);
			float scale = GetScale (distance)/25f;
			Debug.Log(scale + " : " + distance); 
			//rainBow.transform.localPosition = offSetcenterPoint;
			//Vector3 parentRotation = leftM.transform.localEulerAngles;
			//rainBow.transform.eulerAngles =  positionOneObject.transform.eulerAngles;
			//rainBow.transform.localEulerAngles = new Vector3(0, angleX ,0);
			
		
			//empty.transform.parent = rainBow.transform.parent;
			//empty.transform.localPosition = rainBow.transform.localPosition;
			//empty.transform.localEulerAngles = rainBow.transform.localEulerAngles;
			rainBow.transform.LookAt (rightM.transform);
			//rainBow.transform.localRotation = new Quaternion.Euler(0, empty.transform.localEulerAngles.y,empty.transform.localEulerAngles.z); 
			
			//rainBow.transform.localEulerAngles = new Vector3(0,empty.transform.localEulerAngles.y,empty.transform.localEulerAngles.z);
			//if (leftM.transform.rotation.x > 0 && rightM.transform.rotation.x > 0) {

			//	rainBow.transform.eulerAngles = new Vector3 (rainBow.transform.eulerAngles.x,rainBow.transform.eulerAngles.y,rainBow.transform.eulerAngles.z+180);
			//} else {
				//rainBow.transform.eulerAngles = new Vector3 (rainBow.transform.eulerAngles.x,rainBow.transform.eulerAngles.y,rainBow.transform.eulerAngles.z+180);
			//}
			//rainBow.transform.eulerAngles = new Vector3(-90, 0 ,90+angleX);
			//rainBow.transform.localEulerAngles =  new Vector3(0,angleX,0);
			//Quaternion q = new Quaternion();
			//q.SetLookRotation(rainBow.transform.position, rightM.position);
			//rainBow.transform.rotation=q;

			rainBow.transform.localScale = new Vector3 (scale, scale, scale);
			rainBow.transform.localPosition = Vector3.zero;
		

			if (!IsRenderOn(rainBow)) 
			{
				SetRender (true,rainBow);
				isEnable = true;
			}
		} else {
			if (IsRenderOn(rainBow))
			{
	
				SetRender (false, rainBow);
				isEnable = false;
			}
		
		}

	}
	#endregion





	#region Math Calculateion


	/// <summary>
	/// Gets the distance between two objects
	/// </summary>
	/// <returns>The distance.</returns>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	private float GetDistanceX (GameObject a , GameObject b)
	{

		float distance = a.transform.position.x - b.transform.position.x;
		return distance;

	}

	/// <summary>
	/// Gets the distance between two objects
	/// </summary>
	/// <returns>The distance.</returns>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	private float GetDistanceY (GameObject a , GameObject b)
	{

		float distance = a.transform.position.y - b.transform.position.y;
		return distance;

	}

	/// <summary>
	/// Gets the distance between two objects
	/// </summary>
	/// <returns>The distance.</returns>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	private float GetDistanceZ (GameObject a , GameObject b)
	{

		float distance = a.transform.position.z - b.transform.position.z;
		return distance;

	}

	/// <summary>
	/// Gets the distance between two markers
	/// </summary>
	/// <returns>The distance.</returns>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	private float GetDistance (CoreCustomTrackableEventHandler a , CoreCustomTrackableEventHandler b)
	{
		float distance = Vector3.Distance(a.gameObject.transform.position, b.gameObject.transform.position);
		return distance;

	}




	/// <summary>
	/// Gets the distance between two objects
	/// </summary>
	/// <returns>The distance.</returns>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	private float GetDistance (GameObject a , GameObject b)
	{
		//Debug.Log ("a : " + a.transform.position.ToString()  + " b : " + b.transform.position.ToString());
		float distance = Vector3.Distance(a.transform.position, b.transform.position);
		return distance;

	}

	/// <summary>
	/// Gets the scale aspect from distance
	/// </summary>
	/// <returns>The scale.</returns>
	/// <param name="Distance">Distance.</param>
	private float GetScale (float Distance)
	{
		float scaleAspect = Distance / defaultDistanceForDefaultScale;
		return scaleAspect;
	}

	/// <summary>
	/// get center position between two markers
	/// </summary>
	/// <returns>The of vectors.</returns>
	/// <param name="vectors">Vectors.</param>
	private Vector3 CenterOfVectors( Vector3[] vectors )
	{
		Vector3 sum = Vector3.zero;
		if( vectors == null || vectors.Length == 0 )
		{
			return sum;
		}

		foreach( Vector3 vec in vectors )
		{
			sum += vec;
		}
		return sum/vectors.Length;
	}




	#endregion

	#region Render
	/// <summary>
	/// Sets the render for object.
	/// </summary>
	/// <param name="status">If set to <c>true</c> status.</param>
	/// <param name="thisObject">This object.</param>
	private void SetRender (bool status ,GameObject thisObject)
	{
		Renderer[] rendererComponents = thisObject.GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = thisObject.GetComponentsInChildren<Collider>(true);
		Animation[] animationComponents = thisObject.GetComponentsInChildren<Animation>(true);

		foreach (Renderer component in rendererComponents)
		{
			component.enabled = status;
		}

		// Disable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = status;
		}

	}

	/// <summary>
	/// Determines whether this object is render on or not.
	/// </summary>
	/// <returns><c>true</c> if this instance is render on the specified thisObject; otherwise, <c>false</c>.</returns>
	/// <param name="thisObject">This object.</param>
	private bool IsRenderOn (GameObject thisObject)
	{
		Renderer[] rendererComponents = thisObject.GetComponentsInChildren<Renderer>(true);
		foreach (Renderer component in rendererComponents)
		{
			return component.enabled ;
		}
		return false;
	}
	#endregion

}
