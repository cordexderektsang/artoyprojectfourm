﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingBar : MonoBehaviour {

	#region menu download Item

	private bool menuDownLoadStart = false;
	private bool appDownLoadStart = false;
	private bool appSceneLoadStart = false;

	private int width; 
	public float downloadProgress = 0;
	public float downloadProgressForSceneLoad = 0;
	public float downloadProgressForAppStart = 0;
	public bool isDownloadLanguage = false;
	public UILabel labelShowsLanguageDownloadProgess;
	private bool startDownload = true;
	private bool startDownloadForAppStart = true;
	private bool startLoadSceneStart = true;

	private string resourcePath = "download button";
	private string fileName = "btn_DL_";
	public UITexture playEmulation;
	private const int playEmulationNumberOfPicture = 34;
	private int start = 0;
	private int end = 1;
	private float remainder = 0;

	#endregion


	#region download loading  Item

	public UIPanel panel;
	private Vector2 widthPanel;
	private const int sizePanel = 1121;
	private const int startPanel = -858;
	private int gap;

	#endregion


	#region download loading  Item

	public LoadingPageTexture backgroundLoadingPage;
	public LoadingPageTexture middlePictureLoadingPage;

	#endregion
	// Use this for initialization
	void Start () {

		downloadProgress = 0;
		downloadProgressForAppStart = 0;
		downloadProgressForSceneLoad = 0;
		widthPanel = panel.GetViewSize();
		setPanelRect(startPanel);
		gap = sizePanel - startPanel;
		
	}


	/// <summary>
	/// reset Loading bar to start
	/// </summary>
	public void resetLoadingPage ()
	{
		setPanelRect (startPanel);

	}

	public void FinishLoadingPage (){


		setPanelRect (sizePanel);
	}

	public void setPanelRect (float x)
	{

		panel.SetRect (x, 1, widthPanel.x, 32);

	}


	public void ChangeTextureForBackGroundLoadingPage (string name)
	{
		backgroundLoadingPage.uiName = name;
		backgroundLoadingPage.CallForLoadingTextureSearchByName ();

	}

	public void ChangeTextureForImageLoadingPageByName (string name)
	{
		middlePictureLoadingPage.uiName = name;
		middlePictureLoadingPage.CallForLoadingTextureSearchByName ();

	}

	public void ChangeTextureForImageLoadingPageByTexture (Texture texture)
	{
		middlePictureLoadingPage.CallForLoadingTextureSearchByTexture (texture);

	}


#region appSceneLoadingUX

	/*public void wwwProgressForLoadScene (float progress)
	{

		float addInFactor =  (float)gap / (float)GlobalStaticVariable.NumberOfItemToBeDownloadForScene;
		float addOn = addInFactor * downloadProgressForSceneLoad;
		if (progress == 0) {
			startLoadSceneStart = true;
		} 

		else
			if (startLoadSceneStart && progress != 0 && progress != 1) {
				float progressConvert = (addInFactor * progress)+ addOn ;
				Debug.Log ("Progress : " + progressConvert +  "addInFactor : " +  addInFactor + " addOn " + addOn);
				float moveValue = startPanel + progressConvert;
				setPanelRect (moveValue);

			} else
				if (progress == 1) {
					if (!startLoadSceneStart) {
						float progressConvert = (addInFactor * progress)+ addOn ;
						Debug.Log ("Progress : " + progressConvert +  "addInFactor : " +  addInFactor + " addOn " + addOn + " lalalala");
						float moveValue = startPanel + progressConvert;
						setPanelRect (moveValue);

					}
					downloadProgressForSceneLoad += 1;
					startLoadSceneStart = false;
					float value = downloadProgressForSceneLoad * addInFactor;
					Debug.Log (" end :  " + value + "downloadProgressForAppStart"  + downloadProgressForSceneLoad + " gap " + gap);

				}
		Debug.Log ("LoadScene : " + GlobalStaticVariable.NumberOfItemToBeDownloadForScene + " progress "  + progress  + "downloadProgressForAppStart " + downloadProgressForSceneLoad);
		if (downloadProgressForSceneLoad == GlobalStaticVariable.NumberOfItemToBeDownloadForScene) {
			setPanelRect (sizePanel);
			this.gameObject.SetActive (false);

		}

	}*/


#endregion

#region appStartLoadingUX

	public void wwwProgressForAppStart (float progress ,bool isLocal = false)
	{
		//Debug.Log ("==" + progress);
		if (appDownLoadStart) {
			//Debug.Log (progress);
			/*if (progress == -1) {

				downloadProgressForAppStart -= 1;
			}*/
			float addInFactor = (float)gap / (float)GlobalStaticVariable.NumberOfItemToBeDownloadForAppStart;
		//	Debug.Log ("progress : " + downloadProgressForAppStart + " :gap " + gap + " : factor " + addInFactor);
			float addOn = addInFactor * downloadProgressForAppStart;
			if (progress == 0) {
				startDownloadForAppStart = true;
			} else if (startDownloadForAppStart && progress != 0 && progress != 1) {
				float progressConvert = (addInFactor * progress) + addOn;
				//	Debug.Log ("Progress : " + progressConvert +  "addInFactor : " +  addInFactor + " addOn " + addOn);
				float moveValue = startPanel + progressConvert;
				//Debug.Log ("progress : " + progressConvert + " :addOn " + addOn + " : factor " + addInFactor);
				setPanelRect (moveValue);
		
			} else if ((progress == 1 && startDownloadForAppStart) || progress == 1 && isLocal) {
				//if (startDownloadForAppStart) {
					float progressConvert = (addInFactor * progress) + addOn;
					//Debug.Log ("Progress : " + progressConvert +  "addInFactor : " +  addInFactor + " addOn " + addOn + " lalalala");
					float moveValue = startPanel + progressConvert;
					setPanelRect (moveValue);

				//}
				downloadProgressForAppStart += 1;
				Debug.Log ("downloadProgressForAppStart" + downloadProgressForAppStart + " : total Item  " +  GlobalStaticVariable.NumberOfItemToBeDownloadForAppStart);
				startDownloadForAppStart = false;
				//float value = downloadProgressForAppStart * addInFactor;
				//Debug.Log (/*" end :  " + value +*/ "downloadProgressForAppStart"  + downloadProgressForAppStart );

			}

			//Debug.Log ("appStart : " + GlobalStaticVariable.NumberOfItemToBeDownloadForAppStart + " progress "  + progress  + "downloadProgressForAppStart " + downloadProgressForAppStart);
			if (downloadProgressForAppStart == GlobalStaticVariable.NumberOfItemToBeDownloadForAppStart) {
				setPanelRect (sizePanel);
				appDownLoadStart = false;
				//this.gameObject.SetActive (false);
				//StartCoroutine(CloseAppStartLoadingPage());

			}
		}

	}

	private IEnumerator CloseAppStartLoadingPage ()
	{
		yield return new WaitForSeconds (2f);
		this.gameObject.SetActive (false);
	}
		

#endregion

	public void wwwProgress(float progress ,string url)
	{
		//if (menuDownLoadStart) {

		//DownloadProgressForMenu (progress);
		//Debug.Log("download");
		//} else 	if (appDownLoadStart){
		if (!isDownloadLanguage)
		{
			wwwProgressForAppStart(progress);
		}
		else {
			float downprogressForLabel = progress * 100;
	
			labelShowsLanguageDownloadProgess.text = downprogressForLabel.ToString("F1")+"%";
		}


		///}
		//Debug.Log (progress +" : " + url);

	}

#region appSceneDownloadLoadingUX
	/*private void DownloadProgressForMenu (float progress)
	{

		//Debug.Log (progress);
		if(progress == 1 && startDownload){
			downloadProgress += 1;
			startDownload = false;
			float addInFactor =(float) playEmulationNumberOfPicture / (float)GlobalStaticVariable.NumberOfItemToBeDownloadForScene;
			float value = addInFactor * downloadProgress;
			remainder += addInFactor - (int)addInFactor;
			value  = (int)value + remainder;
			Debug.Log ("Finish : " + value + " remainder " + remainder );
			if(value >= playEmulationNumberOfPicture){
				value = playEmulationNumberOfPicture;
			}
			ResourceLoadTexture ((int)value);
		}
		if(progress == 0 && !startDownload){
			Debug.Log ("Start");
			startDownload = true;
		}

	}

	public void LocalProgressForMenu(int progressValue)
	{

				downloadProgress += progressValue;

				float addInFactor =(float) playEmulationNumberOfPicture / (float)GlobalStaticVariable.NumberOfItemToBeDownloadForScene;
				float value = addInFactor * downloadProgress;
				remainder += addInFactor - (int)addInFactor;
				value  = (int)value + remainder;
				Debug.Log ("Finish : " + value + " remainder " + remainder );
				ResourceLoadTexture ((int)value);

	}*/

	public void ResourceLoadTexture (int percentage)
	{
		int ConvertedPercentage = 0;
		if (percentage < playEmulationNumberOfPicture) {
			ConvertedPercentage = percentage;
		} else {
			ConvertedPercentage = playEmulationNumberOfPicture;
		}
			string picturefileName = fileName + ConvertedPercentage;
			string path = resourcePath + "/" + picturefileName;
			playEmulation.mainTexture = Resources.Load (path)as Texture2D;


	}
#endregion

	public void TotalProgressForSceneDownload ()
	{
		//GlobalStaticVariable.NumberOfItemToBeDownloadForScene = 0;
		//GlobalStaticVariable.NumberOfItemToBeDownloadForScene += LoadJson.LocalMarkerJsonDataReference.Count;
		//GlobalStaticVariable.NumberOfItemToBeDownloadForScene  += LoadJson.LocalObjectJsonDataReference.Count;
	//	GlobalStaticVariable.NumberOfItemToBeDownloadForScene  += LoadJson.LocalSceneJsonDataReference.Count;
		//GlobalStaticVariable.NumberOfItemToBeDownloadForScene  += LoadJson.LocalAudioJsonDataReference.Count;
		//GlobalStaticVariable.NumberOfItemToBeDownloadForScene  += LoadJson.LocalUIJsonDataReference.Count;
		//Debug.Log ("Total Download Item : " +  	GlobalStaticVariable.NumberOfItemToBeDownloadForScene);
		//menuDownLoadStart = true;
		playEmulation.gameObject.SetActive (true);
	}

	public void TotalProgressForAppStartDownload ()
	{
		GlobalStaticVariable.NumberOfItemToBeDownloadForAppStart = 0;
		GlobalStaticVariable.NumberOfItemToBeDownloadForAppStart += LoadJson.LocalLanguageJsonDataReference.Count;
		//GlobalStaticVariable.NumberOfItemToBeDownloadForAppStart += LoadJson.LocalMarkerJsonDataReference.Count;
		GlobalStaticVariable.NumberOfItemToBeDownloadForAppStart  += LoadJson.LocalObjectJsonDataReference.Count;
		//GlobalStaticVariable.NumberOfItemToBeDownloadForAppStart  += LoadJson.LocalUIJsonDataReference.Count;
		GlobalStaticVariable.NumberOfItemToBeDownloadForAppStart += 1; //for  device default language Download
		//GlobalStaticVariable.NumberOfItemToBeDownloadForAppStart  += LoadJson.LocalMenuJsonDataReference.Count;
		Debug.Log(" Object " +LoadJson.LocalObjectJsonDataReference.Count + " UIJson " +LoadJson.LocalUIJsonDataReference.Count + " language " + LoadJson.LocalLanguageJsonDataReference.Count + " marker " + LoadJson.LocalMarkerJsonDataReference.Count);

		Debug.Log ("Total Download Item : " +  		GlobalStaticVariable.NumberOfItemToBeDownloadForAppStart );
		appDownLoadStart = true;
	}


	public void TurnOffLoadingPage (){

		this.gameObject.SetActive (false);
	}

}
