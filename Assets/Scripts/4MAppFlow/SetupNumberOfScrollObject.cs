﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SetupNumberOfScrollObject : MonoBehaviour {

	public bool isLocal = true;
	public Transform menu_wrapper;
	public GameObject objectSpawn;
	public ButtomFlow buttomFlowScript;
	// Use this for initialization

	void Awake (){
		if (!isLocal) {
			//LoadJson.menuUIfunction += GenerateSceneSelectionForScroll;
		} 
	}

	void GenerateSceneSelectionForScroll ()
	{
		/*Debug.Log (" GenerateSceneSelectionForScroll " + LoadJson.LocalMenuJsonDataReference.Count);
		foreach(MenuJsonData data in LoadJson.LocalMenuJsonDataReference)
		{
			GameObject image = Instantiate (objectSpawn);
			image.transform.parent = menu_wrapper;
			image.transform.localPosition = new Vector3 (0,0,-100);
			image.transform.localScale = new Vector3 (1,1,1);
			image.tag = TouchController.MenuWrapper;
			UITexture texture = image.GetComponent<UITexture> ();
			Debug.Log (data.pictureName + data.texturePath);
			string DirectionaryPath = data.texturePath + data.textureFileName;
			Debug.Log ("picture " + data.pictureName);
			Debug.Log ("scene name "+ data.sceneName);
			texture.mainTexture = LoadPNG(DirectionaryPath);
			texture.width = data.pictureWidth;
			texture.height = data.pictureHeight;
			if (data.available == true) {
				Debug.Log ( data.sceneName + ": is Available" + data.available);
				image.GetComponent<BoxCollider> ().enabled = true;
				UIButton button = image.GetComponent<UIButton>();
				button.enabled = false;
				button.enabled = true;
				CoreNguiButtonAddAndRemove.AddParameterFunction (buttomFlowScript, new object[] {button}, button, "OnClickGamePlay");
			} else {
				image.GetComponent<BoxCollider> ().enabled = false;
				Debug.Log ( data.sceneName + ": not Available" + data.available);

			}
		//	if (!string.IsNullOrEmpty (data.jsonFile)) {
				//image.name = data.jsonFile;
			image.name = data.sceneName;
			//}
			image.GetComponent<UITexture> ().width = data.pictureWidth;
			image.GetComponent<UITexture> ().height = data.pictureHeight;
		

		}
		menu_wrapper.GetComponent<ScaleScroller> ().OrganisingPosition();*/
	}


	void OnDestroy() 
	{
			//LoadJson.menuUIfunction -= GenerateSceneSelectionForScroll ;
	}


	public static Texture2D LoadPNG(string filePath) {

		if (File.Exists(filePath)) {   
			byte[] fileData = File.ReadAllBytes(filePath);
			Texture2D tex = new Texture2D(2, 2, TextureFormat.RGBA32, false);
			tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
			return tex;
		}
		Debug.Log ("Load PNG  But Picture Path not Exist" + filePath);
		return null;
	}




}
