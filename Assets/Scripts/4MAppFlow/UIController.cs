﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class UIController : MonoBehaviour {
	public GameObject loadingPage; 
	public UISprite displayConfirmPageUi;
	public GameObject frontPage;
	public GameObject loadingBar;
	public EveryplayCoreFunction everyplayCoreFunction;
	public GameObject photoPreview;
	public UILabel displayNumberOfTracker;
	private const string scene = "4mAppFlow";
	private const string switchToPhotoUIName = "btn_AR_switch_photo";
	private const string switchToVideoUIName ="btn_AR_switch_record";
	private const string switchToPhoto = "btn_AR_PhotoCam";
	private const string switchToStartVideo ="btn_AR_record_start";
	private const string switchToStopVideo = "btn_AR_record_stop";
	private const string videoToConfirmPage = "UI_capture_video";
	private const string camToConfirmPage = "UI_capture_photo";
	private UIButton parentbuttonR;
	private UISprite parenttargetR;
	private UIButton buttonR;
	private UISprite targetR;

	public UISprite spriteForPreview;
	public UIButton buttonForPreview;
	public const string NameForPhotoCapture = "btn_game_photoDL";
	public const string NameForVideoCapture = "btn_AR_done";

	public List<GameObject> allUiForTurnOff = new List<GameObject>();

//	public List<Texture2D> listofTextureTochange = new List<Texture2D>();

	// Use this for initialization

	public void FrontPage ()
	{
		StartCoroutine (WaitForFrontPage());
	}
	public IEnumerator WaitForFrontPage ()
	{

		yield return new WaitForSeconds (2);
		loadingPage.SetActive (false);
		frontPage.SetActive (true);

	}

	public void UIUpdateFunction ()
	{
		displayNumberOfTracker.color = Color.white;
		displayNumberOfTracker.text = GlobalStaticVariable.AllTrackingItem.Count.ToString() + "/6" ;
		if (GlobalStaticVariable.AllTrackingItem.Count == 6) {
			displayNumberOfTracker.color = Color.red;

		}
	}

	public void TurnOnOffAllUI (bool status)
	{


		foreach (GameObject ui in allUiForTurnOff)
		{
			ui.SetActive (status);
		}
	}

	public void BackToMenu ()
	{
		StartCoroutine (SceneChange());

	}

	public IEnumerator SceneChange ()
	{
		Resources.UnloadUnusedAssets ();
		System.GC.Collect ();
		loadingBar.SetActive (false);
		loadingPage.SetActive (true);
		frontPage.SetActive (false);
		yield return new WaitForSeconds (1);
		SceneManager.LoadScene(scene);
	}

	public void OnClickSwitchPhotoAndVideo (UISprite target, UIButton button,UISprite parent, UIButton parentbutton )
	{
		parentbuttonR = parentbutton;
		parenttargetR = parent;
		buttonR = button;
		targetR= target;
		Debug.Log (target.spriteName + " : " + switchToStartVideo);
		if (parent.spriteName.Equals (switchToStartVideo)) 
		{
			target.spriteName = switchToPhotoUIName;
			button.normalSprite = switchToPhotoUIName;
			everyplayCoreFunction.mode = 0;
			parent.spriteName  = switchToPhoto; ;
			parentbutton.normalSprite  = switchToPhoto;

		} else {

			target.spriteName =  switchToVideoUIName;
			button.normalSprite = switchToVideoUIName;
			everyplayCoreFunction.mode = 1;
			parent.spriteName  =  switchToStartVideo ;
			parentbutton.normalSprite = switchToStartVideo;

		}
	}
	public void SwitchToVideo ()
	{
		Debug.Log ("here Start");
		parentbuttonR.normalSprite = switchToStartVideo;
		targetR.spriteName = switchToStartVideo;
		OnOffMode(true);

	}

	public void SwitchToStopMode ()
	{
		Debug.Log ("here STOp" +parentbuttonR.gameObject.name);
		parentbuttonR.normalSprite = switchToStopVideo;
		targetR.spriteName = switchToStopVideo;
		OnOffMode(false);

	}


	public void OnOffMode (bool onOff)
	{
		buttonR.gameObject.SetActive (onOff);
	
	}

	public void OnOffPhotoPreview (bool status = false)
	{
		photoPreview.SetActive (status);
		if(status){
			frontPage.SetActive (false);
		}

	}

	public void OnOffPhotoPreviewAndFrontPage ()
	{
		photoPreview.SetActive (false);
		frontPage.SetActive (true);

	}

	public void SwitchUiForConfirmpage(int mode)
	{


		/*if (mode == 0) {
			spriteForPreview.spriteName = NameForPhotoCapture;
			buttonForPreview.normalSprite = NameForPhotoCapture;
		} else {
			spriteForPreview.spriteName = NameForVideoCapture;
			buttonForPreview.normalSprite = NameForVideoCapture;
		}*/
	}

	/*public Texture2D ReturnTargetTexture (string name){

		return GlobalStaticVariable.GetTextre (name);

	}*/




}
