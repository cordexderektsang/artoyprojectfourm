﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonCheckToBeAvaliable : MonoBehaviour 
{
	public ScaleScroller scaleScroller;
	public GameObject playButton;
	public Transform menuWrapper;

	/// <summary>
	///For Front Page
	/// to disable and Enable play button base on current scrolled index
	/// if the boxCollider for current is not on then play button will off otherwise on
	/// </summary>
	public void ButtonCheckFunction ()
	{
		int value = scaleScroller.GetCurrentIndex ();
		bool currentBoxColliderIsEnable = menuWrapper.GetChild (value).GetComponent<BoxCollider> ().enabled;
		if ( !currentBoxColliderIsEnable) {
			playButton.SetActive (false);

		} else if(currentBoxColliderIsEnable && ButtomFlow.isGameReady)
		{
			playButton.SetActive (true);
		}

	}
}
