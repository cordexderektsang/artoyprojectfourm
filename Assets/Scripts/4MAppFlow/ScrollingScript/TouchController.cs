﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchController : MonoBehaviour {

	public static GameObject OnHitTargetFromTouch = null;
	public Camera uiRootCam;
	public  ScaleScroller scaleScroller;
	public simpleScroller simpleScroller;
	private const string LanguageWrapper = "LanguageWrapperVertical";
	public const string MenuWrapper = "Menu_Horizontal";
	private ButtonCheckToBeAvaliable buttonCheckToBeAvaliable;
	private InputTouch inputTouch;


	void Start ()
	{
		OnHitTargetFromTouch = null;
		buttonCheckToBeAvaliable = GetComponent <ButtonCheckToBeAvaliable> ();
		inputTouch = GetComponent<InputTouch> ();
	}


	void Update () 
	{

		if (Input.GetMouseButton (0)) 
		{
			RaycastHit hit;
			if (Physics.Raycast (uiRootCam.ScreenPointToRay (Input.mousePosition), out hit)) 
			{
				OnHitTargetFromTouch = hit.collider.gameObject;
			//	Debug.Log ("OnHitTargetFromTouch : " + OnHitTargetFromTouch);
			
			}
		}
		inputTouch.InputTouchUpdate ();
		ScrollFunction ();
		if (Input.GetMouseButtonUp (0)) {
			//buttonCheckToBeAvaliable.ButtonCheckFunction ();
		}
	}


	private void ScrollFunction ()
	{
		if (OnHitTargetFromTouch != null) {
		
			//if (OnHitTargetFromTouch.name.Contains (MenuWrapper)){
			if (OnHitTargetFromTouch.tag.Equals(MenuWrapper)){
				scaleScroller.isAllowToScroll = true;
			} else {
				scaleScroller.isAllowToScroll = false;
			}

			if (OnHitTargetFromTouch.name.Contains(LanguageWrapper)) {
				simpleScroller.isVerticalScroll = true;
			} else {
				simpleScroller.isVerticalScroll = false;
			}
		}
	}
}
