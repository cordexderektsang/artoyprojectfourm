﻿using UnityEngine;
using System.Collections;

public class chatScroller : MonoBehaviour {

	public enum ScrollType
	{
		NONE,
		VERTICAL,
		HORTORIZAL

	}

	[Header ("Type Of Scroll")]
	public ScrollType scroll = ScrollType.NONE;

	public enum ScrollStatus
	{
		REACH_START_BOUNDARY,
		SCROLL_ALLOWED,
		REACH_END_BOUNDARY,
		SCROLL_NOTALLOWED,
		NONE

	}

	private ScrollStatus status = ScrollStatus.NONE;

	//[HideInInspector]
	public bool isHorizotalScroll = false;
	//[HideInInspector]
	public bool isVerticalScroll = false;

	public UILabel middlescrollview;
	public UIPanel scrollviewPanel;
	/// middle panel is require

	[Range (1, 300)]
	public int decelerationDistance;

	[Range (-100, 100)]
	public float glassCeillBoundary = 50;

	public  int gap;

	[Range (1, 6)]
	public int mouseForce;
	[SerializeField]
	private int startLenght;
	[SerializeField]
	private int endLenght;
	private float firstValue = 0;
	// first touch
	private float secondValue = 0;
	//second touch
	private float firstTouch = 0;
	//first touch down
	private float releaseTouch = 0;
	// release touch
	private float touchGap = 0;
	// gap for touch
	private float counter = 0;

	// did mouse clicked

	private float mouseYFirst;
	private float mouseYSecond;

	private bool canMoveDown = true;
	private bool canMoveUp = false;
	private bool canMoveLeft = true;
	private bool canMoveRight = false;


	private bool isInital = false;
	private bool isMouseRelease = false;
	private Coroutine verticalSlowCorotine = null;
	private Coroutine hortorizalSlowCorotine = null;
	private bool isMouseAlreadyUp = false;

	private const float initalSpeedMovement = 0.1f;
	private float SpeedMovementCounter;


	// Use this for initialization
	private void firstSetUp ()
	{
		if (scroll == ScrollType.VERTICAL) {
			startLenght = (int)this.gameObject.transform.localPosition.y;
		} else if (scroll == ScrollType.HORTORIZAL) {
			startLenght = (int)this.gameObject.transform.localPosition.x;
		}
		SetUp (true);

	}



	#region fucntion call


	public void resetFirstValue (){

		firstValue = 0;
	}
	/// <summary>
	/// Resets to start position.
	/// </summary>
	public void resetToStartPosition ()
	{
		SetUp ();
		if (scroll == ScrollType.VERTICAL) {
			isVerticalScroll = false;
			this.gameObject.transform.localPosition = new Vector2 (this.gameObject.transform.localPosition.x, startLenght);
		} else if (scroll == ScrollType.HORTORIZAL) {
			this.gameObject.transform.localPosition = new Vector2 (startLenght, this.gameObject.transform.localPosition.y);
			isHorizotalScroll = false;
		}


	}

	/// <summary>
	/// Resets to end position.
	/// </summary>
	public void resetToEndPosition ()
	{
		SetUp ();
		if (scroll == ScrollType.VERTICAL) {
			isVerticalScroll = false;
			this.gameObject.transform.localPosition = new Vector2 (this.gameObject.transform.localPosition.x, endLenght);

		} else if (scroll == ScrollType.HORTORIZAL) {
			isHorizotalScroll = false;
			this.gameObject.transform.localPosition = new Vector2 (endLenght, this.gameObject.transform.localPosition.y);
		}

	}





	/// <summary>
	/// base on required active item 
	/// active Left and right object along with parents as well as  deactive the rest
	/// new version
	/// </summary>
	/// <param name="numberOfActiveItem">Number of active item.</param>
	public void setItemStatuss (int numberOfActiveItem)
	{
		int count = 0;
		foreach (Transform child in transform) {

			int childCount = 0;// use for ensure is it even or odd valeu
			foreach (Transform evenChild in child) {

				//if less than active item, make item  and parent visible
				// if odd turn off the next parent otherwise turn off the current one
				if (count < numberOfActiveItem) {
					child.gameObject.SetActive (true);
					evenChild.gameObject.SetActive (true);
					count++;
				} else {

					evenChild.gameObject.SetActive (false);
					childCount++;
				}
				if (childCount != 0 && childCount % 2 == 0) {

					child.gameObject.SetActive (false);
				}

			}

		}

	}


	/// <summary>
	/// base on required active item 
	/// active Left and right object along with parents as well as  deactive the rest
	/// new version
	/// </summary>
	/// <param name="numberOfActiveItem">Number of active item.</param>
	public void setSingleItemStatus (int numberOfActiveItem)
	{
		for (int i = 0; i < ReturnAllChildCount (); i++) {
			if (i < numberOfActiveItem) {
				transform.GetChild (i).gameObject.SetActive (true);
			} else {
				transform.GetChild (i).gameObject.SetActive (false);
			}

		}

	}



	/// <summary>
	/// set item staut to be active or not
	/// old version
	/// </summary>
	/// <param name="numberOfActiveItem">Number of active item.</param>
	/// <param name="status">If set to <c>true</c> status.</param>
	public void SetItemStatus (int numberOfActiveItem, bool status = false, bool evenItem = true)
	{
		if (evenItem) {
			int evenDivide = numberOfActiveItem / 2;
			int remaider = numberOfActiveItem % 2;
			Debug.Log ("evenDivide : " + evenDivide + " remaider " + remaider);
			for (int i = evenDivide; i < ReturnAllChildCount (); i++) {
				if (remaider == 0) {
					transform.GetChild (i).gameObject.SetActive (status);
				} else if (i == evenDivide && remaider != 0) {
					GameObject reference = transform.GetChild (evenDivide).gameObject;

					reference.transform.GetChild (0).gameObject.SetActive (status);
				} else {
					transform.GetChild (i).gameObject.SetActive (status);
				}

			}
		} else {
			for (int i = numberOfActiveItem; i < ReturnAllChildCount (); i++) {

				transform.GetChild (i).gameObject.SetActive (status);

			}


		}
		resetPosition ();
	}





	#endregion

	// Update is called once per frame
	void Update ()
	{
		if (isVerticalScroll || isHorizotalScroll) {

			startUp ();

			inputMouseDownFunction ();

			inputMouseClickFunction ();

			//inputMouseUpFunction ();

			//mouseUpFunction ();

		} else {

			firstValue = 0;
		}

	}

	/// <summary>
	/// Sets up.
	/// </summary>
	public void startUp()
	{

		if (!isInital) {
			firstSetUp ();
			//isInital = true;
			if (GetComponent<UITexture>() != null)
			{
				GetComponent<UITexture>().SetAnchor((Transform)null);
			}
			isInital = true;
			//StartCoroutine(delayForReleaseAnchor(0.01f));
		} else {

			return;
		}
	}

	/// <summary>
	/// Inputs the mouse up function.
	/// </summary>
	public void inputMouseUpFunction ()
	{
		if (Input.GetMouseButtonUp (0)) {
			if (scroll == ScrollType.VERTICAL) {
				releaseTouch = Input.mousePosition.y;
				touchGap = firstTouch - releaseTouch;
				float touchGapPerecentageReference = Mathf.Abs (touchGap) / decelerationDistance;
				SpeedMovementCounter = initalSpeedMovement * touchGapPerecentageReference;
				isMouseAlreadyUp = true;
				verticalSlowCorotine = StartCoroutine (verticalAcceleration ());
				firstValue = 0;
			} 
			if(scroll == ScrollType.HORTORIZAL){
				releaseTouch = Input.mousePosition.x;
				touchGap = firstTouch - releaseTouch;
				float touchGapPerecentageReference = Mathf.Abs (touchGap) / decelerationDistance;
				SpeedMovementCounter = initalSpeedMovement * touchGapPerecentageReference;
				isMouseAlreadyUp = true;
				hortorizalSlowCorotine = StartCoroutine (hortizontalAcceleration ());
				firstValue = 0;

			}
		}

	}

	/// <summary>
	/// Mouses up function for Acceleration 
	/// </summary>
	public void mouseUpFunction()
	{
		if(isMouseAlreadyUp == true){
			if (scroll == ScrollType.VERTICAL) {
				verticalSlowCorotine = StartCoroutine (verticalAcceleration ());
			} else if (scroll == ScrollType.HORTORIZAL){
				verticalSlowCorotine = StartCoroutine (hortizontalAcceleration ());
			}
			//isMouseAlreadyUp = false;

		}
	}

	/// <summary>
	/// Inputs the mouse down function.
	/// </summary>
	public void inputMouseDownFunction ()
	{
		if (Input.GetMouseButtonDown (0)) {
			if (scroll == ScrollType.VERTICAL) {

				firstValue = Input.mousePosition.y;
				firstTouch = Input.mousePosition.y;

			} else  if (scroll == ScrollType.HORTORIZAL){
				firstValue = Input.mousePosition.x;
				firstTouch = Input.mousePosition.x;
			}
			isMouseAlreadyUp = false;
			if (verticalSlowCorotine != null) {
				StopCoroutine (verticalSlowCorotine);
			}
		}

	}

	/// <summary>
	/// Mouses the click function.
	/// </summary>
	public void inputMouseClickFunction (){

		if (Input.GetMouseButton (0)) {
			if (scroll == ScrollType.VERTICAL /*&& Mathf.Abs (this.gameObject.transform.childCount * gap) > middlescrollview.GetViewSize ().y*/ && isVerticalScroll) {


				//	if (this.gameObject.transform.localPosition.y >= startLenght && this.gameObject.transform.localPosition.y <= endLenght) {
				VerticalScrolling ();
				//}


			} else if (scroll == ScrollType.HORTORIZAL /*&& Mathf.Abs (this.gameObject.transform.childCount * gap) > middlescrollview.GetViewSize ().x*/ && isHorizotalScroll) {


				//if (this.gameObject.transform.localPosition.x <= startLenght && this.gameObject.transform.localPosition.x >= endLenght) {
				HortizontalScrolling ();
				//}

			}
		}

	}

	public void SetEndLength(int newLength)
	{
		startUp();
		endLenght = newLength;
	}

	#region  movementFunction 	
	/// <summary>
	/// Vertical scroll
	/// </summary>
	private void VerticalScrolling ()
	{

		secondValue = Input.mousePosition.y;
		//float mouseYActalDistanceWithFirstValue = firstValue - secondValue;
		//float mouseYActalDistanceWithSecondValue = secondValue - firstValue;
		resetPosition ();


		if (firstValue > secondValue && canMoveUp /*&& mouseYActalDistanceWithFirstValue > 0*/ && firstValue != 0) {



			this.gameObject.transform.localPosition = new Vector2 (this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - ((firstValue - secondValue) * mouseForce)/*counter*/);


		} else if (firstValue < secondValue && canMoveDown /*&& mouseYActalDistanceWithSecondValue > 20*/  && firstValue != 0) {

			this.gameObject.transform.localPosition = new Vector2 (this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y + ((secondValue - firstValue) * mouseForce)/*counter*/);

		}
		firstValue = secondValue;
		if (!Input.GetMouseButtonUp (0)) {
			firstTouch = Input.mousePosition.y;
		}

	}

	/// <summary>
	/// HortizontalScroll
	/// </summary>
	private void HortizontalScrolling ()
	{
		secondValue = Input.mousePosition.x;
		//float mouseYActalDistanceWithFirstValue = firstValue - secondValue;
		//float mouseYActalDistanceWithSecondValue = secondValue - firstValue;
		resetPosition ();
		if (firstValue > secondValue && canMoveLeft/* && mouseYActalDistanceWithFirstValue > 20*/ && firstValue != 0) {


			this.gameObject.transform.localPosition = new Vector2 (this.gameObject.transform.localPosition.x - (firstValue - secondValue) * mouseForce, this.gameObject.transform.localPosition.y);

		} else if (firstValue < secondValue && canMoveRight /*&& mouseYActalDistanceWithSecondValue > 20*/  && firstValue != 0) {



			this.gameObject.transform.localPosition = new Vector2 (this.gameObject.transform.localPosition.x + (secondValue - firstValue) * mouseForce, this.gameObject.transform.localPosition.y);


		}
		firstValue = secondValue;
		if (!Input.GetMouseButtonUp (0)) {
			firstTouch = Input.mousePosition.y;
		}
	}

	/// <summary>
	///fade  slow Force after touch release
	/// </summary>
	IEnumerator  verticalAcceleration ()
	{
		yield return new WaitForSeconds (0.01f);
		while (touchGap > 10 || touchGap < -10) {
			yield return new WaitForSeconds (0.01f);
			if (touchGap > 10) {
				//float gapValue = Mathf.Abs (touchGap / 100);
				if (SpeedMovementCounter > 0) {
					//if (counter < gapValue) {
					transform.Translate (0, -SpeedMovementCounter, 0);
					//this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - counter);
					SpeedMovementCounter -= 0.001f;
					if (resetPosition ()) {
						yield break;
					}
					//counter += 0.2f;

				} else {
					resetPosition ();
					isMouseAlreadyUp = false;

					yield break;
				}


			} else if (touchGap < -10) {
				//float gapValue = Mathf.Abs (touchGap / 100);
				if (SpeedMovementCounter > 0) {
					//if (counter < gapValue) {
					transform.Translate (0, SpeedMovementCounter, 0);
					//this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - counter);
					SpeedMovementCounter -= 0.001f;
					if (resetPosition ()) {
						yield break;
					}
					//counter += 0.2f;


				} else {

					resetPosition ();
					yield break;
				}
			}

		} 
	}


	/// <summary>
	///fade  slow Force after touch release
	/// </summary>
	IEnumerator hortizontalAcceleration ()
	{
		yield return new WaitForSeconds (0.01f);
		while (touchGap > 1 || touchGap < -1) {
			yield return new WaitForSeconds (0.01f);
			if (touchGap > 1) {
				//float gapValue = Mathf.Abs (touchGap / 100);
				if (SpeedMovementCounter > 0) {
					//if (counter < gapValue) {
					transform.Translate (-SpeedMovementCounter, 0, 0);
					//this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - counter);
					SpeedMovementCounter -= 0.001f;
					if (resetPosition ()) {
						yield break;
					}
					//counter += 0.2f;

				} else {
					resetPosition ();
					isMouseAlreadyUp = false;

					yield break;
				}


			} else if (touchGap < -1) {
				//float gapValue = Mathf.Abs (touchGap / 100);
				if (SpeedMovementCounter > 0) {
					//if (counter < gapValue) {
					transform.Translate (SpeedMovementCounter, 0, 0);
					//this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - counter);;
					SpeedMovementCounter -= 0.001f;
					if (resetPosition ()) {
						yield break;
					}
					//counter += 0.2f;


				} else {

					resetPosition ();
					yield break;
				}
			}

		} 

	}

	#endregion

	#region  setUpFunction 	

	void OnEnable() 
	{
		Debug.Log ("enable");
		SetUp ();
	}

	/// <summary>
	/// keep update the end length
	/// </summary>
	/// <param name="setUp"></param>
	public void SetUp (bool setUp = false)
	{

		//if (/*Input.GetMouseButtonDown (0) || */setUp) {
		//int childnumber = ReturncurrentActiveChildCount ();
		if (scroll == ScrollType.VERTICAL) {
			Debug.Log (startLenght + " : " +middlescrollview.height  + " : "+ scrollviewPanel.GetViewSize().y);
			//get the view size from panel then get the number of child for this parnet 
			// set the end limit to be the lenght for all child length substract the middle view  size (vertical use negative gapY value)
			int scrollviewsize = (int)middlescrollview.height ;
			//int containItemInBar = scrollviewPanel / ((int)gap);
			//int remainer = (scrollviewsize % -((int)gap));
			//int gapForEachItem = 50; // space for item 

			int limitvalue = (int)startLenght;
			//	Debug.Log (scrollviewsize + " : " + containItemInBar +  " : " + remainer  + " : " + gapY);
			if (scrollviewsize > scrollviewPanel.GetViewSize().y) {
				//limitvalue = childnumber - containItemInBar;
				//	endLenght = (limitvalue * gapY) + startLenght - remainer + gapForEachItem;
				//	Debug.Log(childnumber - containItemInBar + " all child " + ReturnAllChildCount() + " active child " + ReturncurrentActiveChildCount() + " container : " + containItemInBar);
				//	Debug.Log(startLenght +Mathf.Abs(transform.GetChild(childnumber - containItemInBar).gameObject.transform.localPosition.y));
				endLenght =  (scrollviewsize - (int)scrollviewPanel.GetViewSize().y) + startLenght + 200;
			} else {
				endLenght = limitvalue;
			}



		} else if (scroll == ScrollType.HORTORIZAL) {
			//get the view size from panel then get the number of child for this parnet 
			// set the end limit to be the lenght for all child length substract the middle view  size (vertical use position gapX value)
			int scrollviewsize = (int)middlescrollview.width;
			//int containItemInBar = scrollviewsize / ((int)gap);
			//int remainer = (scrollviewsize % -((int)gap));
			//int gapForEachItem = 50; // space for item 
			int limitvalue = (int)startLenght;
			//	Debug.Log (scrollviewsize + " : " + containItemInBar +  " : " + remainer  + " : " + gapY);
			if (scrollviewsize > scrollviewPanel.GetViewSize().y) {
				//limitvalue = childnumber - containItemInBar;
				//	endLenght = (limitvalue * gapY) + startLenght - remainer + gapForEachItem;
				//	Debug.Log(childnumber - containItemInBar + " all child " + ReturnAllChildCount() + " active child " + ReturncurrentActiveChildCount() + " container : " + containItemInBar);
				//	Debug.Log(startLenght +Mathf.Abs(transform.GetChild(childnumber - containItemInBar).gameObject.transform.localPosition.y));
				endLenght =  (scrollviewsize - (int)scrollviewPanel.GetViewSize().x) + startLenght + 200;
			} else {
				endLenght = limitvalue;
			}

		}   


	}


	/*IEnumerator  delayForReleaseAnchor(float time){

		yield return new WaitForSeconds(time);
		if (GetComponent<UITexture>() != null)
		{
			GetComponent<UITexture>().SetAnchor((Transform)null);
		}
		isInital = true;
		yield break;
	}*/

	/// <summary>
	/// reset position to either endpoint or startpoint
	/// then return status about such reset
	/// true is reset
	/// false did not rest
	/// stop moving beyond boundary 
	/// </summary>
	/// <returns></returns>
	private bool resetPosition ()
	{

		if (scroll == ScrollType.VERTICAL) {
			if (this.gameObject.transform.localPosition.y < startLenght) {
				if (hortorizalSlowCorotine != null) {
					StopCoroutine (hortorizalSlowCorotine);
				}
				this.gameObject.transform.localPosition = new Vector2 (this.gameObject.transform.localPosition.x, startLenght);
				canMoveDown = true;
				canMoveUp = false;
				return true;
			} else if (this.gameObject.transform.localPosition.y > endLenght) {
				if (verticalSlowCorotine != null) {
					StopCoroutine (verticalSlowCorotine);
				}
				this.gameObject.transform.localPosition = new Vector2 (this.gameObject.transform.localPosition.x, endLenght);
				canMoveUp = true;
				canMoveDown = false;
				return true;

			} else {

				if (!canMoveUp) {
					float startLenghtGlassCeil = startLenght + glassCeillBoundary;
					if (this.gameObject.transform.localPosition.y > startLenghtGlassCeil) {

						canMoveUp = true;
					}

				}
				if (!canMoveDown) {
					float endLenghtGlassCeil = endLenght - glassCeillBoundary;
					if (this.gameObject.transform.localPosition.y < endLenghtGlassCeil) {

						canMoveDown = true;
					}

				}
				return false;
			}
		} else if (scroll == ScrollType.HORTORIZAL) {

			if (this.gameObject.transform.localPosition.x > startLenght) {
				//Debug.Log ("start");
				if (hortorizalSlowCorotine != null) {
					StopCoroutine (hortorizalSlowCorotine);
				}

				this.gameObject.transform.localPosition = new Vector2 (startLenght, this.gameObject.transform.localPosition.y);
				canMoveLeft = true;
				canMoveRight = false;
				return true;
			} else if (this.gameObject.transform.localPosition.x < -endLenght) {
				//Debug.Log ("end");
				if (verticalSlowCorotine != null) {
					StopCoroutine (verticalSlowCorotine);
				}
				this.gameObject.transform.localPosition = new Vector2 (-endLenght, this.gameObject.transform.localPosition.y);
				canMoveLeft = false;
				canMoveRight = true; 
				return true;

			} else {
				if (!canMoveRight) {
					float startLenghtGlassCeil = startLenght + glassCeillBoundary;
					if (this.gameObject.transform.localPosition.x < startLenghtGlassCeil) {

						//	canMoveLeft = true;
						canMoveRight = true;
					}

				}
				if (!canMoveLeft) {
					float endLenghtGlassCeil = endLenght - glassCeillBoundary;
					if (this.gameObject.transform.localPosition.x > endLenghtGlassCeil) {

						canMoveLeft = true;
					}

				}
				return false;
			}
		}
		return false;
	}

	#endregion

	#region  returnFunction 

	//return number of Child
	public int ReturnAllChildCount ()
	{
		int count = 0;
		foreach (Transform child in transform) {
			count++;		
		}
		return count;
	}

	//disable object
	public void deactiveAllObject ()
	{
		foreach (Transform child in transform) {
			child.gameObject.SetActive (false);
		}
	}


	//return current activeCount
	public int ReturncurrentActiveChildCount ()
	{
		int count = 0;
		foreach (Transform child in transform) {
			if (child.gameObject.activeSelf) {
				count++;
			}
		}
		return count;
	}

	/// <summary>
	/// return scroll status
	/// </summary>
	public  ScrollStatus returnStatus ()
	{

		if (scroll == ScrollType.VERTICAL && isVerticalScroll) {
			if (this.gameObject.transform.localPosition.y < startLenght ) {
				status = ScrollStatus.REACH_START_BOUNDARY;
			} else if (this.gameObject.transform.localPosition.y > endLenght ) {

				status = ScrollStatus.REACH_END_BOUNDARY;

			} else {
				status = ScrollStatus.SCROLL_ALLOWED;
			}

		} else if (scroll == ScrollType.HORTORIZAL && isHorizotalScroll) {
			if (this.gameObject.transform.localPosition.x > startLenght ) {
				status = ScrollStatus.REACH_START_BOUNDARY;
			} else if (this.gameObject.transform.localPosition.x < endLenght ) {
				status = ScrollStatus.REACH_END_BOUNDARY;
			} else {
				status = ScrollStatus.SCROLL_ALLOWED;
			}

		}
		return status;
	}

	#endregion
}
