﻿using UnityEngine;
using System.Collections;

public class ScaleScroller : MonoBehaviour
{
    public float gapX = 0; // gapx for each object
    public float slowMotionMovementDistance = 20f; // slow motion movement after letting go of finger
	public float mouseForce = 2;
    [SerializeField]
    private float startLength;
    [SerializeField]
    private float endLength;
    [Header("should Position be Organised ")]
    public bool laterOrganisationUpdate = false;
    [Header("Item limitation for end scroll")]
    public int limitEndRange = 7;
    public bool isAllowToScroll = true;

    private float firstValue = 0;// first touch
    private float secondValue = 0;//second touch
    private float firstTouch = 0;//first touch down
    private float releaseTouch = 0;// release touch
    private float touchGap = 0;// gap for touch
    private float force = 15;// release force
    private float referenceForce = 15; // reference force
    private float mouseXFirst;
    private float mouseXSecond;
    private float mouseYFirst;
    private float mouseYSecond;
    private bool canMoveUp = true;
    private bool canMoveDown = true;
    [SerializeField]
    private bool canMoveLeft = true;
    [SerializeField]
    private bool canMoveRight = false; // default false cause first picture can only scroll right

    private float scale = 1.2f;//1
    private float scaleForPositionGeneration = 1.2f;
	private float scaleForPositionGenerationSecond = 1.2f;
    private float minimumScale = 0.5f;//0.7
    private float scaleFactor = 0.003f;
    public int index = 1;
    public int previousIndex = -1;
    [SerializeField]
    private int calculationIndex;
    [SerializeField]
    private int totalItems = 0;

    private float scaleValue;
    [Header("Handle posterDecciption")]
  //  public generateNews referenceList;

//    [SerializeField]
   // private bool isMovedLeft = false;
//    [SerializeField]
   // private bool isMovedRight = false;
    [SerializeField]
    private bool isMouseMovedLeftFirst = false;
    [SerializeField]
    private bool isMouseMovedRightFirst = false;

    private bool isFinishedTransition = false;

	[SerializeField]
    private GameObject current;
	[SerializeField]
    private GameObject previous;

    private float offset;
    private float currentScale;
    private float previousScale;


	/// <summary>
	/// Gets the index of the current item.
	/// </summary>
	/// <returns>The current index.</returns>
	public int GetCurrentIndex ()
	{
		return calculationIndex;
	}

	public GameObject GetCurrentObject ()
	{
		return current;
	}

    /// <summary>
    /// Organisation position for all child
    /// </summary>
    public void OrganisingPosition()
    {
		totalItems = 0;
        float localX = 0;// create local x
		Debug.Log(" ===set up == " + startLength);
		startLength = 0;
       // startLength = this.gameObject.transform.localPosition.x;
		scaleForPositionGeneration = scaleForPositionGenerationSecond;
        foreach (Transform child in transform)
        {
            // child.gameObject.transform.localPosition = new Vector2(localX, localY);// set each child position

			child.gameObject.transform.localPosition = new Vector3(localX, child.gameObject.transform.localPosition.y ,child.gameObject.transform.localPosition.z);// set each child position
            localX = localX + gapX;

            child.gameObject.transform.localScale = new Vector2(scaleForPositionGeneration, scaleForPositionGeneration);
            scaleForPositionGeneration = minimumScale;

            totalItems++;
        }
        int numberOfChild = gameObject.transform.childCount - 1;
        if (numberOfChild > 0)
        {
            endLength = (-gameObject.transform.GetChild(numberOfChild).transform.localPosition.x) + (startLength * limitEndRange);
        }
		SetToTargetItem (GlobalStaticVariable.currentIndex);
    }

	public void SetToTargetItem (int SelectedIndex)
	{
		if (SelectedIndex >= totalItems) {
			SelectedIndex = 0;
		}

		this.gameObject.transform.localPosition = new Vector2( -(SelectedIndex * gapX), this.gameObject.transform.localPosition.y);
		foreach (Transform child in transform)
		{
			// child.gameObject.transform.localPosition = new Vector2(localX, localY);// set each child position

			child.gameObject.transform.localScale = new Vector2(minimumScale, minimumScale);

		
		}
		transform.GetChild(SelectedIndex).localScale = new Vector2(scaleForPositionGenerationSecond, scaleForPositionGenerationSecond);
		index = SelectedIndex + 1;
		previousIndex = SelectedIndex -1;
		calculationIndex = SelectedIndex;
		canMoveRight = true;
		canMoveLeft = true;
	
		current =	transform.GetChild (SelectedIndex).gameObject;
		//Debug.Log (SelectedIndex);
		if (SelectedIndex > 0 ) {
			Debug.Log (SelectedIndex);
			previous =	transform.GetChild (SelectedIndex - 1).gameObject;
		}
		isAllowToScroll = true;
	}

    /// <summary>
    /// when object enable do start function
    /// </summary>
    void Start()
    {

			//OrganisingPosition ();

      //  StartCoroutine(LateStart());
    }

    /// <summary>
    /// if --> scale up next pic and scale previous pic 
    /// if scale = 1 then add index
    /// </summary>
    private void AddScaleByScroll(float value)
    {
        if (!isMouseMovedLeftFirst)
        {
            isMouseMovedRightFirst = true;
        }

        /*if (!isMovedRight)
        {
            isMovedRight = true;
        }*/

        calculationIndex = index;

        // Swipe left but not far enough, move back to original
        if (((/*isMovedRight && isMovedLeft &&*/ !isMouseMovedRightFirst) || isMouseMovedLeftFirst) && !Input.GetMouseButton(0))
        {
            calculationIndex--;

            //if (Input.GetMouseButton(0))
            //{
            //    isMovedLeft = false;
            //}
        }

        if ((/*(isMovedRight && isMovedLeft) || */isMouseMovedLeftFirst) && Input.GetMouseButton(0))
        {
            if (isMouseMovedLeftFirst)
            {
                calculationIndex--;
            }
           // isMovedLeft = false;
        }

        if (calculationIndex >= totalItems)
        {
            calculationIndex = totalItems - 1;
        }

			current = this.gameObject.transform.GetChild (calculationIndex).gameObject;

        //try
        //{
            previous = this.gameObject.transform.GetChild(calculationIndex - 1).gameObject;
        //}
        //catch (System.Exception ex)
        //{
        //    Debug.Log("isMovedRight: " + isMovedRight + ", isMovedLeft: " + isMovedLeft + ", calculationIndex: " + calculationIndex);
        //}

    //    referenceList.description.text = referenceList.referenceBanner[calculationIndex].description;
       // referenceList.postTitle.text = referenceList.referenceBanner[calculationIndex].postTitle;
       // referenceList.postDate.text = referenceList.referenceBanner[calculationIndex].postDate;
      ///  referenceList.CountLinesInString(referenceList.description.text, referenceList.description);

        offset = (value / gapX) * (scale - minimumScale);
        currentScale = current.transform.localScale.x;
        previousScale = previous.transform.localScale.x;

        this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x - value, this.gameObject.transform.localPosition.y);

        if (currentScale < scale)
        {
            //CCScreenLogger.LogStatic("currentScale before: " + currentScale, 0);
            currentScale += offset;
            //CCScreenLogger.LogStatic("currentScale after: " + currentScale, 1);
            current.transform.localScale = new Vector2(currentScale, currentScale);
        }

        if (previousScale > minimumScale)
        {
            //CCScreenLogger.LogStatic("previousScale before: " + previousScale, 2);
            previousScale -= offset;
            //CCScreenLogger.LogStatic("previousScale after: " + previousScale, 3);
            previous.transform.localScale = new Vector2(previousScale, previousScale);
        }

        if (previousScale <= minimumScale && currentScale >= scale/* && index < totalItems*/)
        {
            if (index < totalItems && !(/*isMovedLeft && isMovedRight &&*/ !isMouseMovedRightFirst) && !isMouseMovedLeftFirst)
            {
                previousIndex = index;
                index++;
            }

           // isMovedLeft = false;
          //  isMovedRight = false;

            isMouseMovedLeftFirst = false;
            isMouseMovedRightFirst = false;

         //   referenceList.description.text = referenceList.referenceBanner[index - 1].description;
         //   referenceList.postDate.text = referenceList.referenceBanner[index - 1].postDate;
            this.gameObject.transform.localPosition = new Vector2(-gapX * (index - 1), this.gameObject.transform.localPosition.y);
            current.transform.localScale = new Vector2(scale, scale);
            previous.transform.localScale = new Vector2(minimumScale, minimumScale);
            isFinishedTransition = true;
        }
    }

    private void SubstractScaleByScroll(float value)
    {
        if (!isMouseMovedRightFirst)
        {
            isMouseMovedLeftFirst = true;
        }

        /*if (!isMovedLeft)
        {
            isMovedLeft = true;
        }*/

        calculationIndex = index - 2;

        // Swipe right but not far enough, move back to original
        if (((/*isMovedRight && isMovedLeft &&*/ !isMouseMovedLeftFirst) || isMouseMovedRightFirst) && !Input.GetMouseButton(0))
        {
            //Debug.Log("downwards release moveLeftRight");
            calculationIndex++;

            //if (Input.GetMouseButton(0))
            //{
            //    isMovedRight = false;
            //}
        }

        if ((/*(isMovedRight && isMovedLeft) ||*/ isMouseMovedRightFirst) && Input.GetMouseButton(0))
        {
            if (isMouseMovedRightFirst)
            {
                calculationIndex++;
            }
           // isMovedRight = false;
        }

        //Debug.Log("Scaling down");
        if (calculationIndex < 0)
        {
            calculationIndex = 0;
        }

        current = this.gameObject.transform.GetChild(calculationIndex).gameObject;
        previous = this.gameObject.transform.GetChild(calculationIndex + 1).gameObject;

    //    referenceList.description.text = referenceList.referenceBanner[calculationIndex].description;
    //    referenceList.postTitle.text = referenceList.referenceBanner[calculationIndex].postTitle;
    //    referenceList.postDate.text = referenceList.referenceBanner[calculationIndex].postDate;
     //   referenceList.CountLinesInString(referenceList.description.text, referenceList.description);

		offset = (value / gapX) * (scale - minimumScale);
        currentScale = current.transform.localScale.x;
        previousScale = previous.transform.localScale.x;

       // Debug.Log("sub remove: " + value + "offset : " + offset + "previousScale : " + previousScale + "currentScle ;" + currentScale);
        this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x + value, this.gameObject.transform.localPosition.y);

        if (currentScale < scale)
        {
            //CCScreenLogger.LogStatic("currentScale before: " + currentScale, 5);
            currentScale += offset;
            //CCScreenLogger.LogStatic("currentScale after: " + currentScale, 6);
            current.transform.localScale = new Vector2(currentScale, currentScale);
        }

        if (previousScale > minimumScale)
        {
            //CCScreenLogger.LogStatic("previousScale before: " + previousScale, 7);
            previousScale -= offset;
            //CCScreenLogger.LogStatic("previousScale after: " + previousScale, 8);
            previous.transform.localScale = new Vector2(previousScale, previousScale);
        }

        float absLocalPositionX = Mathf.Abs(transform.localPosition.x);
        float remainderMoveDistance = absLocalPositionX % gapX;

        if (previousScale <= (minimumScale * 1.025f) && currentScale >= (scale * 0.975f))
        {
            if (index > 1 && !(/*isMovedLeft && isMovedRight &&*/ !isMouseMovedLeftFirst) && !isMouseMovedRightFirst)
            {
                previousIndex = index;
                index--;
            }

          //  isMovedLeft = false;
          //  isMovedRight = false;

            isMouseMovedLeftFirst = false;
            isMouseMovedRightFirst = false;

        //    referenceList.description.text = referenceList.referenceBanner[index - 1].description;
        //    referenceList.postDate.text = referenceList.referenceBanner[index - 1].postDate;
            this.gameObject.transform.localPosition = new Vector2(-gapX * (index - 1), this.gameObject.transform.localPosition.y);
            current.transform.localScale = new Vector2(scale, scale);
            previous.transform.localScale = new Vector2(minimumScale, minimumScale);
        }
    }


    /// <summary>
    ///  check is object reach end point or start point
    ///  if yes reset
    ///  if no scroll
    /// </summary>
    void Update()
    {

		if (/*(touchGap > 10 || touchGap < -10) &&*/ !Input.GetMouseButton(0))
		{
			hortizontalSlowMotion();
		}


        if (!isAllowToScroll)
        {
            return;
        }
        else
        {

		
            if (gameObject.transform.localPosition.x <= startLength && gameObject.transform.localPosition.x >= endLength)
            {

                repeatScroll();

            
            }
		
			/*}
            else
			{
				Debug.Log ("slow move" );
                hortizontalSlowMotion();
            }*/

            //if (this.gameObject.transform.localPosition.x <= startLenght && this.gameObject.transform.localPosition.x >= endLenght)
            //{
            //    repeatScroll();
            //}

            //if (touchGap >= 150 && this.gameObject.transform.localPosition.x <= startLenght && this.gameObject.transform.localPosition.x >= endLenght && !Input.GetMouseButton(0))
            //{
            //    hortizontalSlowEmotion();
            //}

            //if (touchGap <= -150 && this.gameObject.transform.localPosition.x <= startLenght && this.gameObject.transform.localPosition.x >= endLenght && !Input.GetMouseButton(0))
            //{
            //    hortizontalSlowEmotion();
            //}
        }

    }


    /// <summary>
    /// scroll up , down , left ,right
    /// </summary>
    private void repeatScroll()
    {
        if (Input.GetMouseButtonDown(0))
        {
           // touchGap = 0;

            firstValue = Input.mousePosition.x;
            firstTouch = Input.mousePosition.x;
            mouseYFirst = Input.mousePosition.y;
 

            //isMovedLeft = false;
           // isMovedRight = false;
        }

        if (Input.GetMouseButtonUp(0))
        {
            firstValue = Input.mousePosition.x;
            releaseTouch = Input.mousePosition.x;
          //  touchGap = firstTouch - releaseTouch;
        }

        hortizontalScrolling();
    }


    /// <summary>
    /// reset position to either endpoint or startpoint
    /// then return status about such reset
    /// true is reset
    /// false did not rest
    /// </summary>
    /// <returns></returns>
    private void resetPosition()
    {
        if (this.gameObject.transform.localPosition.x >= startLength)
        {
            //Debug.Log("ResetmoveLeft");
            //StopAllCoroutines();
            this.gameObject.transform.localPosition = new Vector2(startLength, this.gameObject.transform.localPosition.y);
            canMoveLeft = true;
			canMoveRight = true;
        }
        else if (this.gameObject.transform.localPosition.x <= endLength)
        {
            // Debug.Log("ResetmoveRight");
            //StopAllCoroutines();
            this.gameObject.transform.localPosition = new Vector2(endLength, this.gameObject.transform.localPosition.y);
			canMoveLeft = true;
            canMoveRight = true;
        }
        else
        {
            //    Debug.Log("move");
            canMoveLeft = true;
            canMoveRight = true;
        }
    }

    /// <summary>
    /// HortizontalScroll
    /// </summary>
    private void hortizontalScrolling()
    {
        if (Input.GetMouseButton(0))
        {
            secondValue = Input.mousePosition.x;
            mouseYSecond = Input.mousePosition.y;
            float mouseYDistance = Mathf.Abs(mouseYFirst - mouseYSecond);
            float mouseXDistance = Mathf.Abs(firstValue - secondValue);
            float difference = Mathf.Abs(mouseXDistance - mouseYDistance);

            if (mouseXDistance > mouseYDistance && difference > 20)
            {
                //scrollSingleObjectScript.isAllowToScroll = false;

				//Debug.Log("scrolling"  + canMoveLeft.ToString() +" : " +  canMoveRight.ToString() );
                // resetPosition();
                if (firstValue > secondValue && canMoveLeft /*&& !isAbsoluteRight*/)
                {
                  //  Debug.Log("moveLeft: " + mouseXDistance);
                    //float distance = mouseXDistance;
					AddScaleByScroll(mouseXDistance*mouseForce*Time.deltaTime);
                    //   this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x - (distance), this.gameObject.transform.localPosition.y);

                }
                else if (firstValue < secondValue && canMoveRight /*&& !isAbsoluteLeft*/)
                {
                   // Debug.Log("moveRight: " + mouseXDistance);
                  // float distance = mouseXDistance;
					SubstractScaleByScroll(mouseXDistance*mouseForce*Time.deltaTime);
                    //   this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x + (distance), this.gameObject.transform.localPosition.y);

                }
                firstValue = secondValue;

             //   resetPosition();
            }
        }
    }
    /// </summary>
    public void hortizontalSlowMotion()
    {
        bool isMoveRight;
        float absLocalPositionX = Mathf.Abs(transform.localPosition.x);
        float remainderMoveDistance = absLocalPositionX % gapX;
        //currentScale = transform.GetChild(index).transform.localScale.x;

        //if (currentScale > scale)
        //{
        //    ScaleEdgeCase(0);
        //}
        if (remainderMoveDistance == 0)
        {
            //checkScaleEdgeCase();
            //scrollSingleObjectScript.isAllowToScroll = true;
            return;
        }
        // Handle edge case of going too far left
        //if (transform.localPosition.x > 0 && isAbsoluteLeft)
        //{
        //    CCScreenLogger.LogStatic("too far left", 0);
        //    if (transform.localPosition.x > slowMotionMovementDistance /*|| remainderMoveDistance < (gapX - 20f)*/)
        //    {
        //        remainderMoveDistance = slowMotionMovementDistance;
        //    }
        //    // If less than 20 units to go until the next gapX segment, move the remainder
        //    else
        //    {
        //        remainderMoveDistance = transform.localPosition.x;
        //    }

        //    AddScaleByScrollEdge(remainderMoveDistance);
        //}
        //// Handle edge case of going too far right
        //else if (transform.localPosition.x < endLength && isAbsoluteRight)
        //{
        //    CCScreenLogger.LogStatic("too far right", 1);
        //    if (remainderMoveDistance > slowMotionMovementDistance /*|| remainderMoveDistance < (gapX - 20f)*/)
        //    {
        //        remainderMoveDistance = slowMotionMovementDistance;
        //    }
        //    //// If less than 20 units to go until the next gapX segment, move the remainder
        //    //else
        //    //{
        //    //    remainderMoveDistance = transform.localPosition.x;
        //    //}

        //    SubstractScaleByScrollEdge(remainderMoveDistance);
        //}
        if (remainderMoveDistance >= gapX / 2)
        {
            isMoveRight = true;
        }
        else
        {
            isMoveRight = false;
        }

       // CCScreenLogger.LogStatic("absLocalPositionX: " + absLocalPositionX, 0);
       // CCScreenLogger.LogStatic("remainderMoveDistance: " + remainderMoveDistance, 1);
       // CCScreenLogger.LogStatic("isMoveRight: " + isMoveRight, 2);
       // CCScreenLogger.LogStatic("index: " + index, 5);
       // CCScreenLogger.LogStatic("touchGap: :" + touchGap, 6);

        //if (touchGap > 0)

        if (isMoveRight && index <= totalItems)
        {
            //float gapValue = Mathf.Abs(touchGap / 100);
            //if (counter < gapValue && !isRightScale)

            //float remainderMoveDistance = absLocalPositionX % gapX;
            // Move 20 units when doing slow motion
	
            if (remainderMoveDistance > slowMotionMovementDistance /*|| remainderMoveDistance < (gapX - 20f)*/)
            {
                remainderMoveDistance = slowMotionMovementDistance;
		
            }
            // If less than 20 units to go until the next gapX segment, move the remainder
            else if (remainderMoveDistance > (gapX - slowMotionMovementDistance))
            {
                remainderMoveDistance = gapX - remainderMoveDistance;
            }

            //if (!isRightScale)
            //{
                // transform.Translate(-0.01f, 0, 0);
                AddScaleByScroll(remainderMoveDistance);
                //this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - counter);
                //counter += 0.1f;

            //}
            resetPosition();
        }
        //else if (touchGap < 0)
        else if (index > 0)
        {
            //float gapValue = Mathf.Abs(touchGap / 100);
            //if (counter < gapValue && !isLeftScale)

            //float remainderMoveDistance = absLocalPositionX % gapX;
            // Move 20 units when doing slow motion

            if (remainderMoveDistance > slowMotionMovementDistance /*|| remainderMoveDistance < (gapX - 20f)*/)
            {
                remainderMoveDistance = slowMotionMovementDistance;
            }
            // If less than 20 units to go until the next gapX segment, move the remainder
            else if (remainderMoveDistance > (gapX - slowMotionMovementDistance))
            {
                remainderMoveDistance = gapX - remainderMoveDistance;
            }

            //if (!isLeftScale)
            //{
                // transform.Translate(0.01f, 0, 0);
                SubstractScaleByScroll(remainderMoveDistance);
                //this.gameObject.transform.localPosition = new Vector2(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y - counter);
                //counter += 0.1f;
            //}
            resetPosition();
        }

    }

}