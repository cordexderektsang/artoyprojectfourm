﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SetupNumberOfLanguage : MonoBehaviour {

	//public int numberOfLanguage =4;
	public ToyCoreDownLoadScript toyCoreDownLoadScript;
	public GameObject languageBar;
	public Transform langageWrapper;
	public List<UILabel> allLabel = new List<UILabel> ();
	public ButtomFlow buttomFlowScript;
	public bool isLocal = true;
	private const string UISprite_SelectLanguage ="UISprite_SelectLanguage";
	//private const string UILabel_LanguageName ="UILabel_LanguageName";
	private const string UISprite_DownloadButton ="UISprite_DownloadButton";



	// Use this for initialization
	//set font ,label// json set font for each language



	void Awake (){
		if (!isLocal) {
			LoadJson.languageFunction += onCallSetUpNumberOfLanguage;
		} 
	}

	public UISprite OnSelectedLanguageSpriteReference { get; private set; }

	void onCallSetUpNumberOfLanguage () {
		Debug.Log ("onCallSetUpNumberOfLanguage Function");
		foreach (LanguageJsonData data in  LoadJson.LocalLanguageJsonDataReference) {
			GameObject target = Instantiate (languageBar);
			CoreNguiButtonAddAndRemove.AddParameterFunction (buttomFlowScript, new object[] {
				target.transform.FindChild (UISprite_SelectLanguage).GetComponent<UISprite> (),
				langageWrapper
			}, target.GetComponent<UIButton> (), "OnClickLanguageSelection");

			target.transform.parent = langageWrapper.transform;
			target.name = languageBar.name;
			target.transform.localScale = new Vector3 (1, 1, 1);
			target.transform.localPosition = Vector3.zero;
			target.transform.localEulerAngles = Vector3.zero;
			UITexture texture = target.GetComponent<UITexture> ();
			UIWidget widget = target.GetComponent<UIWidget>();
			widget.depth = 1;
			if (data.isLocal)  {
				//Debug.Log ("Language Name : " +data.languageName + " : isLocal : " + data.isLocal);
				texture.mainTexture = Resources.Load (data.languageName) as Texture2D;
			} else {
				//string DirectionaryPath = data.texturePath + data.textureFileName;
				string DirectionaryPath = Application.persistentDataPath + ToyCoreDownLoadScript.PictureFolder + "/" + data.languageName+ GlobalStaticVariable.PNG;
			//	Debug.Log ("Language Name : " +data.languageName + " : isLocal : " + data.isLocal + " Path " + DirectionaryPath);
				texture.mainTexture = SetupNumberOfScrollObject.LoadPNG(DirectionaryPath);
				texture.mainTexture.name = data.languageName;
			}

			if (!data.isLocal) {
				string BundlePathName = Application.persistentDataPath +  ToyCoreDownLoadScript.LanguageFolder  + "/"+ data.fontPackageName +GlobalStaticVariable.AssetBundle_EXT;
				string SumExtPathName = Application.persistentDataPath +  ToyCoreDownLoadScript.LanguageFolder  + "/"+ data.fontPackageName +GlobalStaticVariable.AssetBundle_EXT +GlobalStaticVariable.CHECKSUM_EXT;
				UIButton downloadButton = target.transform.FindChild (UISprite_DownloadButton).GetComponent<UIButton> ();
				if (!File.Exists (BundlePathName) || !File.Exists (SumExtPathName)) {
					/*CoreNguiButtonAddAndRemove.AddParameterFunction (toyCoreDownLoadScript, new object[] {
						data.fontPackageName.ToLower ()
					}, downloadButton, "DownloadLanguagePack");*/
					CoreNguiButtonAddAndRemove.AddParameterFunction(buttomFlowScript, new object[] {
						data.fontPackageName.ToLower () ,data.languageName
					}, downloadButton, "OnEnableDownLanguagePopUp");
					downloadButton.transform.gameObject.SetActive (true);
					target.GetComponent<BoxCollider>().enabled = false;
				} else {
					downloadButton.transform.gameObject.SetActive (false);
				}
			}
			if (data.languageName.Equals (CoreLanguage.LanguagePref)) {
				Debug.Log ("Default lanuage set to " + CoreLanguage.LanguagePref);
				UISprite sprite = target.transform.FindChild(UISprite_SelectLanguage).GetComponent<UISprite>();
				sprite.spriteName = ButtomFlow.LanguageUISelectedName;

			

			} else {
				target.transform.FindChild (UISprite_SelectLanguage).GetComponent<UISprite> ().spriteName = ButtomFlow.LanguageUIUnSelectedName;
			}

			langageWrapper.GetComponent<UIGrid> ().enabled = true;
		}
		
	}

	void OnDestroy() 
	{
		LoadJson.languageFunction -= onCallSetUpNumberOfLanguage ;
	}

	public void CheckLanguage()
	{

		foreach (Transform child in langageWrapper.transform)
		{
			string language = child.GetComponent<UITexture>().mainTexture.name;
			LanguageJsonData package = returnPackageName(language);
			string BundlePathName = Application.persistentDataPath + ToyCoreDownLoadScript.LanguageFolder + "/" + package.fontPackageName + GlobalStaticVariable.AssetBundle_EXT;
			string SumExtPathName = Application.persistentDataPath + ToyCoreDownLoadScript.LanguageFolder + "/" + package.fontPackageName + GlobalStaticVariable.AssetBundle_EXT + GlobalStaticVariable.CHECKSUM_EXT;
			UIButton downloadButton = child.transform.FindChild(UISprite_DownloadButton).GetComponent<UIButton>();
			//	Debug.Log(child.GetComponent<UITexture>().mainTexture.name + " : " + BundlePathName + " : " + File.Exists(BundlePathName));
			//	Debug.Log(child.GetComponent<UITexture>().mainTexture.name + " : " + SumExtPathName + " : " + File.Exists(SumExtPathName));
			child.GetComponent<UIWidget>().depth = 2;
			if (child.GetComponent<UITexture>().mainTexture.name.Equals(CoreLanguage.DefaultLanguage))
			{
				downloadButton.transform.gameObject.SetActive(false);
				child.gameObject.GetComponent<BoxCollider>().enabled = true;

			} else if ((!File.Exists(BundlePathName)&& !language.Equals("English"))|| (!File.Exists(SumExtPathName)&& !language.Equals("English")))
			{
				downloadButton.transform.gameObject.SetActive(true);
				child.gameObject.GetComponent<BoxCollider>().enabled = false;
			}
			else {
				downloadButton.transform.gameObject.SetActive(false);
				child.gameObject.GetComponent<BoxCollider>().enabled = true;
			}
//			Debug.Log("Default lanuage set to " + CoreLanguage.LanguagePref + "  : " + language);

			if (language.Equals(CoreLanguage.LanguagePref))
			{
				Debug.Log("Default lanuage set to " + CoreLanguage.LanguagePref);
				child.transform.FindChild(UISprite_SelectLanguage).GetComponent<UISprite>().spriteName = ButtomFlow.LanguageUISelectedName;


			}
			else {
				child.transform.FindChild(UISprite_SelectLanguage).GetComponent<UISprite>().spriteName = ButtomFlow.LanguageUIUnSelectedName;
			}

		

		}


	}

	public LanguageJsonData returnPackageName(string name)
	{

		foreach (LanguageJsonData data in LoadJson.LocalLanguageJsonDataReference)
		{

			if (data.languageName.Equals(name))
			{
				return data;

			}

		}
		return null;
	}
	
	// Update is called once per frame
}
