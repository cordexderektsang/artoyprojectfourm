﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleAndRotation : MonoBehaviour {

	[Range (0.01f, 2f)]
	public float rotateAmount = 1;
	[Range (0.01f, 2f)]
	public float maxTimeForClickHold =0.2f;
	public LayerMask myLayerMaskForDefault; 
	public LayerMask myLayerMaskForRotation;
	public GameObject localTranform;

	private float lastPosX = 0;
	private float lastPosY = 0;
	private float ROTATE_SPEED = 0.2f;
	private float FirstPressPosition = 0;
	private bool checkForRotation = false;
	private bool isMultPress = false;
	private float counterTimeForClickHold = 0;
	private bool isMult = false;
	private Vector2 lastposVector = Vector2.zero;
	public GameObject currentPlan;


	bool isMouseClick = false;
	bool isMouseUp = false;
	bool mouseHold = false;
	int numberOfTouch = 0;



	/// <summary>
	/// Raycasts the hit.
	/// play animation if mouse up 
	/// check is it Hold or fast mouse click
	/// </summary>
	public  void RaycastHit ()
	{
		numberOfTouch = Input.touchCount;
		isMouseClick = Input.GetMouseButtonDown (0);
		isMouseUp = Input.GetMouseButtonUp (0);
		mouseHold = IsMouseHold();
		isMult = isMultTouch (numberOfTouch);
		if (isMouseClick) {

			RaycastHit hit;
			if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit,30000,myLayerMaskForDefault)) {
				GlobalStaticVariable.RayhitObject = hit.collider.gameObject;
				if(GlobalStaticVariable.RayhitObject.transform.parent.FindChild(SetUpImageTarget.Plan) != null) {
				currentPlan = GlobalStaticVariable.RayhitObject.transform.parent.FindChild(SetUpImageTarget.Plan).gameObject;
				currentPlan.SetActive(true);

				}
			//	Debug.Log ("ray cast hit : " + GlobalStaticVariable.RayhitObject.name);

			} else {
				//GlobalStaticVariable.RayhitObject = null;
			}
		}

		if (isMouseUp  && !isMult) {
			PlayTargetAnimation (mouseHold);
			GlobalStaticVariable.RayhitObject = null;

		}

	}

	public void RaycastHitForRotation()
	{
		//LayerMask mask = 1 >> 10;
		if (Input.GetMouseButton(0))

		{

			RaycastHit hit;
			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit,3500,myLayerMaskForRotation))
			{
				//GameObject hitLayer = hit.collider.gameObject;
				//Debug.Log(hitLayer.name);
				//Debug.Log(hit.transform.gameObject.name);
				localTranform.transform.position = hit.point;
				localTranform.transform.SetParent(hit.collider.transform.parent);
				//Vector2 firstPoint = 	hit.point;
				Vector2 firstPoint = localTranform.transform.localPosition;
				//	Debug.Log ("ray cast hit : " + GlobalStaticVariable.RayhitObject.name);
				if (lastposVector != Vector2.zero)
				{
					//float distance = Vector2.Distance(firstPoint, lastposVector);
					//float distance = firstPoint.x - lastposVector.x;
					float distance = firstPoint.x - lastposVector.x;
					//Debug.Log("hit Point + " + hit.point + " distance " + distance );
					RotateObject(distance*150);
				}
				lastposVector = firstPoint;

			}
			else {
				//GlobalStaticVariable.RayhitObject = null;
			}
		}



	}



	/// <summary>
	/// Plaies the target animation.
	/// </summary>
	/// <param name="mouseHold">If set to <c>true</c> mouse hold.</param>
	public void PlayTargetAnimation (bool mouseHold)
	{
		if (GlobalStaticVariable.RayhitObject == null) {

			return;
		}
		//Debug.Log (CoreController.isDoubleClick);

		if (!mouseHold) {
			AnimatorCoreController animatorCoreController = GlobalStaticVariable.RayhitObject.GetComponent<AnimatorCoreController> ();
			Animator thisAnimator = animatorCoreController.animation;
			if (thisAnimator != null) {
				//Debug.Log ("Call Animation" + GlobalStaticVariable.RayhitObject);
				animatorCoreController.CallForAnimation (GlobalStaticVariable.RayhitObject.name);
			//	AudioListController.instance.PickARandomSoundAndPlaySound(GlobalStaticVariable.RayhitObject.name);
				//if(GlobalStaticVariable.RayhitObject.GetComponent<>();)
				//PickARandomSoundAndPlaySound();
		
			}
		}
	}


	/// <summary>
	/// check is it mult touch
	/// </summary>
	/// <returns><c>true</c>, if mult touch was ised, <c>false</c> otherwise.</returns>
	/// <param name="number">Number.</param>
	private bool isMultTouch (int number)
	{
		if (number >= 2) {
			
			isMultPress = true;
			return true;

		} else if (number == 1 && isMultPress) {


			return true;

		} else if (number == 1 && !isMultPress) {

			isMultPress = false;
			return false;

		} else if(number == 0 ){

			isMultPress = false;
			return false;

		}
		return false;
	}

	public void Rotation()
	{
		// RotationFunction();
//		Debug.Log(checkRotation);

	}

	/*public void Rotation()
	{

		if (Input.touchCount == 1 ) {

			if (lastPosX != 0) {
				float x = Input.GetTouch (0).position.x - lastPosX;

				RotateObject (x,false);
			}
			lastPosX = Input.GetTouch (0).position.x;

		} else if ( Input.GetMouseButton (0) ) {

			if (lastPosX != 0) {
				float x = lastPosX - Input.mousePosition.x;
				RotateObject (x,false);
			}
			lastPosX = Input.mousePosition.x;

		} 
	}*/






	public void OnTouchEnd()
	{

#if UNITY_EDITOR
		if (Input.GetMouseButtonUp(0))
		{
			lastPosX = 0;
			lastPosY = 0;
			lastposVector = Vector2.zero;
			localTranform.transform.parent = null;
			if (currentPlan != null)
			{
				currentPlan.SetActive(false);
				currentPlan = null;
			}

		}
#endif
#if !UNITY_EDITOR
		if (Input.touchCount == 0)
		{
			lastPosX = 0;
			lastPosY = 0;
			lastposVector = Vector2.zero;
			localTranform.transform.parent = null;
			if (currentPlan != null)
			{
			currentPlan.SetActive(false);
			currentPlan = null;
			}
		}
#endif
	}

	/// <summary>
	/// Determines whether this instance is mouse hold.
	/// </summary>
	/// <returns><c>true</c> if this instance is mouse hold; otherwise, <c>false</c>.</returns>
	private bool IsMouseHold ()
	{

		#if !UNITY_EDITOR
		if (Input.touchCount == 1 && Input.GetMouseButton (0)) {

		if (counterTimeForClickHold < maxTimeForClickHold) {
		counterTimeForClickHold += Time.deltaTime;

		} else {
		return true;
		}
		} else {
		if(counterTimeForClickHold < maxTimeForClickHold){

		counterTimeForClickHold = 0;
		return false;
		} else {
		Debug.Log("Time up");
		counterTimeForClickHold = 0;
		return true;
		}
		}
		#endif
		#if UNITY_EDITOR

		if (Input.GetMouseButton (0)) {

			if (counterTimeForClickHold < maxTimeForClickHold) {
				counterTimeForClickHold += Time.deltaTime;

			} else {
				return true;
			}
		} else {
			if(counterTimeForClickHold < maxTimeForClickHold){
				counterTimeForClickHold = 0;
				return false;
			} else {

				counterTimeForClickHold = 0;
				return true;
			}
		}

		return false;
		#endif
	return false;

	}

/*	public void RotationFunction()
	{
		float x = 0;
		float y = 0;
		bool isInverse = checkGyro();
		if (Input.touchCount == 1)
		{
		if (lastPosX != 0)
			{
				 x = Input.GetTouch(0).position.x - lastPosX;

			
			}
			if (lastPosY != 0)
			{
				 y = Input.GetTouch(0).position.y - lastPosY;


			}

			if (x != 0 || y != 0)
			{
				float valueAbsX = Mathf.Abs(x);
				float valueAbsy = Mathf.Abs(y);
				//Debug.Log("valueAbsXx : " + valueAbsX);
				//Debug.Log("valueAbsyy : " + valueAbsy);
				if (valueAbsX > valueAbsy)
				{
					Debug.Log("rotation X");
					RotateObject(x, isInverse);
				}
				else {
					Debug.Log("rotation Y");
					RotateObject(y, isInverse);
				}
			}
			lastPosX = Input.GetTouch(0).position.x;
			lastPosY = Input.GetTouch(0).position.y;

			if (lastPosX != 0)
			{
				x = Input.GetTouch(0).position.x - lastPosX;

			}
			if (lastPosY != 0)
			{
				y = Input.GetTouch(0).position.y - lastPosY;

			}
			//	if (lastPosX != 0 || y != 0)
			//if (x != 0 || y != 0)
			//{
			if (lastposVector != Vector2.zero)
			{
				float fingerCurrentDistance = 0;
				float valueCheck = 0;
				if (x != 0 || y != 0)
				{
					float valueAbsX = Mathf.Abs(x);
					float valueAbsy = Mathf.Abs(y);
					if (valueAbsX > valueAbsy)
					{
						valueCheck = x;
					}
					else {

						valueCheck = y;
					}
				}
				if (valueCheck > 0)
				{
					Debug.Log("bigger " + fingerCurrentDistance);
					fingerCurrentDistance = Vector2.Distance(Input.GetTouch(0).position, lastposVector);
				}
				else {
					Debug.Log("smaller " + fingerCurrentDistance);
					fingerCurrentDistance = Vector2.Distance(Input.GetTouch(0).position, lastposVector) * -1;
				}

				//Debug.Log("twoFingerCurrentDistance : " + fingerCurrentDistance);
				RotateObject(fingerCurrentDistance, false);
			}

			lastposVector = Input.GetTouch(0).position;
			lastPosX = Input.GetTouch(0).position.x;
			lastPosY = Input.GetTouch(0).position.y;

		

		}
		else if (Input.GetMouseButton(0))
		{

				if (lastPosX != 0)
				{
					 x = lastPosX - Input.mousePosition.x;

				}
				if (lastPosY != 0)
				{
					 y = lastPosY - Input.mousePosition.y;

				}
			//	if (lastPosX != 0 || y != 0)
			//if (x != 0 || y != 0)
			//{
			if (lastposVector != Vector2.zero)
			{
				float fingerCurrentDistance = 0;
				float valueCheck= 0;
				float valueAbsX = 0;
				float valueAbsy = 0;
				//float valueDegreee = 0;
				if (x != 0 || y != 0)
				{
					 valueAbsX = Mathf.Abs(x);
					 valueAbsy = Mathf.Abs(y);
					if (valueAbsX >= valueAbsy)
					{
						valueCheck = x;
					}
					else {

						valueCheck = y;
					}
				}
				if (valueCheck > 0)
				{
					fingerCurrentDistance = Vector2.Distance(Input.mousePosition, lastposVector) * -1;
				}
				else {

					fingerCurrentDistance = Vector2.Distance(Input.mousePosition, lastposVector);
				}
				//Debug.Log("fingerCurrentDistance : " + fingerCurrentDistance + " valueAbsX " + valueAbsX + " valueAbsy " + valueAbsy );
				//Debug.Log("twoFingerCurrentDistance : " + fingerCurrentDistance);
				RotateObject(fingerCurrentDistance, false);
			}

			lastposVector = Input.mousePosition;
			lastPosX = Input.mousePosition.x;
			lastPosY = Input.mousePosition.y;

		}
	

	}*/



	 float GetAngleInDegrees(float x, float y)
	{
		return GetAngleInRadians(x, y) / Mathf.PI * 180;
	}

	/// <summary>
	/// Calculate the angle relative to position (0, 0) given coordinates <paramref name="x"/> and <paramref name="y"/>.
	/// </summary>
	/// <returns>
	/// A float representing the angle in radians.
	/// </returns>
	/// <param name="x">
	/// The x coordinate
	/// </param>
	/// <param name="y">
	/// The y coordinate
	/// </param>
	  float GetAngleInRadians(float x, float y)
	{
		return Mathf.Atan2(y, x);
	}

	public bool checkGyro()
	{
		Quaternion gyroValue = Input.gyro.attitude;
		//Debug.Log("Gyrco x : " + gyroValue.eulerAngles.x + "Gyrco y : " + gyroValue.eulerAngles.y +  "Gyrco z : " + gyroValue.eulerAngles.z);
		if (gyroValue.eulerAngles.z > 181 && gyroValue.eulerAngles.z < 300)
		{
			Debug.Log("True");
			return true;
		}
		else {
			return false;
		}

	}

	/// <summary>
	/// Rotates the object.
	/// </summary>
	/// <param name="distance">Distance.</param>
	private void RotateObject(float distance/*, bool isinverse*/)
	{
		if (GlobalStaticVariable.RayhitObject != null && GlobalStaticVariable.RayhitObject.GetComponent<AnimatorCoreController> () != null && GlobalStaticVariable.RayhitObject.GetComponent<ControllerVariable> () != null && !isMult) {
			if (GlobalStaticVariable.RayhitObject.GetComponent<AnimatorCoreController> ().animatorStatus != AnimatorCoreController.AnimationStatus.ACTIVE && GlobalStaticVariable.RayhitObject.GetComponent<ControllerVariable> ().isRotateAllowed) {
				//if (!isinverse)
				//{
			//	Debug.Log("l am Rotation " + distance);
					GlobalStaticVariable.RayhitObject.transform.Rotate(new Vector3(0, 0, -distance * ROTATE_SPEED), Space.Self);
				/*}
				else {
					GlobalStaticVariable.RayhitObject.transform.Rotate(new Vector3(0, 0, distance * ROTATE_SPEED), Space.Self);

				}*/
			}
		}
	}
}
