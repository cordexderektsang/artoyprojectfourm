﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmissionController : MonoBehaviour {

	#region Demo Varable
	//public bool scaleEffect = false;
	//public bool smokeEffect = false;
	//public bool lightEffect = false;
	//public bool rainbowEffect = false;
	//public bool smokeTailEffect = false;
	//public bool moveObject = false;
	//private float maxCounter = 0;
	//public float maxMoveTimer = 0.7f;
	//private Vector3 orignal;
	//private Coroutine coroutineForMove;
	public bool isScaleTransition = true;
	public AudioPlayScript audioPlayScript;
	#endregion

	#region private
	private Transform marketObject;
	private ParticleSystem transitionEffect;
	private ParticleSystem specialEffect;
	private GameObject transitionEffectObject;
	//private GameObject specialEffectObject;
	private Coroutine timeEffectTimer = null;
	public MarkerSetUpComponent markerSetUpComponentScript;
	//private TrailRenderer tailRender;
	//private const string tailRenderName = "TaillRainBow";



	/// <summary>
	///scale Object.
	/// </summary>
	private float initalObjectSize = 0.1f;
	private float scaleObjectFactor = 0.01f;
	private float endObjectSize = 1;
	private float defaultObjectSize = 1;
	private Coroutine coroutineForScale;

	/// <summary>
	/// Glow Intensive 
	/// </summary>
	//public Shader standard;
	//public Shader cutOff;
	//private float initalGlow = 10f;
	//private float glowIntensivetFactor = 0.2f;
	//private float endGlow = 0;
	//private Coroutine coroutineForGlow;


	//private int pathDistance = 5000;


	/// <summary>
	/// The time for repeat transition
	/// </summary>
	private float timeForRepeat = 4;
	[SerializeField]
	private float countTime = 0;
	public bool allowTransitionAnimation = true;

	#endregion

	#region public



	#endregion
	/// <summary>
	/// set up particle object for emission script 
	/// </summary>
	/// <param name="markerSetUpComponent">Marker set up component.</param>
	public void EmissionComponentSetUp (MarkerSetUpComponent markerSetUpComponent) 
	{
		//orignal = this.transform.localPosition;
		/*if (markerSetUpComponent.transitionEffect != null) {
			transitionEffect = markerSetUpComponent.transitionEffect.GetComponent<ParticleSystem> ();
		}*/
		//tailRender = this.transform.FindChild(tailRenderName).GetComponent<TrailRenderer>();
		/*if (markerSetUpComponent.specialEffect != null) {
			specialEffect = markerSetUpComponent.specialEffect.GetComponent<ParticleSystem> ();
		}*/
		transitionEffectObject = markerSetUpComponent.transitionEffect;
		//specialEffectObject = markerSetUpComponent.specialEffect;
		if (transitionEffectObject!= null) {
			transitionEffectObject.SetActive (false);
		}
		/*if (specialEffectObject != null) {
			specialEffectObject.SetActive (false);
		}*/
		markerSetUpComponentScript = markerSetUpComponent;
		marketObject = markerSetUpComponent.markRenderObject;
		endObjectSize = marketObject.transform.localScale.x;
		SetUpAudioComponentScript();

		//standard = Shader.Find("Mobile/Bumped Specular");;
		//cutOff= Shader.Find("MK/Glow/Selective/Mobile/Diffuse");;

	}

	public void SetUpAudioComponentScript()
	{
		if (audioPlayScript == null)
		{
			AudioPlayScript[] audioSetupComponent = GetComponentsInChildren<AudioPlayScript>(true);

			if (audioSetupComponent.Length > 0)
			{
				audioPlayScript = audioSetupComponent[0];
			}
		}


	}

	public void ResetTimeEffectCounter()
	{
		countTime = 0;

		if (timeEffectTimer != null) {
			StopCoroutine (timeEffectTimer);
			timeEffectTimer = null;
		}
		allowTransitionAnimation = false;
	}



	/// <summary>
	/// Time Counter For Transition to Happen again
	/// </summary>
	public void TimeForEffectRepeat ()
	{
		countTime = 0;
		if (timeEffectTimer == null && this.gameObject.activeSelf ) {
			timeEffectTimer = StartCoroutine (TimeRepeatCountFunction ());
		}
		
	
	}





	/*public void StartSmokeTail (bool status)
	{
		//tailRender.Clear ();
		//tailRender.time = 0;
		if (specialEffectObject != null) {
			specialEffectObject.SetActive (status);
			/*if (!specialEffectObject.activeSelf) {
				specialEffectObject.SetActive (true);
				//moveEffect ();
			} else {
				specialEffectObject.SetActive (false);
				//this.transform.localPosition = orignal;
			}
		}
	}*/

	/*public void StartRainBowTail ()
	{
		this.transform.localPosition = orignal;
		tailRender.Clear ();
		tailRender.time = pathDistance;
		moveEffect ();
	}*/

	/// <summary>
	/// Scales the Unicorn.
	/// </summary>
	public void StartScaleFunction ()
	{
		//tailRender.Clear ();
		//tailRender.time = 0;
		if (coroutineForScale == null && marketObject != null && allowTransitionAnimation && isScaleTransition && this.gameObject.activeSelf) {
			Debug.Log ("emission scale");
			coroutineForScale = StartCoroutine (ScaleUpSize ());
		}
	}
		

	/// <summary>
	/// Start The Smoke Effect
	/// </summary>
	public void StartTransitionEmission (bool status) 
	{
		//this.transform.localPosition = orignal;
		//smokepSTail.Clear ();
		Debug.Log("call for emission ");
		if (status != true) {
			Debug.Log ("emission false");
			transitionEffectObject.SetActive (status);
		}
		if (coroutineForScale == null && transitionEffectObject != null && allowTransitionAnimation && this.gameObject.activeSelf) {
			Debug.Log ("emission true");
			audioPlayScript.GetAudio("Model Appear", "",false);
			transitionEffectObject.SetActive (status);
			/*if (!transitionEffectObject.activeSelf) {
				transitionEffectObject.SetActive (true);
			} else {
				transitionEffectObject.SetActive (false);
			}*/
		}
	}

	/// <summary>
	/// Start The Light Effect
	/// </summary>
	/*public void LighttingEffect ()
	{
		tailRender.Clear ();
		tailRender.time = 0;
		if (coroutineForGlow == null) {
			coroutineForGlow = StartCoroutine (GlowEffectFunction ());
		}

	}

	public void moveEffect ()
	{
		if (coroutineForMove == null) {
			coroutineForMove = StartCoroutine (MoveFunction ());
		}

	}*/

	/// <summary>
	/// Scales Up the size.
	/// </summary>
	/// <returns>The size.</returns>
	private IEnumerator ScaleUpSize (){
		//this.transform.localPosition = orignal;

		marketObject.localScale = new Vector3 (initalObjectSize,initalObjectSize,initalObjectSize);
		float scaleVariable = initalObjectSize;

		while (true) {

			yield return new WaitForSeconds (0.01f);
			if (scaleVariable < endObjectSize) {
				marketObject.localScale = new Vector3 (scaleVariable, scaleVariable, scaleVariable);
				scaleVariable += scaleObjectFactor;
			} else {
				marketObject.localScale = new Vector3 (endObjectSize, endObjectSize, endObjectSize);
				coroutineForScale = null;
				//StartCoroutine (ScaleDownSize ());
				break;
			}

		}

	}

	void OnDisable()
	{
		//tailRender.time = 0;
		if (coroutineForScale != null ) {
			Debug.Log("stop scale");
			StopCoroutine (coroutineForScale);
			coroutineForScale = null;
		}
		if (timeEffectTimer != null ) {
			StopCoroutine (timeEffectTimer);
			countTime = 0;
			allowTransitionAnimation = true;
			timeEffectTimer = null;
		}
		marketObject.localScale = new Vector3 (endObjectSize,endObjectSize,initalObjectSize);
	}

	/// <summary>
	/// Scales Down the size.
	/// </summary>
	/// <returns>The size.</returns>
	/*private IEnumerator ScaleDownSize (){
		float scaleVariable = endObjectSize;
		while (true) {
			yield return new WaitForSeconds (0.01f);
			if (scaleVariable > defaultObjectSize) {
				this.transform.localScale = new Vector3 (scaleVariable, scaleVariable, scaleVariable);
				scaleVariable -= scaleObjectFactor;
			} else {
				this.transform.localScale = new Vector3 (defaultObjectSize, defaultObjectSize, defaultObjectSize);
				coroutineForScale = null;
				break;
			}

		}

	}*/

	/// <summary>
	/// Times the repeat count function.
	/// </summary>
	/// <returns>The repeat count function.</returns>
	private IEnumerator TimeRepeatCountFunction (){
		Debug.Log ("time Function");
		while (true) {
			yield return new WaitForSeconds (0.01f);
			if (countTime < timeForRepeat) {
				countTime += Time.deltaTime;
			} else {
				if (markerSetUpComponentScript.animatorCoreController.animatorStatus != AnimatorCoreController.AnimationStatus.ACTIVE)
				{
					allowTransitionAnimation = true;
					this.transform.localEulerAngles = new Vector3 (0, 0, 0);
					this.transform.localScale = new  Vector3 (1, 1, 1);
					break;
				}
			}

		}

	}

	/// <summary>
	/// Lighttings the effect function.
	/// </summary>
	/// <returns>The effect function.</returns>
/*	private IEnumerator GlowEffectFunction (){
		initalGlow = 10;
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		foreach(Renderer unicorn in rendererComponents){
			if (unicorn.GetComponent<MeshRenderer> () != null) {
				Texture texture = unicorn.material.GetTexture ("_MainTex");
				unicorn.material.shader = cutOff;
				unicorn.material.SetFloat ("_MKGlowTexStrength", initalGlow);
				unicorn.material.SetTexture ("_MKGlowTex", texture);
				unicorn.material.SetTexture ("_MainTex", texture);
			}
		}
		this.transform.localPosition = orignal;
		while (true) {
			yield return new WaitForSeconds (0.01f);
			if (initalGlow > endGlow) {
				initalGlow -= glowIntensivetFactor;
				foreach(Renderer unicorn in rendererComponents){
					unicorn.material.SetFloat ("_MKGlowTexStrength", initalGlow);
				}
			} else {
				
				coroutineForGlow = null;
				foreach(Renderer unicorn in rendererComponents){
					if (unicorn.GetComponent<MeshRenderer> () != null) {
						Texture texture = unicorn.material.GetTexture ("_MainTex");
						unicorn.material.shader = standard;
						unicorn.material.SetTexture ("_MainTex", texture);
					}
				}
				break;
			}

		}

	}*/


	/// <summary>
	/// Lighttings the effect function.
	/// </summary>
	/// <returns>The effect function.</returns>
	/*private IEnumerator MoveFunction (){
		this.transform.localPosition = orignal;
		while (true) {
			yield return new WaitForSeconds (0.015f);
			if (maxCounter < maxMoveTimer) {
				maxCounter += Time.deltaTime;
				this.transform.localPosition += new Vector3 (0, 0.01f, 0);
			} else {
				maxCounter = 0;
				coroutineForMove = null;
				break;
			}

		}

	}*/
}
