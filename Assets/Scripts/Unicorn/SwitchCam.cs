﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.SceneManagement;
using CCARPlatformFramework.CCConstants;
using CCARPlatformFramework.CCDataHandling;
using CCARPlatformFramework.CCTools;

public class SwitchCam : MonoBehaviour {
	public GameObject frontRainbowObject;
	public GameObject backRainbowObject;
	public Animator animationForBack;
	public AnimatorCoreController animatorCoreControllerForRainbow;
	public Animator animationForFront;
	public GameObject directionForFront;
	public GameObject directionForBack;
	public GameObject rainbow;


	public GameObject frontRainbowObjectVu;
	public GameObject backRainbowObjectVu;
	public Animator animationForBackVu;
	public AnimatorCoreController animatorCoreControllerForRainbowVU;
	public Animator animationForFrontVu;
	public GameObject rainbowVu;

	void Awake ()
	{
		//VuforiaARController.Instance.RegisterVuforiaStartedCallback (RestartVuforia);
	}

	void Start ()
	{
		CameraDevice.CameraDirection currentDir = CameraDevice.Instance.GetCameraDirection();
		Debug.Log ("========"+ currentDir.ToString());
		SetUpRainBow ();
		//animatorCoreControllerForRainbow =  backRainbowObject.transform.parent.GetComponent<AnimatorCoreController>();

		//Animator [] animationForBackray  = backRainbowObject.GetComponentsInChildren<Animator>(true);
		//animationForBack = animationForBackray [0];
	
		//Animator []  animationForFrontray  = frontRainbowObject.GetComponentsInChildren<Animator>(true);
		//animationForFront = animationForFrontray [0];
		//VuforiaARController.Instance.UnregisterVuforiaStartedCallback (RestartVuforia);
		StartCoroutine (RestartVuforiaAgain ());

	}

	IEnumerator RestartVuforiaAgain()
	{
		yield return new WaitForEndOfFrame();
		Debug.Log ("test testtesttesttesttesttesttest");
		RestartVuforia ();
	}

	void SetUpRainBow()
	{
		string bundlePath = Application.persistentDataPath + ToyCoreDownLoadScript.ObjectsFolder ;
		string bundleFile =  "rainbow"+ GlobalStaticVariable.AssetBundle_EXT;
		string bundlePathVu = Application.persistentDataPath + ToyCoreDownLoadScript.ObjectsFolder;
		string bundleFileVu = "rainbowvu" + GlobalStaticVariable.AssetBundle_EXT;

		rainbow = Instantiate(DynamicLoadMarker.LoadAssetBundle (bundlePath,bundleFile));
		rainbowVu = Instantiate(DynamicLoadMarker.LoadAssetBundle(bundlePathVu, bundleFileVu));
		frontRainbowObject = rainbow.transform.FindChild ("front").gameObject;
		backRainbowObject = rainbow.transform.FindChild ("back").gameObject;
		animationForBack =	 backRainbowObject.GetComponentsInChildren<Animator>(true) [0];
		animationForFront =  frontRainbowObject.GetComponentsInChildren<Animator>(true) [0];
		animatorCoreControllerForRainbow = rainbow.GetComponent<AnimatorCoreController> ();


		frontRainbowObjectVu = rainbowVu.transform.FindChild("front").gameObject;
		backRainbowObjectVu = rainbowVu.transform.FindChild("back").gameObject;
		animationForBackVu = backRainbowObjectVu.GetComponentsInChildren<Animator>(true)[0];
		animationForFrontVu = frontRainbowObjectVu.GetComponentsInChildren<Animator>(true)[0];
		animatorCoreControllerForRainbowVU = rainbowVu.GetComponent<AnimatorCoreController>();

	}

	void RestartCamera() 
	{

		VuforiaARController.Instance.RegisterVuforiaStartedCallback (callBack);
	}


	public void RestartVuforia ()
	{
		
		CameraDevice.Instance.Stop();
		CameraDevice.Instance.Deinit();
		TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
		GameObject.FindObjectOfType<VuforiaBehaviour>().enabled = false;
		VuforiaBehaviour.Instance.enabled = false;
		VuforiaARController.Instance.SetWorldCenterMode(VuforiaARController.WorldCenterMode.CAMERA);
		GameObject.FindObjectOfType<VuforiaBehaviour>().enabled = true;
		VuforiaBehaviour.Instance.enabled = true;
		CameraDevice.Instance.Start();
		TrackerManager.Instance.GetTracker<ObjectTracker> ().Start ();
		Debug.Log ("restart image format");
		CameraDevice.Instance.SetFrameFormat(Image.PIXEL_FORMAT.GRAYSCALE, true);
	}
	 


	public void CamSwitch ()
	{
		RestartCamera();
	}

	private void callBack()
	{
		//front no probelm
		CameraDevice.CameraDirection currentDir = CameraDevice.Instance.GetCameraDirection();
		Debug.Log (currentDir.ToString());
		CameraDevice.Instance.Stop();
		CameraDevice.Instance.Deinit();
		TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();

#if UNITY_IOS

			if (frontRainbowObject.activeInHierarchy) {
				frontRainbowObject.SetActive (false);
				backRainbowObject.SetActive (true);
				//directionForBack.SetActive(true);
				//directionForFront.SetActive(false);
				animatorCoreControllerForRainbow.animation = animationForBack;


				frontRainbowObjectVu.SetActive (false);
				backRainbowObjectVu.SetActive (true);
			animatorCoreControllerForRainbowVU.animation = animationForBackVu;

	
			} else {
				frontRainbowObject.SetActive (true);
				backRainbowObject.SetActive (false);
				//directionForBack.SetActive(false);
				//directionForFront.SetActive(true);
				animatorCoreControllerForRainbow.animation = animationForFront;


				frontRainbowObjectVu.SetActive (true);
				backRainbowObjectVu.SetActive (false);
			animatorCoreControllerForRainbowVU.animation = animationForFrontVu;
			}
#endif
		if (currentDir == CameraDevice.CameraDirection.CAMERA_BACK || currentDir == CameraDevice.CameraDirection.CAMERA_DEFAULT) {
			CameraDevice.Instance.Init (CameraDevice.CameraDirection.CAMERA_FRONT);
//			frontRainbowObject.SetActive (true);
//			backRainbowObject.SetActive (false);
		} else {
			CameraDevice.Instance.Init (CameraDevice.CameraDirection.CAMERA_BACK);
//			frontRainbowObject.SetActive (false);
//			backRainbowObject.SetActive (true);
		}
		GameObject.FindObjectOfType<VuforiaBehaviour>().enabled = false;
		VuforiaBehaviour.Instance.enabled = false;
		VuforiaARController.Instance.SetWorldCenterMode(VuforiaARController.WorldCenterMode.CAMERA);

		GameObject.FindObjectOfType<VuforiaBehaviour>().enabled = true;
		VuforiaBehaviour.Instance.enabled = true;

		CameraDevice.Instance.Start();
		TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
		CameraDevice.Instance.SetFrameFormat(Image.PIXEL_FORMAT.GRAYSCALE, true);
	}




}
