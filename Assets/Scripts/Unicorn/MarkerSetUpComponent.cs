﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerSetUpComponent : MonoBehaviour {

	public Transform markRenderObject;
	public GameObject transitionEffect;
	//public GameObject specialEffect;
	public ParticleSystemRenderer transitionEffectRender;
	//public ParticleSystemRenderer specialEffectRender;
	public EmissionController emissionController;
	public AnimatorCoreController animatorCoreController;
	public 	Animator[] AnimatorComponent;
	public AudioPlayScript  audioPlayScript;
	// Use this for initialization
	public void Awake () 
	{
		Debug.Log ("call for set up : " + this.gameObject.name);
		emissionController = GetComponent<EmissionController> ();
		audioPlayScript = GetComponent<AudioPlayScript> ();
		//markRenderObject = ReturnTargetNameContainTransformChild (GlobalStaticVariable.MarkerRenderObject,this.transform).transform;// find marker display object
		markRenderObject = this.transform;
		AnimatorComponent = GetComponentsInChildren<Animator>(true);
		//animatorCoreController = markRenderObject.GetComponent<AnimatorCoreController> ();
		animatorCoreController = GetComponent<AnimatorCoreController> ();
		if (markRenderObject != null) {
			transitionEffect = ReturnTargetNameContainTransformChild (GlobalStaticVariable.Transition, this.transform);//find transition effect particle
			//specialEffect = ReturnTargetNameContainTransformChild (GlobalStaticVariable.SpecialEffect, markRenderObject);//find special effet particle
		} else {
			Debug.Log ("can't find markRenderObject or specialEffect object from parent"  );
		}
		if (transitionEffect != null) {
			transitionEffectRender = transitionEffect.GetComponent<ParticleSystemRenderer> (); //find particle render
		}
		/*if (specialEffect != null) {
			specialEffectRender = specialEffect.GetComponent<ParticleSystemRenderer> ();//find particle render
		}
		emissionController.EmissionComponentSetUp (this); // set up emission component
		/*if (markRenderObject != null && animatorCoreController != null) {
			if (markRenderObject.GetComponent<Animator> () != null) {
				animatorCoreController.AnimationStartUp (markRenderObject.GetComponent<Animator> ());//set up animator for marker object
			}
		}*/
		if (AnimatorComponent.Length > 0 && animatorCoreController != null) {
			animatorCoreController.AnimationStartUp (AnimatorComponent[0],audioPlayScript);

		}
		emissionController.EmissionComponentSetUp (this); // set up emission component

	}

	// find child object from marker
	private GameObject ReturnTargetNameContainTransformChild (string Name , Transform Object)
	{
		foreach (Transform target in Object){
			Debug.Log (target.name);
			if (target.name.Contains (Name)) {
				return target.gameObject;
			}
		}
		Debug.Log ("can't  : " + Name + " from parent name :" +  Object.name);
		return null;

	}

	public void setUpComponentForEmissionComponent()
	{
		emissionController = GetComponent<EmissionController>();
	}

}
