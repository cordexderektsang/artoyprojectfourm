﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shadow : MonoBehaviour {
	public GameObject bootRoot;
	public bool getYposition = true;
	float position = 0;
	Vector3 defaultScale;
	 public Material rend;
	public float distanceForShadowAllOff = 2;
	private float alpha = 0.65f;
	// Use this for initialization

	void Start ()
	{
		if (bootRoot != null) {
			defaultScale = this.transform.localScale;
			if (getYposition) {
				position = bootRoot.transform.localPosition.y;
			} else {
				position = bootRoot.transform.localPosition.x;
			}
			Renderer render = GetComponent<Renderer>();
			rend = render.material;
			distanceForShadowAllOff = distanceForShadowAllOff + position;
		
		}
	}
	// Update is called once per frame
	void Update () {

		if (bootRoot != null) {
			float num = 0;
			if (getYposition) {
				num = bootRoot.transform.localPosition.y;
			} else {
				num = bootRoot.transform.localPosition.x;
			}
			float percentage = num / distanceForShadowAllOff;
			//Debug.Log (percentage);
			float value = alpha - percentage;
			//Debug.Log (value);
			if (value < 0) {
				value = 0;
			}
		
			//	Debug.Log (value);
			//	this.transform.localScale = new Vector3 (num,num,num);
			rend.SetColor ("_Color", new Color (1, 1, 1, value));
		
		} 
	}
}
