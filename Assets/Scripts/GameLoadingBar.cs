﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLoadingBar : MonoBehaviour {

	#region menu download Item

	public float loadingProgress = 0;
	private bool startLoadGameStart = true;

	#endregion


	#region download loading  Item

	public UIPanel panel;
	private Vector2 widthPanel;
	private const int sizePanel = 1121;
	private const int startPanel = -858;
	private int gap;
	#endregion


	#region download loading  Item

	public LoadingPageTexture backgroundLoadingPage;
	public LoadingPageTexture middlePictureLoadingPage;
	public UIController uiController;

	#endregion
	// Use this for initialization

	/// <summary>
	/// reset Loading bar to start
	/// </summary>
	public void resetLoadingPage ()
	{
		setPanelRect (startPanel);

	}

	public void FinishLoadingPage (){


		setPanelRect (sizePanel);
	}

	public void setPanelRect (float x)
	{

		panel.SetRect (x, 1, 2100, 32);

	}


	public void ChangeTextureForBackGroundLoadingPage (string name)
	{
		backgroundLoadingPage.uiName = name;
		backgroundLoadingPage.CallForLoadingTextureSearchByName ();

	}

	public void ChangeTextureForImageLoadingPageByName (string name)
	{
		middlePictureLoadingPage.uiName = name;
		middlePictureLoadingPage.CallForLoadingTextureSearchByName ();

	}

	public void ChangeTextureForImageLoadingPageByTexture (Texture texture)
	{
		middlePictureLoadingPage.CallForLoadingTextureSearchByTexture (texture);

	}


	#region appSceneLoadingUX


	public void StartFadeLoading ()
	{
		if (this.gameObject.activeSelf) {
			StartCoroutine (StartLoading ());
		}

	}


	public IEnumerator StartLoading (){
		Debug.Log ("Start loading");
		gap = sizePanel - startPanel;
		loadingProgress = 0;
		while (true) {
			yield return new WaitForSeconds (0.2f);
			if (loadingProgress < LoadJson.LocalObjectJsonDataReference.Count) {
				LoadProgressForScene (1);
			} else {
				break;
			}
		}
	}
	public void LoadProgressForScene (float progress)
	{
		setPanelRect(startPanel);
		float addInFactor =  (float)gap / (float) LoadJson.LocalObjectJsonDataReference.Count ;
		float addOn = addInFactor * loadingProgress;
		if (progress == 1) {

				float progressConvert = (addInFactor * progress)+ addOn ;
				float moveValue = startPanel + progressConvert;
				setPanelRect (moveValue);
				loadingProgress += 1;

		}
		//Debug.Log ("LoadScene : " + GlobalStaticVariable.NumberOfItemToBeDownloadForScene + " progress "  + progress  + "loadProgressForAppStart " + loadingProgress);
		if (loadingProgress == LoadJson.LocalObjectJsonDataReference.Count -1) {
			setPanelRect (sizePanel);
			Debug.Log ("Done : ");
			uiController.gameObject.SetActive (true);
			uiController.FrontPage ();

		}

	}


	#endregion


}
