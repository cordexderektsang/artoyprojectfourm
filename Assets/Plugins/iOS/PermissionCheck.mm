//
//  PermissionCheck.m
//  PermissionCheck
//
//  Created by cordexkau on 19/9/2016.
//  Copyright © 2016 cordexkau. All rights reserved.
//

#import "PermissionCheck.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <Social/Social.h>
#import <Foundation/NSException.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>
@interface PermissionCheck ()

@end

@implementation PermissionCheck


- (void)viewDidLoad {
    [super viewDidLoad];
}


- (id)init
{
    self = [super init];
    PermissionCheck = self;
    return self;
}

-(void)CheckCameraPermission: (const char* )_callbackObjectName: (const char* )_callbackFunctionName
{
    NSLog(@"check camera");
    callbackObjectName = [NSString stringWithUTF8String:_callbackObjectName];
    callbackFunctionName = [NSString stringWithUTF8String:_callbackFunctionName];
    const char* callbackObjectNameChar  = [callbackObjectName cStringUsingEncoding:NSUTF8StringEncoding];
    const char* callbackFunctionNameChar  = [callbackFunctionName cStringUsingEncoding:NSUTF8StringEncoding];
    
    if ([AVCaptureDevice respondsToSelector:@selector(requestAccessForMediaType:completionHandler:)]) {
        // Completion handler will be dispatched on a separate thread
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (YES == granted) {
                    // User granted access to the camera, continue with app launch
                    NSString* result = @"true";
                    const char* resultChar  = [result cStringUsingEncoding:NSUTF8StringEncoding];
                    NSLog(@"resulta");
                    
                    UnitySendMessage("Controller", callbackFunctionNameChar, resultChar);
                }
                else {
                    NSLog(@"resultb");
                    NSString* result = @"false";
                    const char* resultChar  = [result cStringUsingEncoding:NSUTF8StringEncoding];
                    UnitySendMessage("Controller", callbackFunctionNameChar, resultChar);
                }
            });
        }];
    }
    else
    {
        NSString* result = @"true";
        const char* resultChar  = [result cStringUsingEncoding:NSUTF8StringEncoding];
        UnitySendMessage("Controller", callbackFunctionNameChar, resultChar);
    }
    
    
    //    if(status == ALAuthorizationStatusAuthorized)
    //    {
    //        // start card-io
    //    }
    //    else if(status == ALAuthorizationStatusNotDetermined)
    //    {
    //
    //        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
    //         {
    //             if(granted)
    //             {
    //                 //Start card-io
    //                 [self testIsNewCard];
    //             }
    //
    //         }];
    //    }
}


-(void)CheckPhotoLibraryPermission
{
    NSLog(@"check photo");
}

@end

static PermissionCheck* delegateObject = nil;

extern "C" {
    
    void _CheckCameraPermission (const char* callbackObjectName, const char* callbackFunctionName)
    {
        if (delegateObject == nil)
            delegateObject = [[PermissionCheck alloc] init];
        
        [delegateObject CheckCameraPermission: callbackObjectName: callbackFunctionName];
    }
    
    void _CheckPhotoLibraryPermission (const char* callbackObjectName, const char* callbackFunctionName)
    {
        if (delegateObject == nil)
            delegateObject = [[PermissionCheck alloc] init];
        
        //        [delegateObject CheckPhotoLibraryPermission: callbackObjectName: callbackFunctionName];
    }
    
}
