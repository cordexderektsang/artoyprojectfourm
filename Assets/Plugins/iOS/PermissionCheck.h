//
//  PermissionCheck.h
//  PermissionCheck
//
//  Created by cordexkau on 19/9/2016.
//  Copyright © 2016 cordexkau. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <AssetsLibrary/AssetsLibrary.h>

@interface PermissionCheck : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    PermissionCheck *PermissionCheck;
    NSString* callbackObjectName;
    NSString* callbackFunctionName;
}

@end
//ALAssetsLibrary* library;

