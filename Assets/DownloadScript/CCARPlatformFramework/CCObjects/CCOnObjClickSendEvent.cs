using CCARPlatformFramework.CCObjects;
using UnityEngine;
using System.Collections.Generic;

namespace CCARPlatformFramework.CCObjects
{
    public class CCOnObjClickSendEvent : CCClickableObjectHandler
    {
        [System.Reflection.Obfuscation(ApplyToMembers = false)]
        public List<EventDelegate> onClickList = new List<EventDelegate>();

        static public CCOnObjClickSendEvent current;

        [System.Reflection.Obfuscation(ApplyToMembers = false)]
        public override void onClick()
        {
            if (current != null || onClickList.Count < 1) return;
            current = this;
            EventDelegate.Execute(onClickList);
            current = null;
        }
    }
}