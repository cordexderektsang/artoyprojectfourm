﻿using UnityEngine;
using System.Collections;

namespace CCARPlatformFramework.CCObjects
{
    public enum CCMarkerType
    {
        None = 0,
        Video = 1,
        AssetBundle = 2,
    }

    public class CCMarkerMetadata
    {
        // Marker component
        public int id = -1;
        public string markerName = "[NULL DATA]";
        public bool markerNeedDownload = false;
        public string other;
        public CCMarkerType type = CCMarkerType.None;
        public string bundleName;

        // Marker properties
        public string markerVersion;
        public Vector3 markerPosition = Vector3.zero;
        public Vector3 markerRotation = Vector3.zero;
        public Vector3 markerScale = Vector3.one;
        public string trackableHandlerName;

        // Marker object and properties
        public string markerObjectName = "";
        public Vector3 markerObjectPosition = Vector3.zero;
        public Vector3 markerObjectRotation = Vector3.zero;
        public Vector3 markerObjectScale = Vector3.one;
        public string markerObjectMaterialName = "";
        public string markerObjectShaderName = "";

        public IList components;

        //public bool requireAudio;
        //public string audioName;
        //public bool requireAudioDownload;
        //public string audioUrl;

        //public bool requireBlend;
        //public float targetBlendRate;
        //public float blendRateSpeed;

        //public override string ToString ()
        //{
        //	return  "id : " 		+ id 				+
        //			"\nName : " 	+ name 				+
        //			"\nPosition : " + position 			+
        //			"\nRotation : " + rotation 			+
        //			"\nScale : " 	+ scale 			+
        //               "\nVersion : "  + markerVersion     +
        //               "\nUrl : "      + url               +
        //               "\nType : " 	+ type.ToString ();
        //}
    }
}