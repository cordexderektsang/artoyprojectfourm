﻿namespace CCARPlatformFramework.CCObjects
{
    public class CCBundleMetadata
    {
        public string downloadName;
        public string version;
        public bool updated = false;
        public bool lightmapLoaded = false;
    }
}