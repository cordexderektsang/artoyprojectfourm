﻿using CCARPlatformFramework.CCInterfaces;
using System.Collections.Generic;

namespace CCARPlatformFramework.CCObjects
{
    public class CCTexts
    {
        // Dictionary of strings to be used
        private Dictionary<string, string> texts = new Dictionary<string, string>();

        public void add(string key, string text)
        {
            texts.Add(key, text);
        }

        public void modify(string key, string newText)
        {
            texts[key] = newText;
        }

        public string get(string key)
        {
            return texts[key];
        }

        public void remove(string key)
        {
            texts.Remove(key);
        }
    }
}