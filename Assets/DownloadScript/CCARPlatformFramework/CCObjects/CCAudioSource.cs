﻿using CCARPlatformFramework.CCInterfaces;
using UnityEngine;

namespace CCARPlatformFramework.CCObjects
{
    public class CCAudioSource : MonoBehaviour, CCIActionsEventHandler
    {
        // Single AudioSource for the object
        private AudioSource audioSource;

        public CCAudioSource()
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }

        public AudioClip clip
        {
            get
            {
                return audioSource.clip;
            }
            set
            {
                audioSource.clip = value;
            }
        }

        public void play()
        {
            Debug.Log("DEBUG: playing audio");
            audioSource.Play();
        }

        public void pause(bool pause)
        {
            if (pause)
            {
                audioSource.Pause();
            }
            else
            {
                audioSource.UnPause();
            }
        }

        public void playOneShot(AudioClip inClip)
        {
            audioSource.PlayOneShot(inClip);
        }

        public void playOneShot(AudioClip inClip, float inVolumeScale)
        {
            audioSource.PlayOneShot(inClip, inVolumeScale);
        }

        public void stop()
        {
            Debug.Log("DEBUG: stopping audio");
            audioSource.Stop();
            // Sanity check for reset
            audioSource.time = 0.0f;
        }

        public void OnTrackingFoundEvent()
        {
            play();
        }

        public void OnTrackingLostEvent()
        {
            stop();
        }
    }
}