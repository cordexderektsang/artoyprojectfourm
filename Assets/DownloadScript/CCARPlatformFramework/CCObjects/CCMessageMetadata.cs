﻿namespace CCARPlatformFramework.CCObjects
{
    public class CCMessageMetadata
    {
        public string qcarDownloadName;
        public string version;
        public bool metadataUpdated = false;
    }
}