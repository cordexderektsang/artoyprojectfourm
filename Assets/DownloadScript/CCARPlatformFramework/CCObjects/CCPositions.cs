﻿using CCARPlatformFramework.CCInterfaces;
using System.Collections.Generic;
using UnityEngine;

namespace CCARPlatformFramework.CCObjects
{
    public class CCPositions
    {
        // Dictionary of positions to be used for placement or movement
        private Dictionary<string, Vector3> positions;

        public void add(string key, Vector3 position)
        {
            positions.Add(key, position);
        }

        public void modify(string key, Vector3 newPosition)
        {
            positions[key] = newPosition;
        }

        public Vector3 get(string key)
        {
            return positions[key];
        }

        public void remove(string key)
        {
            positions.Remove(key);
        }
    }
}