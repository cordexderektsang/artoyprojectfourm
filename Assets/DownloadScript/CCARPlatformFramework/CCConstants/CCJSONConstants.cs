﻿namespace CCARPlatformFramework.CCConstants
{
    public class CCJSONConstants
    {
        // Shared constants
        public const string VERSION = "Version";
        public const string OTHER = "Other";
        public const string COMPONENTS = "Components";
        public const string BUNDLES = "Bundles";
        public const string NAME = "Name";

        // Trackable constants
        public const string CC_TRACKABLE_EVENT_HANDLER = "CCTrackableEventHandler";
        public const string DEFAULT_TRACKABLE_EVENT_HANDLER = "DefaultTrackableEventHandler";

        // Message constants
        public const string MESSAGE = "Message";
        public const string QCAR_URL = "QcarUrl";
        public const string QCAR_NAME = "QcarName";
        public const string ASSET_URL = "AssetUrl";
        public const string ASSET_NAME = "AssetName";

        // Lua constants
        public const string LUA_FILES = "LuaFiles";
        public const string SCRIPT_NAME = "ScriptName";
        public const string PARENT = "Parent";

        // Marker data constants
        public const string MARKERS = "Markers";
        public const string ID = "Id";
        public const string MARKER_NAME = "MarkerName";
        public const string MARKER_PROPERTIES = "MarkerProperties";
        public const string TYPE = "Type";
        public const string BUNDLE_NAME = "BundleName";
        public const string TRACKABLE_HANDLER_NAME = "TrackableHandlerName";

        // Marker object constants
        public const string MARKER_OBJECT_NAME = "MarkerObjectName";
        public const string MARKER_OBJECT_PROPERTIES = "MarkerObjectProperties";
        public const string POSITION = "Position";
        public const string ROTATION = "Rotation";
        public const string SCALING = "Scaling";
        public const string MATERIAL_NAME = "MaterialName";
        public const string SHADER_NAME = "ShaderName";
        public const string COMPONENT_OBJECT_PROPERTIES = "ComponentObjectProperties";

        //public const string AUDIO = "Audio";
        //public const string BLEND = "Blend";
    }
}