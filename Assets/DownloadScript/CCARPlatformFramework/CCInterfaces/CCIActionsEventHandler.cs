﻿namespace CCARPlatformFramework.CCInterfaces
{
    public interface CCIActionsEventHandler
    {
        [System.Reflection.Obfuscation(ApplyToMembers = false)]
        void OnTrackingFoundEvent();

        [System.Reflection.Obfuscation(ApplyToMembers = false)]
        void OnTrackingLostEvent();
    }
}