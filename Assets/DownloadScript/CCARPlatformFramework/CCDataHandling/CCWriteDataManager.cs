﻿using System.IO;

namespace CCARPlatformFramework.CCDataHandling
{
    // Make it not extendable
    public sealed class CCWriteDataManager
    {
        // Make it un-accessable
        private CCWriteDataManager()
        {
        }

        /// <summary>
        /// Format the specified directory and fileName into one path.
        /// </summary>
        /// <param name='directory'>
        /// Directory to save the file
        /// </param>
        /// <param name='fileName'>
        /// Name of the file
        /// </param>
        private static string Format(string directory, string fileName)
        {
            //bool isDirectoryEndWithSlash = directory.EndsWith("/");
            //bool isFileNameStartWithSlash = fileName.StartsWith("/");

            //if (isDirectoryEndWithSlash && isFileNameStartWithSlash)
            //{
            //    fileName = fileName.Substring(1);
            //}
            //else if (!isDirectoryEndWithSlash && !isFileNameStartWithSlash)
            //{
            //    directory = "/" + directory;
            //}
            //return directory + fileName;

            return Path.Combine(directory, fileName);
        }

        /// <summary>
        /// Check if the directory exists and create it if alsoCreateDirectory is set to true.
        /// </summary>
        /// <returns>
        /// True if directory exists
        /// </returns>
        /// <param name='directory'>
        /// Directory to create
        /// </param>
        /// <param name='alsoCreateDirectory'>
        /// If set to <c>true</c>, create the directory.
        /// </param>
        public static bool DirectoryExists(string directory, bool alsoCreateDirectory)
        {
            try
            {
                bool result = Directory.Exists(directory);
                if (!result && alsoCreateDirectory)
                {
                    result = CreateDirectory(directory);
                }
                return result;
            }
            catch (System.Exception e)
            {
             //   CCLog.SpecialMsg(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Check if file exists.
        /// </summary>
        /// <returns>
        /// True if file exists
        /// </returns>
        /// <param name='directory'>
        /// Directory to check for file
        /// </param>
        /// <param name='fileName'>
        /// Name of the file
        /// </param>
        public static bool FileExists(string directory, string fileName)
        {
            try
            {
                string path = Format(directory, fileName);
                bool result = File.Exists(path);
                return result;
            }
            catch (System.Exception e)
            {
              //  CCLog.SpecialMsg(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Check if file exists.
        /// </summary>
        /// <returns>
        /// True if file exists
        /// </returns>
        /// <param name='directoryFileName'>
        /// Directory and name of the file to check
        /// </param>
        public static bool FileExists(string directoryFileName)
        {
            try
            {
                bool result = File.Exists(directoryFileName);
                return result;
            }
            catch (System.Exception e)
            {
                //  CCLog.SpecialMsg(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Create the directory.
        /// </summary>
        /// <returns>
        /// True if no error when creating the directory
        /// </returns>
        /// <param name='directory'>
        /// Directory to create
        /// </param>
        public static bool CreateDirectory(string directory)
        {
            try
            {
                Directory.CreateDirectory(directory);
                return true;
            }
            catch (System.Exception e)
            {
              //  CCLog.SpecialMsg(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Write datas to path.
        /// </summary>
        /// <returns>
        /// True if no error when saving the file
        /// </returns>
        /// <param name='directory'>
        /// Directory to save the file
        /// </param>
        /// <param name='fileName'>
        /// Name of the file
        /// </param>
        /// <param name='bytes'>
        /// Bytes of file
        /// </param>
        public static bool WriteToPath(string directory, string fileName, byte[] bytes)
        {
            try
            {
                string path = Format(directory, fileName);
                File.WriteAllBytes(path, bytes);
                return true;
            }
            catch (System.Exception e)
            {
             //   CCLog.SpecialMsg(e.ToString());
                return false;
            }
        }

        /// <summary>
        /// Write datas to path.
        /// </summary>
        /// <returns>
        /// True if no error when saving the file
        /// </returns>
        /// <param name='directory'>
        /// Directory to save the file
        /// </param>
        /// <param name='fileName'>
        /// Name of the file
        /// </param>
        /// <param name='contents'>
        /// Text to write
        /// </param>
        public static bool WriteToPath(string directory, string fileName, string contents)
        {
            try
            {
                string path = Format(directory, fileName);
                File.WriteAllText(path, contents);
                return true;
            }
            catch (System.Exception e)
            {
              //  CCLog.SpecialMsg(e.ToString());
                return false;
            }
        }
    }
}