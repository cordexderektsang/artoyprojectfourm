﻿using UnityEngine;

namespace CCARPlatformFramework.CCTools
{
    public class CCFadeColor
    {
        /// <summary>
        /// Fade object alpha to a target alpha with set speed. Negative speed is fade out; positive speed is fade in
        /// Alpha value will be capped at targetAlphaValue
        /// </summary>
        /// <param name='color'>
        /// Color object being faded
        /// </param>
        /// <param name='targetAlphaValue'>
        /// Target alpha value to fade to
        /// </param>
        /// <param name='fadeSpeed'>
        /// How quickly the fade occurs
        /// </param>
        /// <returns>
        /// New alpha changed color
        /// </returns>
        public static Color AlphaFade(Color color, float targetAlphaValue, float fadeSpeed)
        {
            // Negative or 0 fade speed not allowed
            if (fadeSpeed <= 0.0f)
            {
                return color;
            }

            // Fade in to higher alpha value
            if (targetAlphaValue > color.a)
            {
                return AlphaFadeIn(color, targetAlphaValue, fadeSpeed);
            }
            // Fade out to lower alpha value
            else if (targetAlphaValue < color.a)
            {
                return AlphaFadeOut(color, targetAlphaValue, fadeSpeed);
            }
            // No change so return the same
            else
            {
                return color;
            }
        }

        /// <summary>
        /// Fade in object alpha to a target alpha with set speed
        /// </summary>
        /// <param name='color'>
        /// Color object being faded in
        /// </param>
        /// <param name='targetAlphaValue'>
        /// Target alpha value to fade in to
        /// </param>
        /// <param name='fadeSpeed'>
        /// How quickly the fade in occurs
        /// </param>
        /// <returns>
        /// New alpha changed color
        /// </returns>
        private static Color AlphaFadeIn(Color color, float targetAlphaValue, float fadeSpeed)
        {
            Color spriteColor = color;

            if (spriteColor.a < targetAlphaValue)
            {
                spriteColor.a += (fadeSpeed * Time.deltaTime);
                if (spriteColor.a > targetAlphaValue)
                {
                    spriteColor.a = targetAlphaValue;
                }
            }

            return spriteColor;
        }

        /// <summary>
        /// Fade out object alpha to a target alpha with set speed
        /// </summary>
        /// <param name='color'>
        /// Color object being faded out
        /// </param>
        /// <param name='targetAlphaValue'>
        /// Target alpha value to fade out to
        /// </param>
        /// <param name='fadeSpeed'>
        /// How quickly the fade out occurs
        /// </param>
        /// <returns>
        /// New alpha changed color
        /// </returns>
        private static Color AlphaFadeOut(Color color, float targetAlphaValue, float fadeSpeed)
        {
            Color spriteColor = color;

            if (spriteColor.a > targetAlphaValue)
            {
                spriteColor.a -= (fadeSpeed * Time.deltaTime);
                if (spriteColor.a < targetAlphaValue)
                {
                    spriteColor.a = targetAlphaValue;
                }
            }

            return spriteColor;
        }
    }
}