﻿using CCARPlatformFramework.CCConstants;
using CCARPlatformFramework.CCDataHandling;
using CCARPlatformFramework.CCObjects;
using CCARPlatformFramework.CCTools;

using System.Collections.Generic;
using System;
using System.IO;
using System.Text;

using UnityEngine;
using Vuforia;
using System.Collections;
using System.Linq;
using System.Xml;

public class AssetBundleDownloadManager : MonoBehaviour {

	/// <summary>
	/// variable
	/// </summary>
	private static string metadataPathPersistent; // inital and create persistent path
	private  const string  textFileName = "done.txt";
	// Use this for initialization 

	
	private static void setPath (string subFolder)
	{
		metadataPathPersistent = Path.Combine(Application.persistentDataPath, subFolder);
	}


	public static void DownloadSingleBundles(string URL , string subFolder, string bundleName ,Action<WWW> finishCallback)
	{
		setPath (subFolder);
		string textPath = metadataPathPersistent + "/" + textFileName;
		string bundlePath = metadataPathPersistent + "/" + bundleName;

		if (!File.Exists (textPath) || !File.Exists (bundlePath)) {
			Debug.Log ("Downloading now : missing something");
			string downloadURL = URL + "/" + bundleName;
			CCDownloadController downloadController = CCDownloadController.Instance;
			downloadController.DownloadSingleFile (downloadURL, CCLoadDataManager.DownloadProgress, (WWW www) => {

				CCWriteDataManager.WriteToPath (metadataPathPersistent, bundleName, www.bytes);

				www.Dispose (); 
				System.IO.File.CreateText (textPath);
				if (finishCallback != null)
				{
					finishCallback(www);
				}
	
			}, () =>
            {
                // TODO
                // Error
                Debug.Log("Network error!");
                /* Option
                1) Retry
                if(retry)
                {
                    DownloadBundles(bundleName);
                }
                2) Return
                else
                {
                    LoadMainMenuScene();
                }
                */
            });
		} else {
			Debug.Log ("nothing download");
	
		}
	}
}
