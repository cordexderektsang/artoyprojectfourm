﻿using UnityEngine;
using System.Collections;

public class UIAtlasAndListOfSpriteConstructor
{
    public UIAtlas altas;
    public string name;
    public UISpriteData sprite;

    public void setListOfSpriteData(UIAtlas _altas, string _name , UISpriteData _sprite)
    {
        altas = _altas;
        name = _name;
        sprite = _sprite;

    }

}
