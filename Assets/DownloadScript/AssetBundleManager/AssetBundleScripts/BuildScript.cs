﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.IO;
using UnityEditor;

public class BuildScript
{
    /// <summary>
    /// Builds all asset bundles for the build platform set in Unity.
    /// </summary>
    /// <param name="babo">BuildAssetBundleOptions</param>
    public static void BuildAllAssetBundles(BuildAssetBundleOptions babo)
    {
        BuildAllAssetBundlesTargetPlatform(babo, EditorUserBuildSettings.activeBuildTarget, Utility.GetPlatformName());
    }

    /// <summary>
    /// Builds the asset bundles target platform.
    /// </summary>
    /// <param name="babo">BuildAssetBundleOptions</param>
    /// <param name="buildTarget">Build target</param>
    /// <param name="platformName">Platform name.</param>
    public static void BuildAllAssetBundlesTargetPlatform(BuildAssetBundleOptions babo, BuildTarget buildTarget, string platformName)
    {
        //string outputPath = Path.Combine(Utility.ASSET_BUNDLES_OUTPUT_PATH, platformName);
        //if (!Directory.Exists(outputPath))
        //{
        //    Directory.CreateDirectory(outputPath);
        //}
        string outputPath = CreateDirectory(platformName);

        BuildPipeline.BuildAssetBundles(outputPath, babo, buildTarget);
    }

    public static void BuildAssetBundlesTargetPlatform(List<AssetBundleBuild> buildList, BuildAssetBundleOptions babo, BuildTarget buildTarget, string platformName)
    {
        //string outputPath = Path.Combine(Utility.ASSET_BUNDLES_OUTPUT_PATH, platformName);
        //if (!Directory.Exists(outputPath))
        //{
        //    Directory.CreateDirectory(outputPath);
        //}
        string outputPath = CreateDirectory(platformName);

        BuildPipeline.BuildAssetBundles(outputPath, buildList.ToArray(), babo, buildTarget);
    }

    public static void WriteJSONForAssetBundles(string jsonString, string fileName)
    {
        //string outputPath = Path.Combine(Utility.ASSET_BUNDLES_OUTPUT_PATH, Utility.JSON_FOR_ASSET_BUNDLES_OUTPUT_PATH);
        //if (!Directory.Exists(outputPath))
        //{
        //    Directory.CreateDirectory(outputPath);
        //}
        string outputPath = CreateDirectory(Utility.JSON_FOR_ASSET_BUNDLES_OUTPUT_PATH);

        string outputFile = Path.Combine(outputPath, fileName);
        outputFile += Utility.JSON_FILE_EXTENSION;

        File.WriteAllText(outputFile, jsonString);
    }

    private static string CreateDirectory(string subdirectoryToCreate)
    {
        string outputPath = Path.Combine(Utility.ASSET_BUNDLES_OUTPUT_PATH, subdirectoryToCreate);
        if (!Directory.Exists(outputPath))
        {
            Directory.CreateDirectory(outputPath);
        }

        return outputPath;
    }
}
#endif