﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AssetBundlesMenuItems : EditorWindow
{
    private static readonly string[] BUILD_ASSET_BUNDLE_OPTION_NAMES =
        {"UncompressedAssetBundle", "DisableWriteTypeTree", "DeterministicAssetBundle",
        "ForceRebuildAssetBundle", "IgnoreTypeTreeChanges", "AppendHashToAssetBundleName"};
    private static bool[] BuildAssetBundleOptions = new bool[BUILD_ASSET_BUNDLE_OPTION_NAMES.Length];

    private static readonly string[] BUILD_ASSET_TARGET_PLATFORM_NAMES =
        {"Android", "iOS"};
    private static bool[] BuildAssetTargetPlatforms = new bool[BUILD_ASSET_TARGET_PLATFORM_NAMES.Length];

    private static readonly string[] OBJECT_EXTENSIONS =
        {"fbx", "prefab"};
    private static readonly string[] MATERIAL_EXTENSIONS =
        {"mat"};
    private static readonly string[] TEXTURE_EXTENSIONS =
        {"png", "jpg"};

    private static Dictionary<string, AssetBundlesProperties> DictOfAssets = new Dictionary<string, AssetBundlesProperties>();
    private static List<AssetBundlesProperties> ListOfAssetsToBuild;

    private Vector2 scrollPos;

    public static AssetBundlesMenuItems Instance { get; private set; }

    [MenuItem("Assets/AssetBundles/Build AssetBundles...")]
    public static void Init()
    {
        //EditorWindow window;
        //window = CreateInstance<AssetBundlesMenuItemsV2>();

        // Load version number
        // versionNumber = (EditorPrefs.HasKey ("versionNumber") ? EditorPrefs.GetString ("versionNumber") : "");

        // Load boolean array of build asset options based on API layout
        for (int i = 0; i < BUILD_ASSET_BUNDLE_OPTION_NAMES.Length; i++)
        {
            //buildAssetBundleOptions[i] = (EditorPrefs.HasKey (buildAssetBundleOptionNames[i]) ? EditorPrefs.GetBool (buildAssetBundleOptionNames[i]) : false);
            if (!EditorPrefs.HasKey(BUILD_ASSET_BUNDLE_OPTION_NAMES[i]))
            {
                if (0 == i || 3 == i)
                {
                    BuildAssetBundleOptions[i] = true;
                }
                else
                {
                    BuildAssetBundleOptions[i] = false;
                }
            }
            else
            {
                BuildAssetBundleOptions[i] = EditorPrefs.GetBool(BUILD_ASSET_BUNDLE_OPTION_NAMES[i]);
            }
        }

        // Load boolean array of build asset target platforms
        for (int i = 0; i < BUILD_ASSET_TARGET_PLATFORM_NAMES.Length; i++)
        {
            BuildAssetTargetPlatforms[i] = (EditorPrefs.HasKey(BUILD_ASSET_TARGET_PLATFORM_NAMES[i]) ? EditorPrefs.GetBool(BUILD_ASSET_TARGET_PLATFORM_NAMES[i]) : true);
        }

        SetupData();

        //window.titleContent.text = "Asset Bundles";
        //window.Show();

        ShowWindow();
    }

    public static void ShowWindow()
    {
        GetWindow<AssetBundlesMenuItems>("Asset Bundles");
    }

    private static void SetupData()
    {
        // Setup dictionary of AssetBundles to generate
        string[] names = AssetDatabase.GetAllAssetBundleNames();
        foreach (string name in names)
        {
            Debug.Log("AssetBundle: " + name);
            string[] assetPaths = AssetDatabase.GetAssetPathsFromAssetBundle(name);

            AssetBundlesProperties assetBundleProperties;

            if (!DictOfAssets.TryGetValue(name, out assetBundleProperties))
            {
                assetBundleProperties = new AssetBundlesProperties();
                assetBundleProperties.assetBundleName = name;
            }
            else
            {
                assetBundleProperties.assets = new List<string>();
                assetBundleProperties.assetCounts = new List<int> { 0, 0, 0, 0 };
                //                assetBundleProperties.JSONData = new List<string>();
                assetBundleProperties.JSONData = new List<Dictionary<string, object>>();
            }

            foreach (string assetPath in assetPaths)
            {
                //assets.Add(assetPath);
                //Debug.Log("AssetPath: " + assetPath);
                assetBundleProperties.assets.Add(assetPath);
            }

            if (!DictOfAssets.ContainsKey(name) && assetPaths.Length > 0)
            {
                DictOfAssets.Add(name, assetBundleProperties);
            }
            else if (assetPaths.Length == 0)
            {
                DictOfAssets.Remove(name);
            }

            //else
            //{
            //    //List<string> assets = new List<string>();
            //    assetBundleProperties = new AssetBundlesProperties();

            //    foreach (string assetPath in assetPaths)
            //    {
            //        //assets.Add(assetPath);
            //        assetBundleProperties.assets.Add(assetPath);
            //        Debug.Log("AssetPath: " + assetPath);
            //    }

            //    //DictOfAssets.Add(name, assets);
            //    DictOfAssets.Add(name, assetBundleProperties);
            //}
        }

        //foreach (KeyValuePair<string, List<string>> assetKvp in DictOfAssets)
        foreach (KeyValuePair<string, AssetBundlesProperties> assetKvp in DictOfAssets)
        {
            //assetKvp.Value.isBuildAssetBundle = EditorGUILayout.Toggle(assetKvp.Key, assetKvp.Value.isBuildAssetBundle, GUILayout.MaxWidth(220.0f));

            //int[] assetCounts = { 0, 0, 0, 0 };

            foreach (string assetPath in assetKvp.Value.assets)
            {
                string fileExtension = assetPath.Substring(assetPath.LastIndexOf(".") + 1).ToLower();

                if (CheckStringInArray(fileExtension, OBJECT_EXTENSIONS))
                {
                    assetKvp.Value.assetCounts[0]++;

                    // Should only have one object per bundle, but support multiple for compatibility
                    assetKvp.Value.JSONData.Add(GenerateJSONData(assetPath));
                    //Debug.Log("JSONData[" + (assetKvp.Value.JSONData.Count - 1) + "]: " + assetKvp.Value.JSONData[assetKvp.Value.JSONData.Count - 1]);
                }
                else if (CheckStringInArray(fileExtension, MATERIAL_EXTENSIONS))
                {
                    assetKvp.Value.assetCounts[1]++;
                }
                else if (CheckStringInArray(fileExtension, TEXTURE_EXTENSIONS))
                {
                    assetKvp.Value.assetCounts[2]++;
                }
                else
                {
                    assetKvp.Value.assetCounts[3]++;
                }
            }

            //using (new EditorGUI.DisabledScope(true))
            //{
            //    for (int i = 0; i < assetCounts.Length; i++)
            //    {
            //        EditorGUILayout.IntField(assetCounts[i], GUILayout.MaxWidth(80.0f));
            //    }

            //    //EditorGUILayout.IntField(0, GUILayout.MaxWidth(80.0f));
            //    //EditorGUILayout.IntField(0, GUILayout.MaxWidth(80.0f));
            //    //EditorGUILayout.IntField(0, GUILayout.MaxWidth(80.0f));
            //    //EditorGUILayout.IntField(0, GUILayout.MaxWidth(80.0f));
            //}

            //if (assetKvp.Value.isBuildAssetBundle)
            //{
            //    ListOfAssetsToBuild.Add(assetKvp.Value);
            //}

            //if (GUILayout.Button("List files", GUILayout.MaxWidth(80.0f)))
            //{
            //    AssetBundlesListing.Init(assetKvp.Value);
            //}

            //EditorGUI.EndDisabledGroup();

            //EditorGUILayout.EndHorizontal();

            //if (EditorGUILayout.Toggle(assetKvp.Key, false, GUILayout.MaxWidth(200.0f)))
            //{
            //    ListOfAssetsToBuild.Add(assetKvp.Key);
            //}
        }
    }

    void OnGUI()
    {
        GUILayout.Label("Asset Bundle Builder", EditorStyles.boldLabel);

        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Build Asset Bundles", GUILayout.Width(150)))
        {
            CheckSettingsAndBuild();
        }

        if (GUILayout.Button("Refresh Data", GUILayout.Width(100)))
        {
            SetupData();
        }

        EditorGUILayout.EndHorizontal();

        float originalLabelWidth = EditorGUIUtility.labelWidth;

        EditorGUI.BeginChangeCheck();

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

        //			versionNumber = EditorGUILayout.TextField("Version number", versionNumber);

        GUILayout.Label("Build Asset Bundle Options", EditorStyles.boldLabel);
        EditorGUIUtility.labelWidth = 200.0f;
        for (int i = 0; i < BUILD_ASSET_BUNDLE_OPTION_NAMES.Length; i++)
        {
            BuildAssetBundleOptions[i] = EditorGUILayout.Toggle(BUILD_ASSET_BUNDLE_OPTION_NAMES[i], BuildAssetBundleOptions[i], GUILayout.MaxWidth(220.0f));
        }


        GUILayout.Label("Build Target Platforms", EditorStyles.boldLabel);
        EditorGUIUtility.labelWidth = 60.0f;
        for (int i = 0; i < BUILD_ASSET_TARGET_PLATFORM_NAMES.Length; i++)
        {
            BuildAssetTargetPlatforms[i] = EditorGUILayout.Toggle(BUILD_ASSET_TARGET_PLATFORM_NAMES[i], BuildAssetTargetPlatforms[i], GUILayout.MaxWidth(80.0f));
        }

        GUILayout.Label("Select Asset Bundles to Build", EditorStyles.boldLabel);
        EditorGUILayout.BeginHorizontal();
        GUILayout.Label("Asset Bundle", EditorStyles.helpBox, GUILayout.Width(220.0f));
        GUILayout.Label("# of objects", EditorStyles.helpBox, GUILayout.Width(80.0f));
        GUILayout.Label("# of materials", EditorStyles.helpBox, GUILayout.Width(80.0f));
        GUILayout.Label("# of textures", EditorStyles.helpBox, GUILayout.Width(80.0f));
        GUILayout.Label("# of others", EditorStyles.helpBox, GUILayout.Width(80.0f));
        EditorGUILayout.EndHorizontal();

        EditorGUIUtility.labelWidth = 200.0f;
        ListOfAssetsToBuild = new List<AssetBundlesProperties>();

        //foreach (KeyValuePair<string, List<string>> assetKvp in DictOfAssets)
        foreach (KeyValuePair<string, AssetBundlesProperties> assetKvp in DictOfAssets)
        {
            EditorGUILayout.BeginHorizontal();

            assetKvp.Value.isBuildAssetBundle = EditorGUILayout.Toggle(assetKvp.Key, assetKvp.Value.isBuildAssetBundle, GUILayout.MaxWidth(220.0f));

            //int[] assetCounts = { 0, 0, 0, 0 };

            //foreach(string assetPath in assetKvp.Value.assets)
            //{
            //    string fileExtension = assetPath.Substring(assetPath.LastIndexOf(".") + 1).ToLower();

            //    if (CheckStringInArray(fileExtension, OBJECT_EXTENSIONS))
            //    {
            //        assetCounts[0]++;

            //        // Should only have one object per bundle, but support multiple for compatibility
            //        assetKvp.Value.JSONData.Add(GenerateJSONData(assetPath));
            //        Debug.Log("JSONData[" + (assetKvp.Value.JSONData.Count - 1) + "]: " + assetKvp.Value.JSONData[assetKvp.Value.JSONData.Count - 1]);
            //    }
            //    else if (CheckStringInArray(fileExtension, MATERIAL_EXTENSIONS))
            //    {
            //        assetCounts[1]++;
            //    }
            //    else if (CheckStringInArray(fileExtension, TEXTURE_EXTENSIONS))
            //    {
            //        assetCounts[2]++;
            //    }
            //    else
            //    {
            //        assetCounts[3]++;
            //    }
            //}

            using (new EditorGUI.DisabledScope(true))
            {
                //for(int i = 0; i < assetCounts.Length; i++)
                for (int i = 0; i < assetKvp.Value.assetCounts.Count; i++)
                {
                    //EditorGUILayout.IntField(assetCounts[i], GUILayout.MaxWidth(80.0f));
                    EditorGUILayout.IntField(assetKvp.Value.assetCounts[i], GUILayout.MaxWidth(80.0f));
                }
            }

            if (assetKvp.Value.isBuildAssetBundle)
            {
                ListOfAssetsToBuild.Add(assetKvp.Value);
            }

            if (GUILayout.Button("List files", GUILayout.MaxWidth(80.0f)))
            {
                AssetBundlesListing.Init(assetKvp.Value);
            }

            //EditorGUI.EndDisabledGroup();

            EditorGUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Close", GUILayout.MaxWidth(80.0f)))
        {
            SaveSettings();
            Close();
        }

        EditorGUILayout.EndScrollView();

        if (EditorGUI.EndChangeCheck())
        {
            SaveSettings();
        }

        EditorGUIUtility.labelWidth = originalLabelWidth;
    }

    private static bool CheckStringInArray(string inputString, string[] checkArray)
    {
        foreach (string checkString in checkArray)
        {
            if (inputString.Equals(checkString))
            {
                return true;
            }
        }

        return false;
    }

    private static void CheckSettingsAndBuild()
    {
        if (!BuildAssetTargetPlatforms[0] && !BuildAssetTargetPlatforms[1])
        {
            Debug.LogError("No build targets selected, nothing was generated");
            return;
        }

        if (ListOfAssetsToBuild.Count == 0)
        {
            Debug.LogError("No assets selected for bundling, nothing was generated");
            return;
        }

        //bool hasBuildTarget = false;
        BuildAssetBundleOptions babo = SetBuildAssetBundleOptions();

        List<AssetBundleBuild> buildList = new List<AssetBundleBuild>();
        foreach (AssetBundlesProperties assetBundlesProperties in ListOfAssetsToBuild)
        {
            AssetBundleBuild assetBundleBuild = new AssetBundleBuild();
            assetBundleBuild.assetBundleName = assetBundlesProperties.assetBundleName + ".assetbundle";
            assetBundleBuild.assetNames = assetBundlesProperties.assets.ToArray();
            buildList.Add(assetBundleBuild);

            // Write JSON to file first
            if (assetBundlesProperties.JSONData.Count >= 1)
            {
                string finalJSON = CombineJSONData(assetBundlesProperties.JSONData);
                Debug.Log(finalJSON);
                //Debug.Log("finalJSON: " + finalJSON);
                //IList abc = (IList) MiniJSON.Json.Deserialize(finalJSON);
                //Debug.Log("test final json: " + abc[0]);
                BuildScript.WriteJSONForAssetBundles(finalJSON, assetBundlesProperties.assetBundleName);
            }
        }

        if (BuildAssetTargetPlatforms[0])
        {
            Debug.Log("Building " + buildList.Count + " assets for: " + BUILD_ASSET_TARGET_PLATFORM_NAMES[0]);
            //BuildScriptV2.BuildAllAssetBundlesTargetPlatform(babo, BuildTarget.Android, BUILD_ASSET_TARGET_PLATFORM_NAMES[0]);
            BuildScript.BuildAssetBundlesTargetPlatform(buildList, babo, BuildTarget.Android, BUILD_ASSET_TARGET_PLATFORM_NAMES[0]);
            //hasBuildTarget = true;
        }

        if (BuildAssetTargetPlatforms[1])
        {
            Debug.Log("Building " + buildList.Count + " assets for: " + BUILD_ASSET_TARGET_PLATFORM_NAMES[1]);
            //BuildScriptV2.BuildAllAssetBundlesTargetPlatform(babo, BuildTarget.iOS, BUILD_ASSET_TARGET_PLATFORM_NAMES[1]);
            BuildScript.BuildAssetBundlesTargetPlatform(buildList, babo, BuildTarget.iOS, BUILD_ASSET_TARGET_PLATFORM_NAMES[1]);
            //hasBuildTarget = true;
        }

        //			if(buildAssetTargetPlatforms[2])
        //			{
        //				Debug.Log ("Building assets for: " + buildAssetTargetPlatformNames[2] + ", version: " + versionNumber);
        //				BuildScript.BuildAssetBundlesTargetPlatform(babo, buildAssetTargetPlatformNames[2], versionNumber);
        //				hasBuildTarget = true;
        //			}
        //
        //			if(buildAssetTargetPlatforms[3])
        //			{
        //				Debug.Log ("Building assets for: " + buildAssetTargetPlatformNames[3] + ", version: " + versionNumber);
        //				BuildScript.BuildAssetBundlesTargetPlatform(babo, buildAssetTargetPlatformNames[3], versionNumber);
        //				hasBuildTarget = true;
        //			}

        //if (!hasBuildTarget)
        //{
        //    Debug.LogError("No build targets selected, assets not generated");
        //}
    }

    private static BuildAssetBundleOptions SetBuildAssetBundleOptions()
    {
        BuildAssetBundleOptions babo = new BuildAssetBundleOptions();

        if (BuildAssetBundleOptions[0])
        {
            babo = babo | UnityEditor.BuildAssetBundleOptions.UncompressedAssetBundle;
        }

        if (BuildAssetBundleOptions[1])
        {
            babo = babo | UnityEditor.BuildAssetBundleOptions.DisableWriteTypeTree;
        }

        if (BuildAssetBundleOptions[2])
        {
            babo = babo | UnityEditor.BuildAssetBundleOptions.DeterministicAssetBundle;
        }

        if (BuildAssetBundleOptions[3])
        {
            babo = babo | UnityEditor.BuildAssetBundleOptions.ForceRebuildAssetBundle;
        }

        if (BuildAssetBundleOptions[4])
        {
            babo = babo | UnityEditor.BuildAssetBundleOptions.IgnoreTypeTreeChanges;
        }

        if (BuildAssetBundleOptions[5])
        {
            babo = babo | UnityEditor.BuildAssetBundleOptions.AppendHashToAssetBundleName;
        }

        Debug.Log(babo);

        return babo;
    }

    /// <summary>
    /// Raises the destroy event. Saves the bundle options to editor preferences to load next time
    /// </summary>
    public void OnDestroy()
    {
        SaveSettings();
    }

    private static void SaveSettings()
    {
        //			EditorPrefs.SetString ("versionNumber", versionNumber);

        for (int i = 0; i < BUILD_ASSET_BUNDLE_OPTION_NAMES.Length; i++)
        {
            EditorPrefs.SetBool(BUILD_ASSET_BUNDLE_OPTION_NAMES[i], BuildAssetBundleOptions[i]);
        }

        for (int i = 0; i < BUILD_ASSET_TARGET_PLATFORM_NAMES.Length; i++)
        {
            EditorPrefs.SetBool(BUILD_ASSET_TARGET_PLATFORM_NAMES[i], BuildAssetTargetPlatforms[i]);
        }
    }

    private static Dictionary<string, object> GenerateJSONData(string assetPath)
    {
        try
        {
            GameObject asset = AssetDatabase.LoadAssetAtPath(assetPath, typeof(GameObject)) as GameObject;
            Debug.Log("path :" + assetPath);
            Dictionary<string, object> objectDictionaryForJSON = new Dictionary<string, object>();
            objectDictionaryForJSON.Add("ObjectName", asset.name);
            objectDictionaryForJSON.Add("Version", 1);

            IList<float> insertList = new List<float>();
            insertList.Add(asset.transform.localPosition.x);
            insertList.Add(asset.transform.localPosition.y);
            insertList.Add(asset.transform.localPosition.z);
            //        objectDictionaryForJSON.Add("Position", MiniJSON.Json.Serialize(insertList));
            objectDictionaryForJSON.Add("Position", insertList);

            insertList = new List<float>();
            insertList.Add(asset.transform.localEulerAngles.x);
            insertList.Add(asset.transform.localEulerAngles.y);
            insertList.Add(asset.transform.localEulerAngles.z);
            //        objectDictionaryForJSON.Add("Rotation", MiniJSON.Json.Serialize(insertList));
            objectDictionaryForJSON.Add("Rotation", insertList);

            insertList = new List<float>();
            insertList.Add(asset.transform.localScale.x);
            insertList.Add(asset.transform.localScale.y);
            insertList.Add(asset.transform.localScale.z);
            //        objectDictionaryForJSON.Add("Scaling", MiniJSON.Json.Serialize(insertList));
            objectDictionaryForJSON.Add("Scaling", insertList);

            return objectDictionaryForJSON;
            //        return MiniJSON.Json.Serialize(objectDictionaryForJSON);
        }
        catch (Exception ex)
        {
            Debug.Log("Exception: " + ex);
            return new Dictionary<string, object>();
        }
    }

    //    private static string CombineJSONData(List<string> JSONData)
    private static string CombineJSONData(List<Dictionary<string, object>> JSONData)
    {
        //        IList<string> JSONDataList = new List<string>();
        //
        //        foreach (string JSONDataEntry in JSONData)
        //        {
        //            JSONDataList.Add(JSONDataEntry);
        //        }
        //
        //        return MiniJSON.Json.Serialize(JSONDataList);
        return MiniJSON.Json.Serialize(JSONData);
    }

    [MenuItem("Assets/AssetBundles/Test/Clear Dictionary")]
    static void ClearDict()
    {
        DictOfAssets = new Dictionary<string, AssetBundlesProperties>();
        //AssetDatabase.RemoveAssetBundleName("shoes_cabinet_360", true);
    }

    [MenuItem("Assets/AssetBundles/Test/Names")]
    static void GetNames()
    {
        string[] names = AssetDatabase.GetAllAssetBundleNames();
        foreach (string name in names)
        {
            string[] assetPaths = AssetDatabase.GetAssetPathsFromAssetBundle(name);
            Debug.Log("AssetBundle: " + name);

            foreach (string assetPath in assetPaths)
            {
                Debug.Log("AssetPath: " + assetPath);
            }
        }
    }

    [MenuItem("Assets/AssetBundles/Test/JSON")]
    static void GenerateJSONFileTest()
    {
        string b = "Assets/UIDesignMaterial/ProductCategory/ListofProductImage.prefab";

        //int startIndex = assetPath.LastIndexOf("\\");
        //int endIndex = assetPath.LastIndexOf(".");
        //int length = endIndex - startIndex;

        //string objectName = assetPath.Substring(startIndex, length);
        //GameObject.Find(objectName);

        IDictionary<string, object> dic = new Dictionary<string, object>();

        GameObject a = AssetDatabase.LoadAssetAtPath(b, typeof(GameObject)) as GameObject;
        Debug.Log("a: " + a.name);

        IList<float> pos = new List<float>();

        pos.Add(a.transform.localPosition.x);
        pos.Add(a.transform.localPosition.y);
        pos.Add(a.transform.localPosition.z);

        string c = MiniJSON.Json.Serialize(pos);
        Debug.Log("c: " + c);

        dic.Add("Position", c);

        string d = MiniJSON.Json.Serialize(dic);
        Debug.Log("d: " + d);
    }
}
#endif