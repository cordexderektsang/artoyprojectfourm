﻿using System.Collections.Generic;

public class AssetBundlesProperties
{
    public string assetBundleName = "";
    public List<string> assets = new List<string>();
    public List<int> assetCounts = new List<int> { 0, 0, 0, 0 };
//    public List<string> JSONData = new List<string>();
	public List<Dictionary<string, object>> JSONData = new List<Dictionary<string, object>>();
    public bool isBuildAssetBundle = false;
}
