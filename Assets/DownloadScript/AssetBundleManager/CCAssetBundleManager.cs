﻿using System.IO;
using UnityEngine;
using System.Collections.Generic;
using System;


namespace CCARPlatformFramework.CCTools
{
    public class CCAssetBundleManager : MonoBehaviour
    {
        // Class with the AssetBundle reference, bundle name and version
        private class AssetBundleRef
        {
            public AssetBundle assetBundle = null;
            public string bundleName;
            public string version;
            public AssetBundle SpriteSource = null;

            public AssetBundleRef(AssetBundle assetBundleIn, string bundleNameIn, string intVersionIn)
            {
                assetBundle = assetBundleIn;
                bundleName = bundleNameIn;
                version = intVersionIn;
            }
        };

        // A dictionary to hold loaded AssetBundle references
        private static Dictionary<string, AssetBundleRef> dictAssetBundleRefs = new Dictionary<string, AssetBundleRef>();

        private static string GetKeyName(string bundleName, string version)
        {
            return bundleName + "_" + version;
        }

        /// <summary>
        /// Gets the asset bundle if loaded, otherwise load and then get it.
        /// </summary>
        /// <returns>The asset bundle.</returns>
        /// <param name="path">Path</param>
        /// <param name="bundleName">Bundle name</param>
        public static AssetBundle GetAssetBundle(string path, string bundleName)
        {
          //  Debug.Log("Path to bundle: " + path + ", bundle name: " + bundleName);

            AssetBundleRef abRef;
            if (dictAssetBundleRefs.TryGetValue(bundleName, out abRef))
			{
			     return abRef.assetBundle;
            }
            else
            {
			    AssetBundle myLoadedAssetBundle = LoadAssetBundle(path, bundleName);
                // Hard-code one version only
                abRef = new AssetBundleRef(myLoadedAssetBundle, bundleName, "1");
                dictAssetBundleRefs.Add(bundleName, abRef);
                return myLoadedAssetBundle;
            }
        }
		///Users/kau/Library/Application Support/Cordex/Pricerite X TMF/catalog/iOS/944234.assetbundle
        /// <summary>
        /// Gets the asset bundle if loaded, otherwise load and then get it.
        /// </summary>
        /// <returns>The asset bundle.</returns>
        /// <param name="path">Path</param>
        /// <param name="bundleName">Bundle name</param>
        /// <param name="version">Version</param>
        public static AssetBundle GetAssetBundle(string path, string bundleName, string version)
        {
            Debug.Log("Path to bundle: " + path + ", bundle name: " + bundleName + ", version number: " + version);

            string keyName = GetKeyName(bundleName, version);
            AssetBundleRef abRef;
            if (dictAssetBundleRefs.TryGetValue(keyName, out abRef))
            {
                return abRef.assetBundle;
            }
            else
            {
                AssetBundle myLoadedAssetBundle = LoadAssetBundle(path, keyName);
                abRef = new AssetBundleRef(myLoadedAssetBundle, bundleName, version);
                dictAssetBundleRefs.Add(keyName, abRef);
                return myLoadedAssetBundle;
            }
        }

        /// <summary>
        /// Loads the asset bundle with the given path, bundle name and version
        /// </summary>
        /// <param name="path">Path</param>
        /// <param name="keyName">Bundle name plus version</param>
        public static AssetBundle LoadAssetBundle(string path, string keyName)
        {
            string fileName = keyName /*+ ".assetbundle"*/;
			//Debug.Log("Loaded asset bundle: " + fileName);
		    AssetBundle myLoadedAssetBundle = AssetBundle.LoadFromFile(Path.Combine(path, fileName));

            myLoadedAssetBundle.name = keyName;

           // Debug.Log("Loaded asset bundle: " + myLoadedAssetBundle.name);

            return myLoadedAssetBundle;
        }
		/// <summary>
        /// Unload the specified bundle name and version. Allobjects boolean determines if objects in scene are unloaded too.
        /// </summary>
        /// <param name="bundleName">Bundle name</param>
        /// <param name="allObjects">If set to <c>true</c>, objects in scene are unloaded too</param>
        public static void Unload(string bundleName, bool allObjects)
        {
			Debug.Log ("===" + bundleName);
            AssetBundleRef abRef;
            if (dictAssetBundleRefs.TryGetValue(bundleName, out abRef))
            {
                abRef.assetBundle.Unload(allObjects);
                abRef.assetBundle = null;
                dictAssetBundleRefs.Remove(bundleName);

				Debug.Log(dictAssetBundleRefs.Count + "Bundle unloaded, name: " + bundleName + ", all objects: " + allObjects);
            }
        }

		public static void unLoadAllBundle  ()
		{
			//Debug.Log ("STart unload ");
			foreach(KeyValuePair<string, AssetBundleRef> bundle in dictAssetBundleRefs)
			{
				//Now you can access the key and value both separately from this attachStat as:
			//	Debug.Log("key : " + bundle.Key + " : value " + bundle.Value);
				bundle.Value.assetBundle.Unload(true);
				bundle.Value.assetBundle = null;
				//abRef.assetBundle = null;
			}
			dictAssetBundleRefs = new Dictionary<string, AssetBundleRef> ();

		}

        /// <summary>
        /// Unload the specified bundle name and version. Allobjects boolean determines if objects in scene are unloaded too.
        /// </summary>
        /// <param name="bundleName">Bundle name</param>
        /// <param name="version">Version</param>
        /// <param name="allObjects">If set to <c>true</c>, objects in scene are unloaded too</param>
        public static void Unload(string bundleName, string version, bool allObjects)
        {
            string keyName = GetKeyName(bundleName, version);
            AssetBundleRef abRef;
            if (dictAssetBundleRefs.TryGetValue(keyName, out abRef))
            {
                abRef.assetBundle.Unload(allObjects);
                abRef.assetBundle = null;
                dictAssetBundleRefs.Remove(keyName);

                Debug.Log("Bundle unloaded, name: " + bundleName + ", version number: " + version + ", all objects: " + allObjects);
            }
        }

        /// <summary>
        /// Get an AudioClip from specified asset bundle.
        /// </summary>
        /// <param name="assetBundle">Asset bundle object</param>
        /// <param name="audioClipName">Audio Clip name</param>
        public static AudioClip GetAudioClipFromAssetBundle(AssetBundle assetBundle, string audioClipName)
        {
            AudioClip audioClip = assetBundle.LoadAsset<AudioClip>(audioClipName);
            Debug.Log("AudioClip loaded: " + audioClip.name);
		

            return audioClip;
        }


		/// <summary>
		/// Returns all asset name in bundle.
		/// </summary>
		/// <returns>The all asset name in bundle.</returns>
		/// <param name="assetBundle">Asset bundle.</param>
		public static string[] ReturnAllAssetNameInBundle (AssetBundle assetBundle)
		{
			string[] allAudioClips = assetBundle.GetAllAssetNames ();

			return allAudioClips;


		}

		public static void removeTargetBundleFromDictiionry (string bundleName)
		{
			Debug.Log ("start remove from dictionary : " + bundleName );
			AssetBundleRef abRef;
			if (dictAssetBundleRefs.TryGetValue(bundleName, out abRef))
			{
				abRef.assetBundle = null;
				dictAssetBundleRefs.Remove(bundleName);

				Debug.Log(" remove Bundle unloaded, name: " +  bundleName);
			}
				
		}

		public static void LogDictionary ()
		{
			int i = 0;
	
			foreach (KeyValuePair<string ,AssetBundleRef> data in dictAssetBundleRefs)
			{

				Debug.Log (" key : "+ data.Key + " : bundle name L: " + data.Value.bundleName);
				i++;
			}
			Debug.Log ("---try get current dictionary Item --" + i  + " length");

		}

        /// <summary>
        /// Get a game object from specified asset bundle.
        /// </summary>
        /// <param name="assetBundle">Asset bundle object</param>
        /// <param name="modelName">Model name</param>
        public static GameObject GetGameObjectFromAssetBundle(AssetBundle assetBundle, string modelName)
        {
            GameObject gameObject = assetBundle.LoadAsset<GameObject>(modelName);
           // Debug.Log("Object loaded: " + gameObject.name);

            return gameObject;
        }


		public static Font GetFontFromAssetBundle(AssetBundle assetBundle, string modelName)
		{
		//	Debug.Log (modelName);
			Font font = assetBundle.LoadAsset<Font>(modelName);
			//Debug.Log (font);
			// Debug.Log("Object loaded: " + gameObject.name);

			return font;
		}


		public static bool CheckTargetContain(AssetBundle assetBundle, string modelName){

			if (assetBundle.Contains (modelName)) {

				return true;
			} else {

				return false;
			}
		}


		public static void LoadSceneFromBundle (string Path , string BundleName, string SceneName)
		{

			AssetBundle currentBundle = CCAssetBundleManager.GetAssetBundle(Path, BundleName);

			UnityEngine.SceneManagement.SceneManager.LoadScene (SceneName);

		}


        /// <summary>
        /// Get a game object from specified asset bundle.
        /// </summary>
        /// <param name="assetBundle">Asset bundle object</param>
        /// <param name="modelName">Model name</param>
        public static Cubemap GetSkyBoxFromAssetBundle(AssetBundle assetBundle, string modelName)
        {
           // Debug.Log("testing : " + assetBundle.Contains(modelName));

            Cubemap gameObject = assetBundle.LoadAsset<Cubemap>(modelName);
            // Debug.Log("Object loaded: " + gameObject.name);

            return gameObject;
        }


        /// <summary>
        /// Get a shader from specified asset bundle.
        /// </summary>
        /// <param name="assetBundle">Asset bundle object</param>
        /// <param name="modelName">Model name</param>
        public static Shader GetShaderFromAssetBundle(AssetBundle assetBundle, string shaderName)
        {
            Shader shader = assetBundle.LoadAsset<Shader>(shaderName);
            Debug.Log("Shader loaded: " + shader.name);

            return shader;
        }

        /// <summary>
        /// Get a material from specified asset bundle.
        /// </summary>
        /// <param name="assetBundle">Asset bundle object</param>
        /// <param name="materialName">Material name</param>
        public static Material GetMaterialFromAssetBundle(AssetBundle assetBundle, string materialName)
        {
            Material material = assetBundle.LoadAsset<Material>(materialName);
            Debug.Log("Material loaded: " + material.name);

            return material;
        }

        /// <summary>
        /// Get a texture from specified asset bundle.
        /// </summary>
        /// <param name="assetBundle">Asset bundle object</param>
        /// <param name="textureName">Texture name</param>
        public static Texture GetTextureFromAssetBundle(AssetBundle assetBundle, string textureName)
        {
            
            Texture texture = assetBundle.LoadAsset<Texture>(textureName);
        //    Debug.Log("Texture loaded: " + texture.name);

            return texture;
        }


		public static Texture2D GetTexture2DFromAssetBundle(AssetBundle assetBundle, string textureName)
		{

			Texture2D texture = assetBundle.LoadAsset<Texture2D>(textureName);
			//    Debug.Log("Texture loaded: " + texture.name);

			return texture;
		}

        //public static void SaveSpriteReference(AssetBundle SpriteSourceReference)
        //{
        //    SpriteSource = SpriteSourceReference;
        //}

        public static List<Sprite> GetSpritesFromAssetBundle(AssetBundle assetBundle, string[] spriteNames)
        { 
            List<Sprite> returnSpriteList = new List<Sprite>();
            foreach (string name in spriteNames)
            {
                returnSpriteList.Add(GetSpriteFromAssetBundle(assetBundle, name));
            }
           
            return returnSpriteList;
        }

        public static Sprite GetSpriteFromAssetBundle(AssetBundle assetBundle, string spriteName)
        {
            Sprite sprite = assetBundle.LoadAsset<Sprite>(spriteName);
            Debug.Log("Sprite loaded: " + sprite.name);
            return sprite;
        }



		public static UIAtlas GetAltasFromAssetBundle(AssetBundle assetBundle, string altasName)
		{
            UIAtlas UIAtlas = assetBundle.LoadAsset<GameObject>(altasName).GetComponent<UIAtlas>();
			Debug.Log("UIAtlas loaded: " + assetBundle.name);
			return UIAtlas;
		}

        public static UnityEngine.Object GetTypeFromAssetBundle<T>(AssetBundle assetBundle, string assetName)
        {
            Type type = typeof(T);
            UnityEngine.Object returnAsset = assetBundle.LoadAsset(assetName, type);
            Debug.Log("Asset by type(" + type.Name + ") loaded: " + returnAsset.name);

            return returnAsset;
        }

        public static UnityEngine.Object GetTypeFromAssetBundle(AssetBundle assetBundle, string assetName)
        {
            UnityEngine.Object returnAsset = assetBundle.LoadAsset(assetName);
            Debug.Log("Asset loaded: " + returnAsset.name);

            return returnAsset;
        }
    }
}