﻿using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

public class Marker
{
    [XmlAttribute("name")]
    public string name;

    [XmlAttribute("size")]
    public string sizeString;

    public Vector2 sizeVector;
}