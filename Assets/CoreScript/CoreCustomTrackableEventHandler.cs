/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using UnityEngine;
using System.Collections;
using CCARPlatformFramework.CCConstants;
using CCARPlatformFramework.CCDataHandling;
using CCARPlatformFramework.CCTools;

namespace Vuforia
{
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
	public class CoreCustomTrackableEventHandler : MonoBehaviour,
                                                ITrackableEventHandler
    {
        #region PRIVATE_MEMBER_VARIABLES
 
        private TrackableBehaviour mTrackableBehaviour;
		private const string nameForUnicorn ="UIicorn";
		public MarkerSetUpComponent markerComponentScript;
		private bool isFirstTrackCompleted = false;
		public AudioPlayScript audioPlayScript;
		public string relatedMarkerName;
		public Transform relatedMarketObject;
		public Vector3 scale;
		public Vector3 position;
		public Vector3 rotation;
		public int vuId = -1;
		//vumarker only
		private VuMarkManager mVuMarkManager;
		private VuMarkTarget vumark;
		public VuMarkBehaviour vuMarkerBehaviour;
		public SetUpImageTarget setUpImageTarget;
		public DistanceClassVuMarker distanceClassVuMarker;
		public GameObject plan;



	//	private CoreCustomTrackableEventHandler coreCustomTrackableEventHandler;
    
        #endregion // PRIVATE_MEMBER_VARIABLES


		public bool isTrack = false;

		public int markerUniqueId;

        #region UNTIY_MONOBEHAVIOUR_METHODS

		public void Awake (){

			vuMarkerBehaviour = GetComponent<VuMarkBehaviour> ();
			setUpImageTarget = GameObject.Find ("Controller").GetComponent<SetUpImageTarget> ();
			plan = Resources.Load("Plane") as GameObject;

		}
    
        void Start()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
			//audioPlayScript = GetComponent<AudioPlayScript> ();
			distanceClassVuMarker = GameObject.Find("Controller").GetComponent<DistanceClassVuMarker>();
			if (vuMarkerBehaviour == null) {
				SetUpScriptComponent ();
			} else {

				Destory ();

			}
		
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
	

	
        }

        #endregion // UNTIY_MONOBEHAVIOUR_METHODS



        #region PUBLIC_METHODS


		private void SetUpScriptComponent ()
		{
			SetUpMarkerComponentScript ();
			SetUpAudioComponentScript ();
			SetUpMarkerRelatedObject ();

		}

		public void SetUpMarkerComponentScript ()
		{
			if (markerComponentScript == null) {
				MarkerSetUpComponent[] markerSetupComponent = GetComponentsInChildren<MarkerSetUpComponent> (true);
				if ( markerSetupComponent.Length > 0) {
					markerComponentScript = markerSetupComponent [0];

				}
			}
	

		}

		public void SetUpAudioComponentScript ()
		{
			if (audioPlayScript == null) {
				AudioPlayScript[] audioSetupComponent = GetComponentsInChildren<AudioPlayScript> (true);

				if (audioSetupComponent.Length > 0) {
					audioPlayScript = audioSetupComponent [0];
				}
			}


		}

		public void SetUpMarkerRelatedObject ()
		{
			if (relatedMarketObject == null) {
				GameObject relatedMarkerObjectReference = GameObject.Find (relatedMarkerName);
				if (relatedMarkerObjectReference != null) {
					relatedMarketObject = GameObject.Find (relatedMarkerName).transform;
				}

			}

		}
        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        #endregion // PUBLIC_METHODS



        #region PRIVATE_METHODS

		private void Destory(){

			if (this.transform.childCount > 1) {
				GameObject child = this.transform.GetChild (0).gameObject;
				GameObject child2 = this.transform.GetChild(1).gameObject;
				DestroyImmediate (child);
				DestroyImmediate(child2);
			}
		}

		private void VUId()
		{

			mVuMarkManager = TrackerManager.Instance.GetStateManager().GetVuMarkManager();

			
			foreach (var bhvr in mVuMarkManager.GetActiveBehaviours()) {

				if (bhvr.transform.childCount == 0) {
					vumark = bhvr.VuMarkTarget;
					vumark.StartExtendedTracking();
					int index = 0;
					int.TryParse (vumark.InstanceId.StringValue, out index);
					vuId  = index;

					Debug.Log (" object init for index  -->" + index);

						ImageObjectForEachTracker markerObject = setUpImageTarget.getVuIdObject(index);
						if (markerObject != null) {
							GameObject start = Instantiate (markerObject.markerGameObject);
							GameObject rotatePlan = Instantiate(plan);
							rotatePlan.transform.parent = bhvr.transform;
							rotatePlan.transform.localScale = new Vector3(1, 1, 1);	
							rotatePlan.transform.localEulerAngles = Vector3.zero;
							rotatePlan.transform.localPosition = Vector3.zero;
							rotatePlan.name = SetUpImageTarget.Plan;
							rotatePlan.SetActive(false);
							start.transform.parent = bhvr.transform;
							bhvr.name = markerObject.markerTrackableName;
							if (start.name.Contains ("Cloud") ) {

								start.transform.localPosition = new Vector3 (0.04f,-0.03f,-0.01f);
								start.transform.localEulerAngles = new Vector3 (-90, 0, 0);
								start.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
								start.name = markerObject.markerGameObject.name;
							if(index == 7 && vuId == 7) {
								distanceClassVuMarker.positionOneObject = start.transform.parent.gameObject;
								//distanceClassVuMarker.positionOneTrackScript = this;
								} else if(index == 8  && vuId  == 8){
								distanceClassVuMarker.positionTwoObject = start.transform.parent.gameObject;
								//distanceClassVuMarker.positionTwoTrackScript = this;
								}
							} else {

								start.transform.localPosition = Vector3.zero;
								start.transform.localEulerAngles = new Vector3 (-90, 0, 0);
								start.transform.localScale = new Vector3 (1, 1, 1);
								start.name = markerObject.markerGameObject.name;

							}
						//}
					} else {

						Debug.Log ("=====no object find for vu id========");
						bhvr.name = "nothing";
					}
				} else {

					Debug.Log ("============nothing=========");

				}
				 
		
			}
		
		}



        private void OnTrackingFound()
        {

			if (vuMarkerBehaviour != null) {
				
				VUId ();
			
			}
		
			if (this.transform.childCount == 1 && vuMarkerBehaviour == null ) {

				Transform markerObject = relatedMarketObject.transform.GetChild (1);
				markerObject.parent = this.transform;
				markerObject.localPosition = position;
				markerObject.localEulerAngles = rotation;
				markerObject.localScale = scale;
			
			}
			if (markerComponentScript == null || markerComponentScript.emissionController == null)
			{
				SetUpScriptComponent();
			}
			if (audioPlayScript != null)
			{
				Debug.Log("audio set track");
				audioPlayScript.SetUpTrackComponent(this);
			}
				Debug.Log (this.gameObject.name + " is tracked");
				isTrack = true;
				isFirstTrackCompleted = true;
				RenderComponent ();
		
			if (markerComponentScript != null && markerComponentScript.emissionController != null) {
				markerComponentScript.emissionController.StartTransitionEmission (false);
				markerComponentScript.emissionController.StartTransitionEmission (true);
				markerComponentScript.emissionController.StartScaleFunction ();
				markerComponentScript.emissionController.ResetTimeEffectCounter ();
				Debug.Log ("StartTransitionEmission for " + mTrackableBehaviour.TrackableName );
			}
				GlobalStaticVariable.AllTrackingItem.Add (this);
	

			
        }

		private void RenderComponent ()
		{
			Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
			Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

			// Enable rendering:
			foreach (Renderer component in rendererComponents)
			{
		
				component.enabled = true;
			}

			// Enable colliders:
			foreach (Collider component in colliderComponents)
			{
				component.enabled = true;
			}
		}

		/*private IEnumerator DelayShowObject ()
		{
			if (markerComponentScript.transitionEffectRender != null) {
				markerComponentScript.transitionEffectRender.enabled = true;
				markerComponentScript.emissionController.StartTransitionEmission(true);
			}
			yield return new WaitForSeconds (2f);
			Debug.Log ("wait");
			RenderComponent ();

		}*/


        private void OnTrackingLost()
        {
			isTrack = false;
				if (markerComponentScript != null && isFirstTrackCompleted) {
				markerComponentScript.emissionController.TimeForEffectRepeat ();
			}
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = false;
            }

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
			if (audioPlayScript != null) {
				audioPlayScript.stopAll ();
			}

			GlobalStaticVariable.AllTrackingItem.Remove (this);
			if (vuMarkerBehaviour != null) {
				Destory ();
			}
        }

        #endregion // PRIVATE_METHODS
    }
}
