﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
public class SetUpImageTarget : MonoBehaviour {

	//private List<ImageTargetInfor> allMarker = new List<ImageTargetInfor>();
	public List <ImageObjectForEachTracker> objectForTracker = new List <ImageObjectForEachTracker> ();
	public DistanceClass distanceClass;
	public GameObject planForDetection;
	public const string Plan = "Plan";

	// start function To inital add in all marker
	public void GetAllImageTargetStart () 
	{
		GlobalStaticVariable.ResetAllGlobalVariables ();
		distanceClass = GetComponent<DistanceClass> ();
		AddALLImageTargetToList ();
	}



	/// <summary>
	/// Adds ALL image target to list.
	/// </summary>
	private void AddALLImageTargetToList ()
	{
		if (objectForTracker.Count == 0) 
		{
			return;
		}
		GlobalStaticVariable.allMarker = new List<ImageTargetInfor>();
		StateManager sm = TrackerManager.Instance.GetStateManager ();
		IEnumerable<TrackableBehaviour> activeTrackables = sm.GetTrackableBehaviours ();
			// Iterate through the list of active trackables
			foreach (TrackableBehaviour tb in activeTrackables) {
				Debug.Log (" add in marker to list Name : " + tb.TrackableName);
				//set up tracker data
				ImageTargetInfor imageTarget = new ImageTargetInfor ();
				GameObject plan = Instantiate(planForDetection);
				plan.transform.parent = tb.transform;
				plan.transform.localScale = new Vector3(1, 1, 1);	
				plan.transform.localPosition = new Vector3(0, 0, 0);
				plan.transform.localEulerAngles = new Vector3(0, 0, 0);
				plan.gameObject.SetActive(false);
				plan.gameObject.name = Plan;
				imageTarget.markerGameObject = tb.gameObject;
				imageTarget.markerGameObject.name = tb.TrackableName;
				imageTarget.markerTrackableName = imageTarget.markerGameObject.name;
				imageTarget.trackableEventScript = tb;
				imageTarget.defaultTrackableEventHandler = imageTarget.markerGameObject.AddComponent (typeof(CoreCustomTrackableEventHandler)) as CoreCustomTrackableEventHandler;

			if (!imageTarget.markerGameObject.name.Contains ("Rainbow_Marker")) {
				imageTarget.markerGameObject.AddComponent (typeof(SmoothObject));
			}
		
				ImageObjectForEachTracker getTargetObjectForTargetMarker = ReturnTargetObjectForTargetMarker (tb.TrackableName);// find set up date for list

				
			if (getTargetObjectForTargetMarker != null && getTargetObjectForTargetMarker.markerGameObject != null) {
				// set up object
				GameObject targetObjectForMarker = Instantiate(getTargetObjectForTargetMarker.markerGameObject);
				targetObjectForMarker.name = getTargetObjectForTargetMarker.markerGameObject.name;
				targetObjectForMarker.transform.parent = imageTarget.markerGameObject.transform;
				targetObjectForMarker.transform.localPosition = getTargetObjectForTargetMarker.gameObjectPosition;
				targetObjectForMarker.transform.localEulerAngles = getTargetObjectForTargetMarker.gameObjectRotation;
				targetObjectForMarker.transform.localScale = getTargetObjectForTargetMarker.gameObjectScale;
				imageTarget.defaultTrackableEventHandler.relatedMarkerName = getTargetObjectForTargetMarker.relatedMarkerName;
				imageTarget.defaultTrackableEventHandler.position = getTargetObjectForTargetMarker.gameObjectPosition;
				imageTarget.defaultTrackableEventHandler.scale = getTargetObjectForTargetMarker.gameObjectScale;
				imageTarget.defaultTrackableEventHandler.rotation = getTargetObjectForTargetMarker.gameObjectRotation;
				// set up render base on is track function
				renderOnOrOff (imageTarget.defaultTrackableEventHandler.isTrack);
				imageTarget.rendeObject = targetObjectForMarker; //marker object
				imageTarget.defaultTrackableEventHandler.markerUniqueId = getTargetObjectForTargetMarker.markerUniqueId;// unique id for marker
				GlobalStaticVariable.allMarker.Add (imageTarget);

			} else if (getTargetObjectForTargetMarker != null) {

			
				imageTarget.defaultTrackableEventHandler.relatedMarkerName = getTargetObjectForTargetMarker.relatedMarkerName;
				imageTarget.defaultTrackableEventHandler.markerUniqueId = getTargetObjectForTargetMarker.markerUniqueId;// unique id for marker
				imageTarget.defaultTrackableEventHandler.position = getTargetObjectForTargetMarker.gameObjectPosition;
				imageTarget.defaultTrackableEventHandler.scale = getTargetObjectForTargetMarker.gameObjectScale;
				imageTarget.defaultTrackableEventHandler.rotation = getTargetObjectForTargetMarker.gameObjectRotation;
				GlobalStaticVariable.allMarker.Add (imageTarget);

			}


	
		}
		//StartExtendTracking ();
		Debug.Log("==marker ready ====");
	
	}

	public ImageObjectForEachTracker getVuIdObject (int index)
	{
		foreach (ImageObjectForEachTracker data in objectForTracker){

				if(data.vuMarkerId == index)
				{

					return data;
				}
		
		}
		Debug.Log("VuId not vaildate");
		return null;


	}

	/// <summary>
	/// Find Target marker name and return data for image set up
	/// </summary>
	/// <returns>The target object for target marker.</returns>
	/// <param name="trackerName">Tracker name.</param>
	private ImageObjectForEachTracker ReturnTargetObjectForTargetMarker (string trackerName)
	{
		foreach (ImageObjectForEachTracker target in objectForTracker)
		{
			if(target.markerTrackableName.Equals(trackerName))
			{
				return target;
			}

		}
		if (objectForTracker.Count >= 1) {
			Debug.Log (" can't find target using the first element for default element");
			return objectForTracker [0];
		} 
		Debug.Log (" Nothing in the Set up List");
		return null;

	}

	public void StartExtendTracking ()
	{

		foreach (ImageTargetInfor ImageData in GlobalStaticVariable.allMarker)
		{
			try{
				((ImageTarget)ImageData.trackableEventScript.Trackable).StartExtendedTracking ();
			} catch(System.Exception ex){
				((VuMarkTemplate)ImageData.trackableEventScript.Trackable).StartExtendedTracking ();
			}



		}

	}



	/// <summary>
	/// Renders the on or off.
	/// </summary>
	/// <param name="status">If set to <c>true</c> status.</param>
	private void renderOnOrOff (bool status)
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(status);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(status);

		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = status;
		}

		// Enable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = status;
		}


	}

}


	

[System.Serializable]
public class ImageObjectForEachTracker 
{
	public GameObject markerGameObject;
	public string markerTrackableName;
	public string relatedMarkerName;
	public Vector3 gameObjectPosition;
	public Vector3 gameObjectScale;
	public Vector3 gameObjectRotation;
	public int markerUniqueId;
	public string objectName;
	public int vuMarkerId;


}

[System.Serializable]
public class ImageTargetInfor 
{
	public GameObject markerGameObject;
	public GameObject rendeObject;
	public string markerTrackableName;
	public TrackableBehaviour trackableEventScript;
	public CoreCustomTrackableEventHandler defaultTrackableEventHandler;

}
