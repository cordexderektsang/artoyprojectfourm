﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreAudioSetting : MonoBehaviour {

	private const string volumeValue = "volumeValue";
	private const string alertValue = "alertValue";
	private const string MuteValueForV = "MuteValueForV";
	private const string MuteValueForB = "MuteValueForB";
	private const string BackGroundTag = "BackGround";
	bool volumeSwitch = true;
	private const int numberOfVolumePoint = 5;
	private const int convertToVolumePoint = 20;


	public static int VolumeValue
	{
		get {
			return CCLocalStorageAPI.Contian (volumeValue) ? CCLocalStorageAPI.GET <int> (volumeValue) : 100;
		}
		set {
			//Debug.Log (value);
			CCLocalStorageAPI.SET<int> (volumeValue, value);
		}
	}


	public static int VolumeValueConvertToVolumePoint
	{
		get {
			return CCLocalStorageAPI.Contian (volumeValue) ? CCLocalStorageAPI.GET <int> (volumeValue)/convertToVolumePoint : 100/convertToVolumePoint;
		}
		set {
			CCLocalStorageAPI.SET<int> (volumeValue, value*convertToVolumePoint);
		}
	}

	public static int BackGroundValue
	{
		get {
			return CCLocalStorageAPI.Contian (alertValue) ? CCLocalStorageAPI.GET <int> (alertValue) : 100;
		}
		set {
			//Debug.Log (value);
			CCLocalStorageAPI.SET<int> (alertValue, value);
		}
	}


	public static int BackGroundConvertToVolumePoint
	{
		get {
			
			return CCLocalStorageAPI.Contian (alertValue) ? CCLocalStorageAPI.GET <int> (alertValue)/convertToVolumePoint : 100/convertToVolumePoint;
		}
		set {
			CCLocalStorageAPI.SET<int> (alertValue, value*convertToVolumePoint);
		}
	}


	public static string MuteValueForVolume
	{
		get {
			if (CCLocalStorageAPI.Contian (MuteValueForV)) {
			//	Debug.Log (CCLocalStorageAPI.GET <string> (MuteValueForV));
				return  CCLocalStorageAPI.GET <string> (MuteValueForV); 
			} else {
				return "none";
			}
		}
		set {
			CCLocalStorageAPI.SET<string> (MuteValueForV, value);
		}
	}

	public static string MuteValueForBackGround
	{
		get {
			if (CCLocalStorageAPI.Contian (MuteValueForB)) {
	
				return CCLocalStorageAPI.GET <string> (MuteValueForB);
			} else {
		
				return "none";
			}
		}
		set {

			CCLocalStorageAPI.SET<string> (MuteValueForB, value);
		}
	}




	public static void OnBGMClick (bool IsIncrease, bool IsVolume)
	{
		if (IsVolume) {
			SetOtherVolume (IsIncrease);
		} else {
			SetBackGroundVolume (IsIncrease);
		}
		SetUpAllAudio ();

	}



	public static void SetUpAllAudio ()
	{
		AudioSource[] audio = FindObjectsOfType(typeof(AudioSource)) as AudioSource[]; 
		UIPlaySound[] nguiAudio = FindObjectsOfType(typeof(UIPlaySound)) as UIPlaySound[]; 
		//Debug.Log ("unity audio : " + audio.Length + "Ngui audio : " + nguiAudio.Length);
		float volumeValue = 	((float) VolumeValue) / ((float) 100);
		float backGroundValue = 	((float) BackGroundValue) / ((float) 100);
		//Debug.Log ("65Find Number of Audio Compoent :  " +  audio.Length + "Setting Music Volume To This Value : " + volumeValue + "Setting backGround Volume To This Value : " + backGroundValue);
		foreach (AudioSource sound in audio) 
		{ 
			if (!sound.gameObject.tag.Equals (BackGroundTag)) {
				sound.volume = volumeValue;
			} else {
				sound.volume = backGroundValue;

			}
			sound.enabled = true;
		}

		foreach (UIPlaySound nguiSound in nguiAudio) 
		{ 
			if (CoreAudioSetting.MuteValueForVolume.Equals (ButtomFlow.NoMute)) {
				nguiSound.volume = volumeValue;
			} else {
				nguiSound.volume = 0;

			}
		}

	}


	public static AudioSource[]ReturnAllAudio()
	{
		AudioSource[] audio = FindObjectsOfType(typeof(AudioSource)) as AudioSource[]; 
		return audio;
	}


	public static void OnOffAllSound ()
	{
		AudioSource[] audio = FindObjectsOfType(typeof(AudioSource)) as AudioSource[]; 
		UIPlaySound[] nguiAudio = FindObjectsOfType(typeof(UIPlaySound)) as UIPlaySound[]; 
		foreach (AudioSource sound in audio) 
		{ 
			if (sound.gameObject.tag.Equals (BackGroundTag)) {
				if (CoreAudioSetting.MuteValueForBackGround.Equals (ButtomFlow.NoMute)) {
					sound.mute = false;
				} else {
					sound.mute = true;
				}
			} else {
				if (CoreAudioSetting.MuteValueForVolume.Equals (ButtomFlow.NoMute)) {
					sound.mute = false;
				} else {
					sound.mute = true;
				}
			}
		}

		foreach (UIPlaySound nguiSound in nguiAudio) 
		{ 
			if (!CoreAudioSetting.MuteValueForVolume.Equals (ButtomFlow.NoMute)) {
				nguiSound.volume = 0;
			} else {
				float volumeValue = ((float)VolumeValue) / ((float)100);
				nguiSound.volume = volumeValue;
			}
		}

	}

	private  static void SetOtherVolume (bool IsIncrease)
	{
		int value = VolumeValue;
		if (IsIncrease == false) {
			switch (value) {
			case 100:
				value = 80;
				VolumeValue = value;
				break;
			case 80:
				value = 60;
				VolumeValue = value;
				break;
			case 60:
				value = 40;
				VolumeValue = value;
				break;
			case 40:
				value = 20;
				VolumeValue = value;
				break;
			case 20:
				value = 0;
				VolumeValue = value;
				break;
			}

		} else {
			switch (value) {
			case 0:
				value = 20;
				VolumeValue = value;
				break;
			case 20:
				value = 40;
				VolumeValue = value;
				break;
			case 40:
				value = 60;
				VolumeValue = value;
				break;
			case 60:
				value = 80;
				VolumeValue = value;
				break;
			case 80:
				value = 100;
				VolumeValue = value;
				break;
			}

		}
	}

	private static void SetBackGroundVolume (bool IsIncrease)
	{
		int value = BackGroundValue;
		Debug.Log ("IsIncrease" + IsIncrease + "value " + value );
		if (!IsIncrease) {
			switch (value) {
			case 100:
				value = 80;
				BackGroundValue = value;
				break;
			case 80:
				value = 60;
				BackGroundValue = value;
				break;
			case 60:
				value = 40;
				BackGroundValue = value;
				break;
			case 40:
				value = 20;
				BackGroundValue = value;
				break;
			case 20:
				value = 0;
				BackGroundValue = value;
				break;
			}

		} else {
			switch (value) {
			case 0:
				value = 20;
				BackGroundValue = value;
				break;
			case 20:
				value = 40;
				BackGroundValue = value;
				break;
			case 40:
				value = 60;
				BackGroundValue = value;
				break;
			case 60:
				value = 80;
				BackGroundValue = value;
				break;
			case 80:
				value = 100;
				BackGroundValue = value;
				break;
			}

		}
	}

}
