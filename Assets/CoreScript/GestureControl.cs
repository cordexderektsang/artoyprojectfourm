﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;
using System;

public class GestureControl : CCBaseGestureControllerVer2
{

	#region Public Variables

	[Range (0.01f, 10f)]
	public float autoFocusTime = 0.2f;

	//[Range(0.01f, 2f)]
	//public float doubleClickTime = 0.3f;

	[Range (0.01f, 2f)]
	public float maxScaleValue = 2f;
	[Range (0.01f, 2f)]
	public float minScaleValue = 0.5f;
	[Range (1f, 10f)]
	public float scaleSpeed = 1f;


	// slider
	//public bool isObjectReady = false;

	#endregion

	#region Private Variables

	private bool isCameraOverUI = false;
	private bool isRequireFingerReset = false;

	private bool isTracked = false;



	//private bool isDoubleClickTimer = false;
	//private float doubleClickTimer = 0.0f;

	private bool isAutoFocus = true;
	private bool isAutoFocusTimer = true;
	private float autoFocusTimer = 0.0f;


	//index for swiping
	[SerializeField]
	private int _index = 0;

	public int index {
		get {
			return _index;
		}

		private set {
			_index = value;
		}
	}

	private bool userRotation = false;
	private bool userScale = false;

	private RaycastHit hit;

	private Coroutine unsetTrackableCoroutine;



	public delegate void callBackFunction ();

	callBackFunction callBack;


	#endregion

	// Use this for initialization
	protected override void Start ()
	{
		base.Start ();

		//lighting1Position = lighting1.transform.position;
		//lighting1Rotation= lighting1.transform.eulerAngles;
		//lighting2Position = lighting1.transform.localPosition;
		//lighting2Rotation= lighting1.transform.localEulerAngles;
		//Texture2D currentproductDetail = (Texture2D)productDetail.productDetail [index];
		//objectDetail.sprite = Sprite.Create(currentproductDetail, new Rect(0, 0, currentproductDetail.width, currentproductDetail.height), new Vector2(0.5f, 0.5f));
	}

	// Update is called once per frame
	protected override void Update ()
	{
		if (isAutoFocusTimer) {
			autoFocusTimer += Time.deltaTime;
			if (autoFocusTimer >= autoFocusTime) {
				autoFocusTimer = 0.0f;
				checkTouchAutofocus();
				isAutoFocusTimer = true;
				isAutoFocus = false;
			}
		}



		//if (isDoubleClickTimer)
		//{
		//    doubleClickTimer += Time.deltaTime;

		//    if (doubleClickTimer >= doubleClickTime)
		//    {
		//        ResetDoubleClickTimer();
		//    }
		//}
		base.Update ();
		//if (Input.GetMouseButtonDown (0)) {

		//playAnimation();
		///	tempLeft();
		//}

	}

	//private void ResetDoubleClickTimer()
	//{
	//    if (isDoubleClickTimer)
	//    {
	//        doubleClickTimer = 0.0f;
	//        isDoubleClickTimer = false;
	//        clickCount = 0;
	//    }
	//}
	
	public void tempLeft ()
	{
		oneFingerSwipeLeft ();
	}

	public void tempRight ()
	{
		oneFingerSwipeRight ();
	}

	public void tempRotate ()
	{
		rotateObject (25f);
	}

	public void tempRotate2 ()
	{
		rotateObject (-25f);
	}





	private void changeFurnitureObject (int newIndex)
	{


		//turnOffAllRender(index,false);
		// listOfObject[index].SetActive(false);
		//	SetIndexValue (newIndex);
		//turnOffAllRender(index,true);
		//resetRender ();
		//listOfObject[index].SetActive(true);
		//Texture2D currentproductDetail = (Texture2D)productDetail.productDetail[index];
		//objectDetail.sprite = Sprite.Create(currentproductDetail, new Rect(0, 0, currentproductDetail.width, currentproductDetail.height), new Vector2(0.5f, 0.5f));
//        productDetail.resetAllTexture();
		//    productDetail.setSelectedTexture(index);


		//ResetDoubleClickTimer();
	}

	/// <summary>
	/// left swipe
	/// </summary>
	protected override void oneFingerSwipeLeft ()
	{
		
	}

	/// <summary>
	/// right swipe
	/// </summary>
	protected override void oneFingerSwipeRight ()
	{
		
	}

	protected override void oneFingerSwipeUp ()
	{

	}

	protected override void oneFingerSwipeDown ()
	{
		

	}

	//protected override void checkTouchStateOneFinger()
	//{
	//    if (!isRequireFingerReset)
	//    {
	//        base.checkTouchStateOneFinger();
	//    }
	//}

	protected override void touchStateOneFinger ()
	{
		if (!isRequireFingerReset) {
			base.touchStateOneFinger ();

		}
	}

	protected override void checkTouchStateTwoFinger ()
	{
		if (GlobalStaticVariable.RayhitObject == null) {
			return;
		}

		base.checkTouchStateTwoFinger ();

		// Don't allow user to change touch phase without re-touching screen
		if (!isRequireFingerReset) {
			isRequireFingerReset = true;
		}
	}

	#region No Finger Touch Functions

	protected override void touchStateNoFinger ()
	{
		if (GlobalStaticVariable.RayhitObject == null) {
			return;
		}

		base.touchStateNoFinger ();

		isCameraOverUI = false;
		isRequireFingerReset = false;
		isAutoFocus = true;
		userRotation = false;
		userScale = false;
	}

	#endregion

	#region One Finger Touch Functions

	protected override void touchStateOneFingerBegan ()
	{

	}

	protected override void touchStateOneFingerStationary ()
	{

	}

	protected override void touchStateOneFingerMoved ()
	{
		if (GlobalStaticVariable.RayhitObject == null) {
			return;
		}

		if (!isCameraOverUI && isTracked) {
			base.touchStateOneFingerMoved ();
		}
	}

	protected override void touchStateOneFingerEndedCanceled ()
	{
		Debug.Log ("touchStateOneFingerEndedCanceled");
		if (isAutoFocusTimer) {
			autoFocusTimer = 0.0f;
			//isAutoFocusTimer = false;
		}
		//userRotation = false;
		//userScale = false;
        
		// Check finger action if UI was not clicked on
		if (!isCameraOverUI) {
			// Determine the finger action to respond to in here

			// Set autofocus
			if (!isAnyFingerMoved) {
				Debug.Log ("!isAnyFingerMoved " + GlobalStaticVariable.RayhitObject);
				if (GlobalStaticVariable.RayhitObject == null) {
					checkTouchAutofocus ();
				} else if (isAutoFocus) {
					checkTouchAutofocus ();
				}
			}
			base.touchStateOneFingerEndedCanceled ();
		}
	}

	protected override void oneFingerMove ()
	{

	}

	private void checkTouchAutofocus ()
	{
		// Set autofocus
		try {
			CameraDevice.Instance.SetFocusMode (CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
		} catch {
		}
	}

	#endregion

	#region Two Finger Touch Functions

	protected override void touchStateTwoFingerBegan ()
	{

	}


	protected override void touchStateTwoFingerStationary ()
	{
		//Debug.Log("station");
		//playAnimation();
	}



	protected override void touchStateTwoFingerMoved ()
	{
    
		if (!isCameraOverUI) {
			base.touchStateTwoFingerMoved ();

		
		}
	}

	protected override void twoFingerSwipeDown ()
	{

	}

	protected override void twoFingerSwipeLeft ()
	{

	}

	protected override void twoFingerSwipeRight ()
	{

	}

	protected override void twoFingerSwipeUp ()
	{

	}

	protected override void twoFingerPinch ()
	{
		Debug.Log ("Pinch");
		scaleObject (/** aspectRatio * 0.001f*/);
	}

	protected override void twoFingerMove ()
	{

	}

	protected override void twoFingerRotate ()
	{
		
	}

	#endregion

	/// <summary>
	///  rotate object
	/// </summary>
	/// <param name="rotatedAngle"></param>
	public void rotateObject (float rotatedAngle)
	{
		
	}




	/// <summary>
	/// Resets the item detail.
	/// </summary>


	/// <summary>
	/// Scale the current object
	/// </summary>
	public void scaleObject ()
	{
		if (GlobalStaticVariable.RayhitObject != null && GlobalStaticVariable.RayhitObject.GetComponent<AnimatorCoreController> () != null && GlobalStaticVariable.RayhitObject.GetComponent<ControllerVariable> () != null) {
			if (GlobalStaticVariable.RayhitObject.GetComponent<AnimatorCoreController> ().animatorStatus != AnimatorCoreController.AnimationStatus.ACTIVE && GlobalStaticVariable.RayhitObject.GetComponent<ControllerVariable> ().isScaleAllowed) {
				float scaleValue = GlobalStaticVariable.RayhitObject.transform.localScale.x;
				//Vector3 maxScale = new Vector3(0.5f, 0.5f, 0.5f);
				//Vector3 minScale = new Vector3(0.4f, 0.4f, 0.4f);
				float newScaleValue = fingerPinchDistance * 0.0005f * scaleSpeed;
				scaleValue += newScaleValue;

				//CCScreenLogger.LogStatic (newScaleValue.ToString (), 0);
				//CCScreenLogger.LogStatic (scaleValue.ToString (), 1);

				if (scaleValue > maxScaleValue) {
					scaleValue = maxScaleValue;
				} else if (scaleValue < minScaleValue) {
					scaleValue = minScaleValue;
				}
				GlobalStaticVariable.RayhitObject.transform.localScale = new Vector3 (scaleValue, scaleValue, scaleValue);
			}
		}
	}



	IEnumerator waitForAnimationReady (Animation animation)
	{
		yield return new WaitForEndOfFrame ();
		animation.Stop ();
	}
}
