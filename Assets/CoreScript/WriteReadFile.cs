﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class WriteReadFile : MonoBehaviour {
	public const string fileName = "cms.txt";
	//错误不客气 simple chinese
	//繁體字 tradition chinese
	//Lebensmüde Torschlusspanik deutsch
	//enkele, sommige vrouw, echtgenote dutch
	// hello bye English
	// Mi scusi.   Prego Italiano 
	// おはようございます こんにちは お腹が空いています Janpanses
	// Oui, s'il vous plaît/Non, merci wee, seel voo play/nong, mair-see  S'il vous plaît seel voo play Fracne
	//  ¿Quiere comprarlo usted?  ¿Cómo se dice maybe en Español?  Hay muchos  ¿Cómo se dice maybe en Español? Espanol

	public static void WriteString(string text)
	{
		string path = Application.persistentDataPath + "/" + fileName;

		//Write some text to the test.txt file
		StreamWriter writer = new StreamWriter(path);
		writer.WriteLine(text);
		writer.Close();

		//Re-import the file to update the reference in the editor
		//AssetDatabase.ImportAsset(path);
		//TextAsset asset = Resources.Load("test");

		//Print the text from the file
		//Debug.Log(asset.text);
	}


	public static string ReturnTextFile()
	{
		string path = Application.persistentDataPath + "/" + fileName;
		if (File.Exists(path))
		{
			//Read the text from directly from the test.txt file
			StreamReader reader = new StreamReader(path);
			string jsonText = reader.ReadToEnd();
			reader.Close();
			return jsonText;
		}
		else {
			Debug.Log("Empty Json");
			return "";
		}
	}
}
