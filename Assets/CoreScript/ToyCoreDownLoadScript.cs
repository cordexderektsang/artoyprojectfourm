﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CCARPlatformFramework.CCConstants;
using CCARPlatformFramework.CCDataHandling;
using CCARPlatformFramework.CCTools;
using System.IO;
using Vuforia;

public class ToyCoreDownLoadScript : MonoBehaviour {


	public const string MainFolder = "4M";
	public const string MainFolderTemp = "4M";
	public const string MainFolderBeta = "4MBeta";
	public const string JsonFolder = "/Jsons";
	public const string LanguageFolder = "/Language";
	public const string ObjectsFolder = "/Objects";
	public const string PictureFolder = "/Pictures";
	public const string SceneFolder = "/Scenes";
	public const string AudioFolder = "/Audio";
	public const string XMlFolder = "/Xml";
	public const string UIFolder = "/UI";
	public const string Android = "/Android";
	public const string IOS = "/IOS";
	public string PersistentPath = "";
	public bool isDownloadStart = false;
	public GameObject closeButtonForNetwork;
	public string downLoadURL ="";
	public bool isNetworkError = false;
	public string path = "";
	public string fileName = "";
	public string bundlePath = "";
	public string bundleFile = "";
	public bool isLocal = true;
	public bool isBeta = false;
	public ScaleScroller ScaleScroller;
	public LoadingBar loadingBar;
	public ButtomFlow buttomFlow;
	public GameObject maintainPopUp;
	public GameObject versionPopUp;
	public bool isJsonExist = false;

	private LoadJson loadJson;
	private bool isFinsishDownloadTask = true;
	private FailDownloadInfor failDownloadInfor;
	private CCDownloadController downloadController;
	private bool failForVersion = false;
	private bool failForMaintain = false;
	private bool onClickCustomLanguageDownload = false;




	//public delegate void DisconnectFunction (string downLoadURL , string path , string fileName ,FileDownloadType typeOfDownload , int index);
	//public DisconnectFunction downloadFunction;

	public enum FileDownloadType {MarkerAndObjectJson, MenuJson, LanguageJson ,MenuPng, LanguagePng,LanguageBundle, SceneBundle, ObjectBundle, MarkerBundle ,Nothing};
	private FileDownloadType typeOfDownload = FileDownloadType.Nothing;




	public void Awake ()
	{
		//WriteReadFile.WriteString();
		//WriteReadFile.ReadString();
		Debug.Log(CoreLanguage.LanguagePref);
		PersistentPath = Application.persistentDataPath;


		if (!GlobalStaticVariable.finishDownload) {
			loadingBar.gameObject.SetActive (true);
			if (!isBeta)
			{
				#if UNITY_IOS

				downLoadURL += MainFolder + IOS;
				#elif UNITY_ANDROID

				downLoadURL += MainFolder + Android;
				#endif
			}
			else {

				#if UNITY_IOS

				downLoadURL += MainFolderBeta + IOS;
				#elif UNITY_ANDROID

				downLoadURL += MainFolderBeta + Android;

				#endif

			}
		
			CreateDictionary ();

			if (isLocal) {
				ScaleScroller.enabled = true;
			} 
			
	
			
		}
		loadJson = GetComponent<LoadJson>();
		isJsonExist = loadJson.CheckIsAllFileExist();

		//GlobalStaticVariable.finishDownload = false;

	}


	public void LoadLanguagePack()
	{
		if (isJsonExist)
		{
			GetDeviceRegion.GetDeviceLanguage();
			loadJson.CheckLanguagePackage();
			if (!string.IsNullOrEmpty(CoreLanguage.LanguagePref) )
			{
	
				string languageLocalPathDatWithoutFileName = PersistentPath + LanguageFolder + "/";
				Debug.Log("local language check 222 " +LoadJson.LocalDefaultLauguage.fontPackageName);
				if (File.Exists(languageLocalPathDatWithoutFileName + LoadJson.LocalDefaultLauguage.fontPackageName + GlobalStaticVariable.AssetBundle_EXT) && File.Exists(languageLocalPathDatWithoutFileName + LoadJson.LocalDefaultLauguage.fontPackageName + GlobalStaticVariable.AssetBundle_EXT + GlobalStaticVariable.CHECKSUM_EXT) )
				{
					Debug.Log("local language check " + CoreLanguage.LanguagePref);
					LoadFontBundle(FileDownloadType.LanguageBundle, LoadJson.LocalDefaultLauguage.fontPackageName, languageLocalPathDatWithoutFileName, LoadJson.LocalDefaultLauguage.fontPackageName + GlobalStaticVariable.AssetBundle_EXT);
				}
			}
		}

	}

	public IEnumerator CheckForMaintain()
	{

		isFinsishDownloadTask = false;
		string url = downLoadURL + "/" + "maintain" + GlobalStaticVariable.TXT_EXT;
		Debug.Log ("===== maintain check ====");
		WWW www = new WWW(url);
		float timer = 0;
		float timeOut = 3;
		bool fail = false;

		while (!www.isDone) {
			
			if (timer > timeOut) {
				fail = true;
				failForMaintain = true;
				NetworkCoreCheckHandler.isNetworkError = true;
				yield break;
			}
			timer += Time.deltaTime;
			if (string.IsNullOrEmpty (www.error)) {
			//	Debug.Log (" no errror");
				yield return null;
			} else {
				//Debug.Log ("errror");

			}

		}
		//yield return www;
		//Debug.Log (www.error);
		if (!string.IsNullOrEmpty(www.text) && NetworkCoreCheckHandler.CheckInternet() && string.IsNullOrEmpty(www.error)&& !isFinsishDownloadTask)
		{
			Debug.Log ("===== maintain check Successfull ====");
			isFinsishDownloadTask = true;
			NetworkCoreCheckHandler.isNetworkError = false;
			if (www.text.Equals("1"))
			{

				maintainPopUp.SetActive(true);

			}
			else {

				StartCoroutine(CheckForVersion());
			}
		}
		else {
			Debug.Log ("===== maintain check fail ====");
			failForMaintain = true;
			NetworkCoreCheckHandler.isNetworkError = true;

		}

	}




	public IEnumerator CheckForVersion()
	{
		string url = downLoadURL + "/" + "version" + GlobalStaticVariable.TXT_EXT;
		Debug.Log ("===== version check ====");
		WWW www = new WWW(url);
		isFinsishDownloadTask = false;
		//yield return www;
		float timer = 0;
		float timeOut = 3;
		bool fail = false;
		while (!www.isDone) {

			if (timer > timeOut) {
				fail = true;
				failForVersion = true;
				NetworkCoreCheckHandler.isNetworkError = true;
				yield break;
			}
			timer += Time.deltaTime;
			if (string.IsNullOrEmpty (www.error)) {
				//Debug.Log (" version no errror");
				yield return null;
			} else {
				//Debug.Log (" version errror");

			}


		}
		if (!string.IsNullOrEmpty(www.text) && NetworkCoreCheckHandler.CheckInternet() && string.IsNullOrEmpty(www.error) && !isFinsishDownloadTask)
		{
			isFinsishDownloadTask = true;
			if (www.text.Equals(Application.version))
			{

				DownloadLanguageJson();
				DownloadMarkerAndObjectJson();
			}
			else {

				versionPopUp.SetActive(true);

			}
		}
		else {
			NetworkCoreCheckHandler.isNetworkError = true;
			failForVersion = true;
		
		}

	}

	public void DownloadJsonOnButtonCall()
	{

		DownloadLanguageJson();
		DownloadMarkerAndObjectJson();
	}





	public void Start()
	{
		//isJsonExist = loadJson.CheckIsAllFileExist();

		closeButtonForNetwork.SetActive(true);
		CoreLanguage.label = Resources.FindObjectsOfTypeAll(typeof(UILabel)) as UILabel[];
		LoadLanguagePack();
		Debug.Log(isJsonExist + " : " + NetworkCoreCheckHandler.CheckInternet());
		if (isJsonExist && !NetworkCoreCheckHandler.CheckInternet ()) {

			LocalLoading ();
			loadJson.CompleteAllDownloadReplaceJson ();
			GlobalStaticVariable.finishDownload = true;

		} else if (!GlobalStaticVariable.finishDownload) {
			isDownloadStart = true;
			StartCoroutine (CheckForMaintain ());
			//DownloadLanguageJson();
			//DownloadMarkerAndObjectJson();

		} else {
			LocalLoading ();

		}
	}

	public void LocalLoading()
	{

		if (LoadJson.languageFunction != null)
		{
			LoadJson.languageFunction();
		}
		loadingBar.gameObject.SetActive(false);
	}


	public void DownloadMarkerAndObjectJson()
	{
	/*	if (isJsonExist)
		{
			closeButtonForNetwork.SetActive(true);
		}*/
		//Download Language Json
		//string MarkerAndObjectPath = downLoadURL + JsonFolder + "/" + LoadJson.MarkerAndObjectListUnicorn ;
		//string languagePath = downLoadURL + MainFolder + JsonFolder + "/" + "lalal" ;
		//string saveMarkerAndObjectPath = PersistentPath + JsonFolder + "/";
		//Debug.Log (" ======step 3 Start Download MarkerAndObject Json=====");
		//DownloadFunction (MarkerAndObjectPath, saveMarkerAndObjectPath, LoadJson.MarkerAndObjectListUnicorn, FileDownloadType.MarkerAndObjectJson);
		StartCoroutine(DownLoadDownloadMarkerAndObjectJsonSequence());

	}

	public IEnumerator DownLoadDownloadMarkerAndObjectJsonSequence()
	{
		while (true)
		{
			if (!isFinsishDownloadTask)
			{
				yield return null;
			}
			else if (isFinsishDownloadTask)
			{
				string MarkerAndObjectPath = downLoadURL + JsonFolder + "/" + LoadJson.MarkerAndObjectListUnicorn;
				//string languagePath = downLoadURL + MainFolder + JsonFolder + "/" + "lalal" ;
				string saveMarkerAndObjectPath = PersistentPath + JsonFolder + "/";
				Debug.Log(" ======step 3 Start Download MarkerAndObject Json=====");
				DownloadFunction(MarkerAndObjectPath, saveMarkerAndObjectPath, LoadJson.MarkerAndObjectListUnicorn, FileDownloadType.MarkerAndObjectJson);
				break;
			}

		}

	}

	public void DownloadLanguageJson()
	{
		//Download Language Json
		string languagePath = downLoadURL + JsonFolder + "/" + LoadJson.languageList ;
		//string languagePath = downLoadURL + MainFolder + JsonFolder + "/" + "lalal" ;
		string saveLanguagePath = PersistentPath + JsonFolder + "/";
		Debug.Log ("===== step 2 Start Download Language Json ======");
		DownloadFunction (languagePath, saveLanguagePath, LoadJson.languageList, FileDownloadType.LanguageJson);

	}

	/*public void DownloadMenuItemJson()
	{

		//Download Menu Item Json
		string menuItemsPath = downLoadURL  + JsonFolder + "/" + LoadJson.listOfSceneAndUI;
		string saveMenuItemsPath = PersistentPath + JsonFolder + "/";
		Debug.Log ("Download Menu Json");
		DownloadFunction (menuItemsPath,saveMenuItemsPath, LoadJson.listOfSceneAndUI,FileDownloadType.MenuJson);

	}*/


	public void DownloadPicturePNG (){

		StartCoroutine (DownLoadPictureSequence());

	}

	public IEnumerator DownLoadPictureSequence ()
	{
		string savePicturePath = PersistentPath + PictureFolder + "/";
		//int indexForLoopsForMenu= 0;
		int indexForLoopsForLanguage= 0;
		while (true) {
			yield return new  WaitForSeconds (0.01f);
			if (!isFinsishDownloadTask) {
				yield return null;
			} else if (isFinsishDownloadTask) {

				// Menu Image
				/*if (indexForLoopsForMenu < LoadJson.LocalMenuJsonDataReference.Count) {
					Debug.Log ("======== step 2 === Download  Menu Png =============");
	
					DownloadMenuPNG (indexForLoopsForMenu , savePicturePath);
					indexForLoopsForMenu++;

				} else*/
				// Language Image
				if (indexForLoopsForLanguage < LoadJson.LocalLanguageJsonDataReference.Count) {
				//	Debug.Log ("======== step 4 ===  Downloading  Language Png ============= start with index : " + indexForLoopsForLanguage +" : " +  LoadJson.LocalLanguageJsonDataReference.Count + " : "+  LoadJson.LocalLanguageJsonDataReference[indexForLoopsForLanguage].languageName);

					DownloadLanguagePNG (indexForLoopsForLanguage,savePicturePath);
					indexForLoopsForLanguage++;

				}

				if ( indexForLoopsForLanguage == LoadJson.LocalLanguageJsonDataReference.Count ) {
					Debug.Log ("======== step 5 === Start Download Object  and  other Items =============");
					DownloadMarkerAndObject ();
				
					break;
				}


			}
		}

	} 

/*	public void DownloadMenuPNG (int indexForLoopsForMenu, string savePicturePath)
	{

		string menuLocalPicturePath = PersistentPath + PictureFolder + "/" + LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName + GlobalStaticVariable.PNG;
		if (LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].isDownload || !IsFileckmExist(menuLocalPicturePath+GlobalStaticVariable.CHECKSUM_EXT)) {
			Debug.Log ("Download For menu pictrue For " + LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName );
			string menuPicturePath = downLoadURL  + PictureFolder + "/" + LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName;
			DownloadFunction (menuPicturePath, savePicturePath, LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName + GlobalStaticVariable.PNG, FileDownloadType.MenuPng, indexForLoopsForMenu);

		} else {
			Debug.Log ("Load local For menu pictrue"  + LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName );
			string saveMenuPath = PersistentPath + PictureFolder + "/";
			loadJson.SwitchCaseForLoadImage (saveMenuPath, LoadJson.LocalMenuJsonDataReference [indexForLoopsForMenu].pictureName + GlobalStaticVariable.PNG, FileDownloadType.MenuPng, indexForLoopsForMenu);
			loadingBar.wwwProgressForAppStart (1);

		}

	}*/

	public void DownloadLanguagePNG(int indexForLoopsForLanguage, string savePicturePath)
	{

		string languageLocalPicturePath = PersistentPath + PictureFolder + "/" + LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName + GlobalStaticVariable.PNG;
		Debug.Log (LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].isDownload + " : " + languageLocalPicturePath+GlobalStaticVariable.CHECKSUM_EXT);
		if (LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].isLocal) {
			// do nothing let it load from resource
			Debug.Log ("Local For Language pictrue");
			loadingBar.wwwProgressForAppStart(1,true);
		
		} else if (LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].isDownload || !IsFileckmExist(languageLocalPicturePath+GlobalStaticVariable.CHECKSUM_EXT)) {
			Debug.Log ("Download For Language pictrue"  + LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName );

			string languagePicturePath = downLoadURL + PictureFolder + "/" + LoadJson.LocalLanguageJsonDataReference[indexForLoopsForLanguage].languageName + GlobalStaticVariable.PNG;

			if (IsFileckmExist(languagePicturePath + GlobalStaticVariable.CHECKSUM_EXT) && IsFileckmExist(languagePicturePath))
			{
				File.Copy(languagePicturePath, languagePicturePath + GlobalStaticVariable.Temp, true);
			}

			DownloadFunction (languagePicturePath, savePicturePath, LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName + GlobalStaticVariable.PNG, FileDownloadType.LanguagePng, indexForLoopsForLanguage);
		} else if (!LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].isLocal) {
			Debug.Log ("Load local For Language pictrue" + LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName);
			string saveLanguagePath = PersistentPath + PictureFolder + "/";
			loadJson.SwitchCaseForLoadImage (saveLanguagePath, LoadJson.LocalLanguageJsonDataReference [indexForLoopsForLanguage].languageName + GlobalStaticVariable.PNG, FileDownloadType.LanguagePng, indexForLoopsForLanguage);
			loadingBar.wwwProgressForAppStart(1, true);
		}



	}


	public void DownloadLanguagePack (string languagePack)
	{

		string saveLanguagePath = PersistentPath + LanguageFolder ;
		string downloadLanguagePath = downLoadURL  + LanguageFolder + "/"  + languagePack.ToLower() +GlobalStaticVariable.AssetBundle_EXT;
		Debug.Log ("=========== DownLoad Language Language ===== :  " + languagePack);
		isDownloadStart = true;
		onClickCustomLanguageDownload = true;
		GlobalStaticVariable.finishDownload = false;
		DownloadFunction (downloadLanguagePath,saveLanguagePath,languagePack.ToLower()+GlobalStaticVariable.AssetBundle_EXT, FileDownloadType.LanguageBundle);

	}

	public void DownloadObjectAndMarkerJson (string FileName)
	{

		string savebjectAndMarkerPath = PersistentPath + JsonFolder ;
		string downloadLanguagePath = downLoadURL  + JsonFolder + "/"  + FileName;
		Debug.Log ("====== Download Marker and Object Json ===== " + downloadLanguagePath + " Saved To : " + savebjectAndMarkerPath);
		DownloadFunction (downloadLanguagePath,savebjectAndMarkerPath,FileName, FileDownloadType.MarkerAndObjectJson);

	}


	public void DownloadMarkerAndObject ()
	{

		StartCoroutine (DownLoadMarkerAndObjectSequence());

	}
		


	public IEnumerator DownLoadMarkerAndObjectSequence ()
	{
		string saveXmlPath = PersistentPath + XMlFolder + "/";
		string saveObjectPath = PersistentPath + ObjectsFolder + "/";
		string saveScenePath = PersistentPath + SceneFolder + "/";
		string saveAudioPath = PersistentPath + AudioFolder + "/";
		string saveUIPath = PersistentPath + UIFolder + "/";
		int indexForLoopsForObject= 0;
		//int indexForLoopsForMarker= 0;
		//bool startSceneDownload = false;
		bool startAudioDownload = false;
		//bool startUIDownload = false;
		bool startLanguageDownload = false;
		//bool downloadXml = false;
		bool downloadComplete = false;

		while (true) {
			yield return new  WaitForSeconds (0.01f);
			if (!isFinsishDownloadTask) {
				yield return null;
			} else if (isFinsishDownloadTask) {

				if (indexForLoopsForObject < LoadJson.LocalObjectJsonDataReference.Count) {
					Debug.Log ("==========Step 6 For Download Scene Sequenece - Objects ==========");
					DownloadObject (saveObjectPath, indexForLoopsForObject);
					indexForLoopsForObject++;

				} /*else if (indexForLoopsForMarker < LoadJson.LocalMarkerJsonDataReference.Count) {
					Debug.Log ("==========Step 7 For Download Scene Sequenece - Markers ==========");
					DownloadMarker (saveXmlPath, downloadXml, indexForLoopsForMarker);
					indexForLoopsForMarker++;

				}*/ else {
					startAudioDownload = true;
					/*if (!startSceneDownload) {
						startSceneDownload = DownloadScene (saveScenePath, startSceneDownload);
					}
					if (!startAudioDownload) {
						startAudioDownload = DownloadAudio (saveAudioPath, startSceneDownload, startAudioDownload);
					}
					if (!startUIDownload) {
						startUIDownload = DownloadUI (saveUIPath, startAudioDownload, startUIDownload);
					}*/
					if (startAudioDownload) {
						startLanguageDownload = DownLoadLanguageFontSequence();
					}
					if (!downloadComplete)
					{
						downloadComplete = DownloadComplete(startLanguageDownload, downloadComplete);

					}
					if (downloadComplete && isFinsishDownloadTask)
					{
						break;

					}

				} 
			}
		}

	}



	public bool DownLoadLanguageFontSequence()
	{
		string languageLocalPathDatWithoutFileName = PersistentPath + LanguageFolder + "/";
		string saveLanguageFontPath = PersistentPath + LanguageFolder ;
		string downloadLanguagePath = downLoadURL + LanguageFolder + "/" + LoadJson.LocalDefaultLauguage.fontPackageName+ GlobalStaticVariable.AssetBundle_EXT;
		string languageLocalPathDat = PersistentPath + LanguageFolder + "/" + LoadJson.LocalDefaultLauguage.fontPackageName;
		if (!IsFileckmExist(languageLocalPathDat + GlobalStaticVariable.AssetBundle_EXT + GlobalStaticVariable.CHECKSUM_EXT) && !LoadJson.LocalDefaultLauguage.fontPackageName.Equals(CoreLanguage.DefaultFontTypeName))
		{
			Debug.Log("======Step 8 Download Language Font ===== " + downloadLanguagePath + " Saved To : " + saveLanguageFontPath);
			if (IsFileckmExist(languageLocalPathDat + GlobalStaticVariable.AssetBundle_EXT + GlobalStaticVariable.CHECKSUM_EXT) && IsFileckmExist(languageLocalPathDat  +GlobalStaticVariable.AssetBundle_EXT))
			{
				Debug.Log("Copy old Version Language package");
				File.Copy(languageLocalPathDat + GlobalStaticVariable.AssetBundle_EXT, languageLocalPathDat + GlobalStaticVariable.AssetBundle_EXT + GlobalStaticVariable.Temp, true);
			}

			DownloadFunction(downloadLanguagePath, saveLanguageFontPath, LoadJson.LocalDefaultLauguage.fontPackageName + GlobalStaticVariable.AssetBundle_EXT, FileDownloadType.LanguageBundle);
			return true;
		}
		else if (!LoadJson.LocalDefaultLauguage.fontPackageName.Equals(CoreLanguage.DefaultFontTypeName))
		{
			Debug.Log("======Step 8 Load Local Language Font ===== " + downloadLanguagePath + " Saved To : " + saveLanguageFontPath);
			LoadFontBundle(FileDownloadType.LanguageBundle, LoadJson.LocalDefaultLauguage.fontPackageName , languageLocalPathDatWithoutFileName, LoadJson.LocalDefaultLauguage.fontPackageName + GlobalStaticVariable.AssetBundle_EXT);
			loadingBar.wwwProgressForAppStart(1, true);
			return true;

		}
		else {
			Debug.Log("======Step 8 Load Local Language Font ===== " + CoreLanguage.LanguagePref );
			loadingBar.wwwProgressForAppStart(1, true);
			return true;
		}

	}

	public bool DownloadComplete (bool startLanguageDownload , bool downloadComplete){

	//	Debug.Log(startLanguageDownload + " : " + downloadComplete + " : " + isFinsishDownloadTask);
		if(startLanguageDownload && !downloadComplete && isFinsishDownloadTask){
			GlobalStaticVariable.finishDownload = true;
			isDownloadStart = false;
			if (LoadJson.languageFunction != null)
			{
				LoadJson.languageFunction();
			}
			loadJson.CompleteAllDownloadReplaceJson ();
			//DeleteTemp ();
			Debug.Log ("==========Step 9 For Download Scene Sequenece - Complete ==========");
			loadingBar.TurnOffLoadingPage ();
		
			return true;
		}
		return false;

	}

	/*public bool DownloadUI (string saveUIPath ,bool startAudioDownload , bool startUIDownload )
	{

		if (startAudioDownload && isFinsishDownloadTask && !startUIDownload) {
			Debug.Log ("==========Step 7 For Download Scene Sequenece - UI ==========");
			string audioLocalUIPathDat = PersistentPath + UIFolder + "/" + LoadJson.LocalUIJsonDataReference [0].UIName;
			if (LoadJson.LocalUIJsonDataReference [0].isDownload || !IsFileckmExist (audioLocalUIPathDat + GlobalStaticVariable.AssetBundle_EXT + GlobalStaticVariable.CHECKSUM_EXT)) {
				Debug.Log ("Download For UI  For " + LoadJson.LocalUIJsonDataReference [0].UIName);
				string datDownloadPath = downLoadURL  + UIFolder + "/" + LoadJson.LocalUIJsonDataReference [0].UIName.ToLower () + GlobalStaticVariable.AssetBundle_EXT;
				//LoadJson.LocalUIJsonDataReference [0].UIPath = saveUIPath;
				//LoadJson.LocalUIJsonDataReference [0].UIFileName = LoadJson.LocalUIJsonDataReference [0].UIName.ToLower ()+ GlobalStaticVariable.AssetBundle_EXT;
				DownloadFunction (datDownloadPath, saveUIPath, LoadJson.LocalUIJsonDataReference [0].UIName.ToLower () + GlobalStaticVariable.AssetBundle_EXT, FileDownloadType.SceneBundle, 0);

			} else {
				Debug.Log ("Local For Ui  For " + LoadJson.LocalUIJsonDataReference [0].UIName);
				//LoadJson.LocalUIJsonDataReference [0].UIPath = saveUIPath;
				//LoadJson.LocalUIJsonDataReference [0].UIFileName = LoadJson.LocalUIJsonDataReference [0].UIName.ToLower () + GlobalStaticVariable.AssetBundle_EXT;
				loadingBar.wwwProgressForAppStart (1, true);
			}


			return true;

		}
		return false;
	}

/	public bool DownloadAudio (string saveAudioPath ,bool startSceneDownload , bool startAudioDownload )
	{

		if (startSceneDownload && isFinsishDownloadTask && !startAudioDownload) {
			Debug.Log ("==========Step 8 For Download Scene Sequenece - Audio ==========");
			string audioLocalPicturePathDat = PersistentPath + AudioFolder + "/" + LoadJson.LocalAudioJsonDataReference [0].AudioName + GlobalStaticVariable.AssetBundle_EXT;
			Debug.Log ("audio " + LoadJson.LocalAudioJsonDataReference [0].isDownload + " : " + IsFileckmExist (audioLocalPicturePathDat + GlobalStaticVariable.AssetBundle_EXT + GlobalStaticVariable.CHECKSUM_EXT)  + " path : " + audioLocalPicturePathDat + GlobalStaticVariable.AssetBundle_EXT + GlobalStaticVariable.CHECKSUM_EXT);
			if (LoadJson.LocalAudioJsonDataReference [0].isDownload || !IsFileckmExist (audioLocalPicturePathDat + GlobalStaticVariable.AssetBundle_EXT + GlobalStaticVariable.CHECKSUM_EXT)) {
				Debug.Log ("Download For Audio  For " + LoadJson.LocalAudioJsonDataReference [0].AudioName);
				string datDownloadPath = downLoadURL  + AudioFolder + "/" + LoadJson.LocalAudioJsonDataReference [0].AudioName.ToLower () + GlobalStaticVariable.AssetBundle_EXT;
				LoadJson.LocalAudioJsonDataReference [0].AudioPath = saveAudioPath;
				LoadJson.LocalAudioJsonDataReference [0].AudioName = LoadJson.LocalAudioJsonDataReference [0].AudioName.ToLower () + GlobalStaticVariable.AssetBundle_EXT;
				DownloadFunction (datDownloadPath, saveAudioPath, LoadJson.LocalAudioJsonDataReference [0].AudioName.ToLower () + GlobalStaticVariable.AssetBundle_EXT, FileDownloadType.SceneBundle, 0);

			} else {
				Debug.Log ("Local For Audio  For " + LoadJson.LocalAudioJsonDataReference [0].AudioName);
				LoadJson.LocalAudioJsonDataReference [0].AudioPath = saveAudioPath;
				LoadJson.LocalAudioJsonDataReference [0].AudioName = LoadJson.LocalAudioJsonDataReference [0].AudioName.ToLower ()+ GlobalStaticVariable.AssetBundle_EXT;
				loadingBar.LocalProgressForMenu (1);
			}


			return true;
		}
		return false;
	}*/


	/*public bool DownloadScene (string saveScenePath  ,bool startSceneDownload)
	{

		if (!string.IsNullOrEmpty (GlobalStaticVariable.CurrentOnClickSceneName) && isFinsishDownloadTask && !startSceneDownload) {
			Debug.Log ("==========Step 7 For Download Scene Sequenece - Scene ==========");
			string localScenePathDat = PersistentPath + SceneFolder + "/" + LoadJson.LocalSceneJsonDataReference [0].SceneName;
			if (LoadJson.LocalSceneJsonDataReference [0].isDownload || !IsFileckmExist (localScenePathDat + GlobalStaticVariable.AssetBundle_EXT + GlobalStaticVariable.CHECKSUM_EXT)) {
				Debug.Log ("Download For Scene  For " + GlobalStaticVariable.CurrentOnClickSceneName);
				string datDownloadPath = downLoadURL  + SceneFolder + "/" + GlobalStaticVariable.CurrentOnClickSceneName.ToLower () + GlobalStaticVariable.AssetBundle_EXT;
				LoadJson.LocalSceneJsonDataReference [0].ScenePath = saveScenePath;
				LoadJson.LocalSceneJsonDataReference [0].SceneFileName = GlobalStaticVariable.CurrentOnClickSceneName.ToLower () +  GlobalStaticVariable.AssetBundle_EXT;
				DownloadFunction (datDownloadPath, saveScenePath, GlobalStaticVariable.CurrentOnClickSceneName.ToLower () + GlobalStaticVariable.AssetBundle_EXT, FileDownloadType.SceneBundle, 0);
			} else {
				LoadJson.LocalSceneJsonDataReference [0].ScenePath = saveScenePath;
				LoadJson.LocalSceneJsonDataReference [0].SceneFileName = GlobalStaticVariable.CurrentOnClickSceneName.ToLower ()+ GlobalStaticVariable.AssetBundle_EXT;
				Debug.Log ("Local For Scene  For " + GlobalStaticVariable.CurrentOnClickSceneName);
				loadingBar.LocalProgressForMenu (1);
			}

			return true;
		}

		return false;

	}

	public void DownloadMarker (string saveXmlPath  ,bool downloadXml , int indexForLoopsForMarker)
	{
		if (isFinsishDownloadTask && !downloadXml) {
			string markerLocalPicturePathXml = PersistentPath + XMlFolder + "/" + LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerXmlName;
			if (LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].isDownload || !IsFileckmExist (markerLocalPicturePathXml + GlobalStaticVariable.CHECKSUM_EXT)) {
				Debug.Log ("Download For  xml Marker  For " + LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerXmlName);
				string xmLDownloadPath = downLoadURL  + XMlFolder + "/" + LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerXmlName;
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerXmlPath = saveXmlPath;
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerXmlFileName = LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerXmlName;
				DownloadFunction (xmLDownloadPath, saveXmlPath, LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerXmlFileName, FileDownloadType.MarkerBundle, indexForLoopsForMarker);
				downloadXml = true;

			} else {

				Debug.Log ("local For marker xml ");
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerXmlPath = saveXmlPath;
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerXmlFileName = LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerXmlName;
				downloadXml = true;
			}
		}
		if (isFinsishDownloadTask && downloadXml) {
			string markerLocalPicturePathDat = PersistentPath + XMlFolder + "/" + LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerDatName;

			if (LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].isDownload || !IsFileckmExist (markerLocalPicturePathDat + GlobalStaticVariable.CHECKSUM_EXT)) {
				Debug.Log ("Download For dat  Marker  For " + LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerDatName);
				string datDownloadPath = downLoadURL  + XMlFolder + "/" + LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerDatName;
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerDatPath = saveXmlPath;
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerDatFileName = LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerDatName;
				DownloadFunction (datDownloadPath, saveXmlPath, LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerDatFileName, FileDownloadType.MarkerBundle, indexForLoopsForMarker);
				downloadXml = false;
				indexForLoopsForMarker++;

			} else {
				Debug.Log ("local For marker Dat");
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerDatPath = saveXmlPath;
				LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].MarkerDatFileName = LoadJson.LocalMarkerJsonDataReference [indexForLoopsForMarker].markerDatName;
				downloadXml = false;
				indexForLoopsForMarker++;
				loadingBar.wwwProgressForAppStart (1,true);
			}
		}

	}*/



	public void DownloadObject (string saveObjectPath , int indexForLoopsForObject)
	{
		string objectLocalPicturePath = PersistentPath + ObjectsFolder + "/" + LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName + GlobalStaticVariable.AssetBundle_EXT;
		Debug.Log (objectLocalPicturePath + " : " + IsFileckmExist (objectLocalPicturePath + GlobalStaticVariable.CHECKSUM_EXT) + " : " + LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].isDownload);

		if (LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].isDownload || !IsFileckmExist (objectLocalPicturePath + GlobalStaticVariable.CHECKSUM_EXT)) {
			Debug.Log ("Download For Object For " + LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName);
			string objectDownloadPath = downLoadURL  + ObjectsFolder + "/" + LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName + GlobalStaticVariable.AssetBundle_EXT;
			//LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].ObjectPath = saveObjectPath;
			//LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].ObjectFileName = LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName;
			if (IsFileckmExist(objectLocalPicturePath + GlobalStaticVariable.CHECKSUM_EXT) && IsFileckmExist(objectLocalPicturePath))
			{
				File.Copy(objectLocalPicturePath  ,objectLocalPicturePath +GlobalStaticVariable.Temp,true);
			}
			DownloadFunction (objectDownloadPath, saveObjectPath, LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName + GlobalStaticVariable.AssetBundle_EXT, FileDownloadType.ObjectBundle, indexForLoopsForObject);

		} else {
			Debug.Log ("local For Object For " + LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName);
			//LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].ObjectPath = saveObjectPath;
			//LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].ObjectFileName = LoadJson.LocalObjectJsonDataReference [indexForLoopsForObject].objectName;
			loadingBar.wwwProgressForAppStart (1,true);

		}

	}

	public void CreateDictionary ()
	{

		string jsonFolder = PersistentPath + JsonFolder;
		string languageFolder = PersistentPath + LanguageFolder;
		string objectsFolder = PersistentPath + ObjectsFolder;
		string picutrefolder = PersistentPath + PictureFolder;
		string sceneFolder = PersistentPath + SceneFolder;
		string xmlFolder = PersistentPath + XMlFolder;
		string audioFolder = PersistentPath + AudioFolder;
		string uiFolder = PersistentPath + UIFolder;
		if(!Directory.Exists(jsonFolder)){
			Directory.CreateDirectory (jsonFolder);
		}
		if(!Directory.Exists(languageFolder)){
			Directory.CreateDirectory (languageFolder);
		}
		if(!Directory.Exists(objectsFolder)){
			Directory.CreateDirectory (objectsFolder);
		}
		if(!Directory.Exists(picutrefolder)){
			Directory.CreateDirectory (picutrefolder);
		}
		if(!Directory.Exists(sceneFolder)){
			Directory.CreateDirectory (sceneFolder);
		}
		if(!Directory.Exists(xmlFolder)){
			Directory.CreateDirectory (xmlFolder);
		}
		if(!Directory.Exists(audioFolder)){
			Directory.CreateDirectory (audioFolder);
		}
		if(!Directory.Exists(UIFolder)){
			Directory.CreateDirectory (uiFolder);
		}


	}
	/* Download Flows
	 * step 1
	 * 1.check internet
	 * 2. get json for number of scenes , each scene name ,number of languages , each pictures
	 * 2.1 read json
	 * 2.2 set up nubmer of language for selection
	 * 2.3 add scene name to target button
	 * 2.4 download pictures.
	 * 2.5 during scroll disable
	 * 3 Download picture for menu display
	 * 4 if fail cycle from step 1 start
	 * step 2
	 * 5 check internet
	 * 5.5 check for is it trial or full
	 * 6.get json for download list - markers , unicorn , element then scene
	 * 6.1 read json
	 * 6.2 download markers xml and dat
	 * 6.3 download all unicorn
	 * 6.4 download all elements
	 * 6.5 download one scene
	 * 7 check sum for each bundle
	 * 8. if fail cycle from step 2 start
	 * 9. load Game.
	 * 10. fail carry on if can
	 */

	/* To do List
		Test 8/9 languages
		start write download flow
		start write/read json
		disconnect and reconnect flow
		progess bar and MB
		Xcode test put in album function
		photo UI for preview and share
		language option , audio option , scene option
		each audio source for each unicorn , background ,and micro
		online marker loading
		setup address for bundles , scenes upload and download test
		Get CMS notice for product information

	*/

	public void DownloadFunction (string downLoadURL , string path , string fileName ,FileDownloadType typeOfDownload , int index = 0)
	{
		Debug.Log(downLoadURL);
		failDownloadInfor = new FailDownloadInfor();
		failDownloadInfor.downLoadURL = downLoadURL;
		failDownloadInfor.path = path;
		failDownloadInfor.fileName = fileName;
		failDownloadInfor.typeOfDownload = typeOfDownload;
		failDownloadInfor.index = index;
		isFinsishDownloadTask = false;
		CCLoadDataManager.downloadFunction += loadingBar.wwwProgress;
		isFinsishDownloadTask = false;
		string pathForSum_Ext = path  + fileName + GlobalStaticVariable.CHECKSUM_EXT;
		/*if (File.Exists (pathForSum_Ext)) {
				Debug.Log ("Detele Path " + pathForSum_Ext);
			File.Delete (pathForSum_Ext);

		}*/
		downloadController = CCDownloadController.Instance;
		downloadController.DownloadSingleFile (downLoadURL, CCLoadDataManager.DownloadProgress, (WWW www) => {
		
						//Debug.Log("====="+www.progress  + " : " + downLoadURL);
			if(string.IsNullOrEmpty(www.error) && www.progress == 1)
			{
				//Debug.Log( path +" : " +  fileName);
				CCWriteDataManager.WriteToPath (path, fileName, www.bytes);	

							///special case for marker json and language json for timeout
				if((typeOfDownload == FileDownloadType.MarkerAndObjectJson && string.IsNullOrEmpty(www.text)) || (typeOfDownload == FileDownloadType.LanguageJson && string.IsNullOrEmpty(www.text))){
					NetworkCoreCheckHandler.isNetworkError = true;
					isFinsishDownloadTask = false;
				} else {

				SwitchCaseForJson(www.text,typeOfDownload);
				loadJson.SwitchCaseForLoadImage(path,fileName,typeOfDownload,index);
				CCWriteDataManager.WriteToPath(path, fileName + GlobalStaticVariable.CHECKSUM_EXT, "");
				LoadFontBundle(typeOfDownload,fileName,path,fileName);
				www.Dispose ();
				Debug.Log("============finish download ============== ");
				isFinsishDownloadTask = true;
				NetworkCoreCheckHandler.isNetworkError = false;

				}
				CCLoadDataManager.downloadFunction -= loadingBar.wwwProgress;

			}
			else
			{
				Debug.Log("error" + fileName + " : " + typeOfDownload.ToString());
				NetworkCoreCheckHandler.isNetworkError = true;
				failDownloadInfor = new FailDownloadInfor();
				failDownloadInfor.downLoadURL = downLoadURL;
				failDownloadInfor.path = path;
				failDownloadInfor.fileName = fileName;
				failDownloadInfor.typeOfDownload = typeOfDownload;
				failDownloadInfor.index = index;
				isFinsishDownloadTask = false;
				//DownloadFunction(downLoadURL,path,fileName,typeOfDownload,index);
				//RetryCurrentFailDownload();
			}



		},
			() =>
			{
				Debug.Log("fail Download" + fileName + " : " + typeOfDownload.ToString());
				NetworkCoreCheckHandler.isNetworkError = true;
				failDownloadInfor = new FailDownloadInfor();
				failDownloadInfor.downLoadURL = downLoadURL;
				failDownloadInfor.path = path;
				failDownloadInfor.fileName = fileName;
				failDownloadInfor.typeOfDownload = typeOfDownload;
				failDownloadInfor.index = index;
					isFinsishDownloadTask = false;
				//DownloadFunction(downLoadURL,path,fileName,typeOfDownload,index);
				//RetryCurrentFailDownload();

			});

	}

	public void StopDownload()
	{

		if (downloadController != null)
		{
			Debug.Log("here stop Download");
			downloadController.StopDownLoad();
		}
	}


	public void RetryCurrentFailDownload()
	{
		Debug.Log(failForMaintain + " : " +  failForVersion);
		NetworkCoreCheckHandler.isNetworkError = false;
		//loadingBar.wwwProgressForAppStart(-1, true);
		CCDownloadController.Instance.StopDownLoad ();
		if (failForMaintain)
		{

			StartCoroutine(CheckForMaintain());

		}
		else if (failForVersion)
		{
			StartCoroutine(CheckForVersion());

		}
		else if (loadingBar.isDownloadLanguage)
		{
			buttomFlow.ResumeOnClickDownloadLanguage();

		} else if (failDownloadInfor != null)
		{
			DownloadFunction(failDownloadInfor.downLoadURL,failDownloadInfor.path,failDownloadInfor.fileName,failDownloadInfor.typeOfDownload,failDownloadInfor.index);
		}
		
		
	}


	public void SwitchCaseForJson (string JsonText , FileDownloadType typeOfDownload)
	{
		switch(typeOfDownload){
		case FileDownloadType.LanguageJson:
			loadJson.ReadOnlineJsonForLanguage (JsonText);
			break;
		/*case FileDownloadType.MenuJson:
			loadJson.ReadOnlineJsonForMenu (JsonText);
			break;*/
		case FileDownloadType.MarkerAndObjectJson:
			loadJson.ReadOnlineJsonForMarkerAndObject (JsonText);
			break;
		}

	}



	public void LoadFontBundle (FileDownloadType typeOfDownload ,string LanguageName , string bundlePath, string bundleFile)
	{
		switch (typeOfDownload) {
		case FileDownloadType.LanguageBundle:
			if (onClickCustomLanguageDownload)
			{
				onClickCustomLanguageDownload = false;
				isDownloadStart = false;
				GlobalStaticVariable.finishDownload = true;
			}

			AssetBundle currentBuddle = CCAssetBundleManager.GetAssetBundle (bundlePath, bundleFile);
			string language = LanguageName.Replace (GlobalStaticVariable.AssetBundle_EXT, "");
			//Debug.Log ("===Load Language :====" + language);
			CoreLanguage.FontTypeBond = CCAssetBundleManager.GetFontFromAssetBundle (currentBuddle, language+CoreLanguage.FontBond );
			CoreLanguage.FontTypeRegular = CCAssetBundleManager.GetFontFromAssetBundle (currentBuddle, language+CoreLanguage.FontRegular );
			Debug.Log ("Font " + CoreLanguage.FontTypeBond + ":" +LanguageName);
			CoreLanguage.SearchForLanguagePack ();
			buttomFlow.OnClickDownloadStopLanguage();
			break;
		}

	}

	public bool IsFileckmExist (string path)
	{
		return File.Exists (path);
	}


	public void LoadAssetBundle ()
	{

		AssetBundle currentBuddle = CCAssetBundleManager.GetAssetBundle(bundlePath, bundleFile);
		string objectName = "";
		GameObject augmentationObject = (CCAssetBundleManager.GetGameObjectFromAssetBundle(currentBuddle, objectName));

	}


	void OnDestroy() 
	{
		
		CCLoadDataManager.downloadFunction = null;
	}

	/*
	 * json for number of scene
	 * json for number of languages
	 * download target scene
	 * download list of bundles
	 * set up marker
	 * 
	*/



}

public class FailDownloadInfor {

	public string downLoadURL;
	public string path;
	public string fileName;
	public ToyCoreDownLoadScript.FileDownloadType typeOfDownload;
	public int index;

}
