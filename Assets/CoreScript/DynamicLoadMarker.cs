﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System.IO;
using CCARPlatformFramework.CCConstants;
using CCARPlatformFramework.CCDataHandling;
using CCARPlatformFramework.CCTools;


public class DynamicLoadMarker : MonoBehaviour {



	public SetUpImageTarget setUpImageTarget;

	//public GameObject targetDisplayObject;
	private string markerDataSet ;
	private string VUmarkerDataSet ;
	private string fullPath = "";
	public GameLoadingBar gameLoadingBar;
	private VuforiaARController vuforia;
	// Use this for initialization

	public void Awake ()
	{
		//gameLoadingBar.resetLoadingPage ();
		//gameLoadingBar.gameObject.SetActive (true);
	}

	public void Start()
	{

		LoadObjectData ();
		//VuforiaBehaviour vb = GameObject.FindObjectOfType<VuforiaBehaviour>();
		//vb.RegisterVuforiaStartedCallback (() => LoadDataSet(markerName));
	

		//vuforia = VuforiaARController.Instance;
/*		foreach (MarkerJsonData data in LoadJson.LocalMarkerJsonDataReference) {
			string filePath = Application.persistentDataPath +"/"+ ToyCoreDownLoadScript.XMlFolder + "/"; +  data.markerXmlName ;
			Debug.Log ("= path =="+filePath + "data.markerXmlName : t " + data.markerXmlName );*/
		/*	bool isAlreadyLoad = false;
			foreach(DataSetVariable target in listofDataset)
			{
				Debug.Log (target.fileName + " : " + data.markerXmlName);
				if (target.fileName.Equals (data.markerXmlName)) {

					isAlreadyLoad = true;
				}

			}*/
			//	if (!isAlreadyLoad) {
				//	vuforia.RegisterVuforiaStartedCallback (() => LoadDataSet (filePath, data.markerXmlName));

				//} else {

				//	vuforia.RegisterVuforiaStartedCallback (LoadDataSetLocal);

				//}
			//}

		//vuforia.RegisterVuforiaStartedCallback (LoadDataSet);
		/*if (gameLoadingBar.gameObject.activeSelf && gameLoadingBar != null) {
			gameLoadingBar.StartFadeLoading ();
		}*/
	
	}





	/// <summary>
	/// generate all image object
	/// </summary>
	public void LoadDataSet(/*string filePath ,string fileName*/)
	{
		//fullPath = Path.Combine (Application.persistentDataPath ,markerDataSet);
		//Debug.Log (fullPath);
		//Debug.Log("step 5 load data set with following fileName : " + fileName);

			ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker> ();		//IEnumerable<DataSet> loadedDataSets = objectTracker.GetDataSets();

		foreach (MarkerJsonData data in LoadJson.LocalMarkerJsonDataReference) {
			//string filePath = data.MarkerDatPath + data.markerXmlName;
		string filePath = Application.persistentDataPath +"/"+ ToyCoreDownLoadScript.XMlFolder + "/"+  data.markerXmlName ;
			Debug.Log ("===" + filePath );
			bool isFileExist = File.Exists (filePath);
			//ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();		//IEnumerable<DataSet> loadedDataSets = objectTracker.GetDataSets();
			objectTracker.Stop ();
			Debug.Log ("is path here " + isFileExist);
			DataSet dataSet = objectTracker.CreateDataSet ();

			if (dataSet.Load (filePath, VuforiaUnity.StorageType.STORAGE_ABSOLUTE)) {
				Debug.Log (" Loading : " + data.markerXmlName + " Do File Path Exist : " + isFileExist);
				objectTracker.Stop ();
				if (!objectTracker.ActivateDataSet (dataSet)) {
					// Note: ImageTracker cannot have more than 100 total targets activated
					Debug.Log ("<color=yellow>Failed to Activate DataSet: " + data.markerXmlName + "</color>");
				}

				if (!objectTracker.Start ()) {
					Debug.Log ("<color=yellow>Tracker Failed to Start.</color>");
				}
				objectTracker.Start ();
				//DataSetVariable dataReference = new DataSetVariable ();
				//dataReference.dataSet = dataSet;
				//dataReference.fileName = data.markerXmlName;
				//Debug.Log ("add dataset : " + dataReference.fileName);
				//listofDataset.Add (dataReference);

			} else {

				Debug.Log ("fail");
				//objectTracker.DestroyDataSet (dataSet,true);
				//LoadDataSet (fileName);
			}
		}

		/*foreach (DataSet loadedDataSet in loadedDataSets)
		{
			Debug.Log("testing the path: " + loadedDataSet.Path);

		}*/

		//IEnumerable<TrackableBehaviour> tbs = TrackerManager.Instance.GetStateManager().GetTrackableBehaviours();
		//IEnumerable<VuMarkAbstractBehaviour> vu = TrackerManager.Instance.GetStateManager().GetVuMarkManager().GetAllBehaviours();

		/*foreach (VuMarkAbstractBehaviour tb in vu)
		{
			Debug.Log (tb.TrackableName);
				//if (data.trackableName.Equals (tb.TrackableName)) 
				//{
				// change generic name to include trackable name
				//tb.gameObject.name = tb.TrackableName;

				// add additional script components for trackable
				//tb.gameObject.AddComponent<CoreCustomTrackableEventHandler>();
				//GameObject target = Instantiate (targetDisplayObject);
			//Debug.Log ("lalal");
			//	target.transform.parent = tb.gameObject.transform;

				//dataPath.downLoadimageTargetJsonData (tb.TrackableName);										


		}*/
		LoadObjectData ();
		vuforia.UnregisterVuforiaStartedCallback (LoadDataSet);
	}
	void OnDestroy() {
		

		//print("Script was destroyed");

		/*ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();		//IEnumerable<DataSet> loadedDataSets = objectTracker.GetDataSets();
		objectTracker.Stop();
		IEnumerable<DataSet> dataset = objectTracker.GetDataSets ();
		foreach (DataSet loadedDataSet in dataset)
		{
			StateManager stateManager = TrackerManager.Instance.GetStateManager ();
			IEnumerable<TrackableBehaviour> tb = stateManager.GetTrackableBehaviours();
			foreach (TrackableBehaviour t in tb) 
			{
				if (loadedDataSet.Contains (t.Trackable)) 
				{
					stateManager.DestroyTrackableBehavioursForTrackable (t.Trackable, true);
				}
			}
			objectTracker.DeactivateDataSet (loadedDataSet);
			loadedDataSet.DestroyAllTrackables (true);
		}*/
	//	DestroyDataSets ();

		//DestoryAllImage ();
	}

	private void DestroyDataSets()
	 {
		 
		ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();;
		objectTracker.Stop();
		IEnumerable<DataSet> dataSets = objectTracker.GetActiveDataSets ();
		   
		foreach(DataSet target in dataSets)
			  {
			 
			   StateManager stateManager = TrackerManager.Instance.GetStateManager ();
				IEnumerable<TrackableBehaviour> tb = stateManager.GetTrackableBehaviours();
			   foreach (TrackableBehaviour t in tb) 
				   {
			//	if (target.Contains (t.Trackable)) 
					  //  {
					     stateManager.DestroyTrackableBehavioursForTrackable (t.Trackable, true);
					   // }
				   }
			 
			objectTracker.DeactivateDataSet (target);
			objectTracker.DestroyDataSet(target, true);
			 
			  }
		  
	}


	public void LoadObjectData ()
	{
		Debug.Log ("load Object");
		int i = 0;
		string path = Application.persistentDataPath + ToyCoreDownLoadScript.ObjectsFolder + "/";
		Debug.Log ("==================" + LoadJson.LocalObjectJsonDataReference.Count);
		foreach (ObjectJsonData data in LoadJson.LocalObjectJsonDataReference)
		{
			Debug.Log(data.objectName);
		//	Debug.Log ("==================" + setUpImageTarget.objectForTracker.Count);
			foreach (ImageObjectForEachTracker ImageData in  setUpImageTarget.objectForTracker)
			{
	
				if (data.associatedMarkerName.Equals (ImageData.markerTrackableName)) {
					Debug.Log ("======" +ImageData.markerTrackableName  + " ===" + data.associatedMarkerName );
					ImageData.gameObjectPosition = data.ObjectPosition;
					ImageData.gameObjectRotation = data.ObjectRotation;
					ImageData.gameObjectScale = data.ObjectScale;
					GameObject markerObject = LoadAssetBundle (/*data.ObjectPath*/path,data.objectName +GlobalStaticVariable.AssetBundle_EXT);
					ImageData.markerGameObject = markerObject;
					ImageData.vuMarkerId = data.VuMarkerId;
					//UnLoadAssetBundle (data.ObjectFileName + GlobalStaticVariable.AssetBundle_EXT);
					i++;

				}

			}
		}
		Debug.Log ("load Object : " + i );
		setUpImageTarget.gameObject.SetActive (true);


	}


	public void DestoryAllImage ()
	{
	/*	Debug.Log ("============DIEE==============" + setUpImageTarget.objectForTracker.Count );
		foreach (ImageObjectForEachTracker ImageData in  setUpImageTarget.objectForTracker)
		{
			if (ImageData.markerGameObject != null) {
				Debug.Log (ImageData.markerGameObject.name);
				DestroyImmediate (ImageData.markerGameObject, true);
			}

		}*/


	/*	foreach (GameObject objectReference in  setUpImageTarget.allMarkerInitObject)
		{

			Debug.Log (objectReference.name);
			DestroyImmediate (objectReference,true);


		}*/
		//Resources.UnloadUnusedAssets ();
		//System.GC.Collect ();
		/*GlobalStaticVariable.allMarker = new List<ImageTargetInfor>();
		Resources.UnloadUnusedAssets ();
		System.GC.Collect();*/


	}



	public static GameObject LoadAssetBundle (string bundlePath, string bundleFile )
	{
		Debug.Log ("==== bundle file  ==" + bundleFile);
		//AssetBundle currentBuddle = AssetBundle.LoadFromFile (Path.Combine (bundlePath,bundleFile));
		AssetBundle currentBuddle = CCAssetBundleManager.GetAssetBundle(bundlePath, bundleFile);
		string[] objectName = CCAssetBundleManager.ReturnAllAssetNameInBundle (currentBuddle);

		GameObject augmentationObject = (CCAssetBundleManager.GetGameObjectFromAssetBundle(currentBuddle, objectName[0]));
		//Debug.Log ("==== load bundel ==" + currentBuddle.name);
		//objectName = null;
		//UnLoadAssetBundle (bundleFile + GlobalStaticVariable.AssetBundle_EXT);
		//currentBuddle.Unload(true);
		//currentBuddle.Unload (false);
		//CCAssetBundleManager.removeTargetBundleFromDictiionry (bundleFile+ GlobalStaticVariable.AssetBundle_EXT);
		return augmentationObject;
		//return null;

	}

	public static void UnLoadAssetBundle (string bundlename)
	{

		CCAssetBundleManager.Unload (bundlename,false);


	}


}

public class DataSetVariable {


	public DataSet dataSet;
	public string fileName;


}
