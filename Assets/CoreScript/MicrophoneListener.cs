﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class MicrophoneListener : MonoBehaviour
{
	public bool IsWorking = true;
	private bool _lastValueOfIsWorking;

	public bool RaltimeOutput = true;
	private bool _lastValueOfRaltimeOutput = false;
	private string microName = "";

	private AudioSource _audioSource;
	private float _lastVolume = 1f;
	private bool isMicroPhoneStart = false;


	public void MicroListenerStartFunction ()
	{
		_audioSource = GetComponent<AudioSource> ();
	}

	public void MicroPhoneStart()
	{
		//	#if !UNITY_EDITOR
		IsWorking = true;
		RaltimeOutput = true;
		WorkStart();
		//	#endif
	}





	/*public void MicroPhoneUpdate()
	{
	//	#if !UNITY_EDITOR
		if(EveryplayCoreFunction.StartMicroPhone) 
		{
			if (!isMicroPhoneStart) {
				Debug.Log ("Start Micro");
				MicroPhoneStart ();
				isMicroPhoneStart = true;
				CheckIfIsWorkingChanged (true);
				CheckIfRealtimeOutputChanged (true);
			}

		} else {
			if (isMicroPhoneStart) {
				Debug.Log ("Stop Micro");
				CheckIfIsWorkingChanged (false);
				CheckIfRealtimeOutputChanged (false);
				isMicroPhoneStart = false;
			}
	
		}
			//CheckIfIsWorkingChanged();
			//CheckIfRealtimeOutputChanged();

		//#endif
	}*/

	public void CheckIfIsWorkingChanged(bool isWorking)
	{
		//if (_lastValueOfIsWorking != IsWorking)
		//{
			if (IsWorking)
			{
				WorkStart();
			}
			else
			{
				WorkStop();
			}
		//}

		//_lastValueOfIsWorking = IsWorking;
	}

	public void CheckIfRealtimeOutputChanged(bool RaltimeOutput)
	{
		//if (_lastValueOfRaltimeOutput != RaltimeOutput)
		//{
			DisableSound(RaltimeOutput);
		//}

		//_lastValueOfRaltimeOutput = RaltimeOutput;
	}

	public void DisableSound(bool SoundOn)
	{
		if (SoundOn)
		{
			if (_lastVolume > 0)
			{
				_audioSource.volume = _lastVolume;
			}
			else
			{
				_audioSource.volume = 1f;
			}

			Debug.Log ("none muted ");
		}
		else
		{
			_lastVolume = _audioSource.volume;
			_audioSource.volume = 0f;
			Debug.Log ("muted ");
		}
	}

	private void WorkStart()
	{

		#if !UNITY_EDITOR
			foreach (string device in Microphone.devices) {
				microName = device;
			}
			_audioSource.clip = Microphone.Start(microName, true, 10, 44100);
			_audioSource.loop = true;
		while (!(Microphone.GetPosition(microName) > 0))
			{
				_audioSource.Play();
			}
		#endif
		#if UNITY_EDITOR
		_audioSource.clip = Microphone.Start(null, true, 10, 44100);
		_audioSource.loop = true;
		while (!(Microphone.GetPosition(null) > 0))
		{
			_audioSource.Play();
		}
		#endif
	}

	private void WorkStop()
	{
		#if !UNITY_EDITOR
		Microphone.End(microName);
		#endif
		#if UNITY_EDITOR
		Microphone.End(null);
		#endif
	}



}