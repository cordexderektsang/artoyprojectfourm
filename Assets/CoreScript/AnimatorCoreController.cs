﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorCoreController : MonoBehaviour {

	public Animator animation;
	private const string nameForIdle = "IDL";
	private const string nameForActive = "Active";
	private const string nameForDefault = "Default";
	private const string nameForOnClick = "";
	public bool isAnimationFinish = true;
	private float currentAnimtionTime = 0;
	private float timerForStatusChange = 0f;
	private float timeForIDLToBeSwitch ;
	public float timeForDefaultToBeSwitch = 2;
	public float numberOfCycleForIDL = 2;
	private Coroutine waitForStausChange = null;
	public AudioPlayScript audioPlayScriptReference;
	private Coroutine waitForAnimationFinish = null;


	public enum AnimationStatus {IDL, ACTIVE, DEFAULT};

	public AnimationStatus animatorStatus = AnimationStatus.DEFAULT;

	void Start ()
	{


	}

	private void StartAnimationStatus ()
	{
		if (waitForStausChange != null) {
			StopCoroutine (waitForStausChange);
			waitForStausChange = null;
		}
		if (waitForStausChange == null) {
			waitForStausChange = StartCoroutine (WaitForChangeStaus ());
		}

	}




	public void AnimationStartUp(Animator anim , AudioPlayScript audioPlayScript)
	{
		animation = anim;
		audioPlayScriptReference = audioPlayScript;
		timeForIDLToBeSwitch = getAnimationLength (nameForIdle)*numberOfCycleForIDL;
		Debug.Log (timeForIDLToBeSwitch);
		StartAnimationStatus ();
	}

	//set both animation name and variable the  same
	/// <summary>
	/// set the animation parameter.
	/// </summary>
	/// <param name="setTarget">Set target.</param>
	public void GetAnimationParameter (string setTarget)
	{
		if (animation != null && animation.gameObject.activeSelf) {
			AnimatorControllerParameter param;
			for (int i = 0; i < animation.parameters.Length; i++) {
				param = animation.parameters [i];
				if (param.name.Equals (setTarget)) 
				{
					if (param.type == AnimatorControllerParameterType.Bool) 
					{
				
						//Debug.Log (param.name + " to true ");
						//Debug.Log ("current variable set to true: " + param.name);
						if (setTarget.Equals (nameForActive)) {

							if (waitForAnimationFinish == null) {
								Debug.Log ("Start WaitForAnimationFinish");
								animatorStatus = AnimationStatus.ACTIVE;
								//StartCoroutine(GetCurrentClipLength(animation));
								float animationTime = getAnimationLength (param.name);
								animation.SetBool (param.name, true);
								animation.SetTrigger (param.name);
								waitForAnimationFinish = StartCoroutine (WaitForAnimationFinish (param.name, animationTime));
							}
			
						} else {

							animation.SetBool (param.name, true);
							animation.SetTrigger (param.name);
						}
						//	Debug.Log ("Default Bool: " + param.defaultBool);
						/*} else if (param.type == AnimatorControllerParameterType.Float) {
						Debug.Log ("Default Float: " + param.defaultFloat);
					} else if (param.type == AnimatorControllerParameterType.Int) {
						Debug.Log ("Default Int: " + param.defaultInt);
					}*/
					} 
			
				} else {
					//Debug.Log (param.name + " to false ");
					animation.SetBool (param.name, false);
					//Debug.Log ("current variable set to false: " + param.name);
				}
			}
		}
	}

	/// <param name="name">Name.</param>
	IEnumerator WaitForChangeStaus()
	{	timerForStatusChange = getTheLengthAnimationTime (nameForIdle);
		Debug.Log (timerForStatusChange);
		while (true) {

			yield return new WaitForSeconds (timerForStatusChange);
			//AudioListController.instance.StopSoundAndPlaySound ();
			if (animatorStatus == AnimationStatus.IDL && animatorStatus != AnimationStatus.ACTIVE) {
				timerForStatusChange = getTheLengthAnimationTime (nameForDefault );
	
				animatorStatus = AnimationStatus.DEFAULT;
				GetAnimationParameter (nameForDefault);
				//audioPlayScriptReference.GetAudio(this.gameObject.name, animatorStatus.ToString(), true);

			} else if (animatorStatus == AnimationStatus.DEFAULT && animatorStatus != AnimationStatus.ACTIVE) {
				timerForStatusChange = getTheLengthAnimationTime (nameForIdle);
				//timerForStatusChange = timeForIDLToBeSwitch;
	
				animatorStatus = AnimationStatus.IDL;
				GetAnimationParameter (nameForIdle);
				//audioPlayScriptReference.GetAudio(this.gameObject.name, animatorStatus.ToString(),true);
			}
		}
		//this.transform.localScale = new Vector3 (1,1,1);


	}

	float getAnimationLength (string name){
		
		float time = 2;


			RuntimeAnimatorController ac = animation.runtimeAnimatorController; 
		//Get Animator controller
		if (ac != null) {
			for (int i = 0; i < ac.animationClips.Length; i++) { 
				string clipName = ac.animationClips [i].name.Trim ();
		//For all animations
				if (clipName.Contains (name)) {        //If it has the same name as your clip
					time = ac.animationClips [i].length;
			
					if (time == 0) {
						time = timeForDefaultToBeSwitch;
					}
	
					return time;
				}
			}
		}
		return time;
	}

	float getTheLengthAnimationTime (string name){

		float time = 0;


		RuntimeAnimatorController ac = animation.runtimeAnimatorController; 
		//Get Animator controller
		if (ac != null) {
			for (int i = 0; i < ac.animationClips.Length; i++) { 
				string clipName = ac.animationClips [i].name.Trim ();
				//For all animations
				if (/*ac.animationClips [i].length > time &&*/ clipName.Contains (name)) {        //If it has the same name as your clip
					//Debug.Log("---" + clipName + " : " +ac.animationClips [i].length);
					time = ac.animationClips [i].length;

					if (time == 0) {
						time = timeForDefaultToBeSwitch;
					}

					return time;
				}
			}
		}
		return time;

	}

	/// <summary>
	/// Waits for animation finish.
	/// </summary>
	/// <returns>The for animation finish.</returns>
	/// <param name="name">Name.</param>
	IEnumerator WaitForAnimationFinish(string name, float timer)
	{
		isAnimationFinish = false;
		Debug.Log (" waitting Time : " + timer);
		yield return new WaitForSeconds(timer);
		animation.SetBool (name, false);
		animatorStatus = AnimationStatus.IDL;
		timerForStatusChange = 0;
		print("Finish");
		yield return new WaitForSeconds (0.01f);
		audioPlayScriptReference.stopAll ();
		isAnimationFinish = true;
		waitForAnimationFinish = null;
		StartAnimationStatus();
	
	}




	public void CallForAnimation (string name)
	{
		if(animation != null && isAnimationFinish){
			audioPlayScriptReference.stopAll();
			Debug.Log ("Call Animation");
			StopCoroutine (waitForStausChange);
			waitForStausChange = null;
			GetAnimationParameter(nameForActive);
			string status = animatorStatus.ToString ();
			audioPlayScriptReference.GetAudio (name, status,false);
		

		}

	}

}
