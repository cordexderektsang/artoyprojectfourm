using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class EveryplayCoreFunction : MonoBehaviour
{


	public UITexture photoPreview;
	private bool isRecording = false;
	private bool isPaused = false;
	private bool isRecordingFinished = false;
	public  int mode = 0;
	public UIController uIController;
	private string pathForImageCapture; 
	private float timerForRecord = 0 ;
	private float maxTimerForRecord = 60;
	private Texture2D photoReference;
	//public AudioSource[] allAudio;
	public List<GameObject> ListOfCloseUiForVideoRecord;
	public List<AudioClip> onClickAudioClip;
	public AudioSource audioSource;
	public GameObject loadingPage;




	void Awake()
	{

		// DontDestroyOnLoad(gameObject);
		//Debug.Log("reset Every Play");
		//Everyplay.ResetEveryPlayScript();
	}

	void Update ()

	{
		if (isRecording) {
            startRecordTimer ();
			/*if (Input.touchCount > 0)
			{
				RaycastHit hit;
				if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.touches[0].position), out hit, 30000))
				{
					Debug.Log("hit Object");
				}
				else {
					Debug.Log("Stop Record");
					CoreFunctionForVideo();
					timerForRecord = 0;
				}

			}*/
        }


	}

	private void startRecordTimer ()
	{
		if (timerForRecord < maxTimerForRecord) {
			//Debug.Log ("start Time" + timerForRecord);
			timerForRecord += Time.deltaTime;

		} else {
			//Debug.Log ("auto stop from timer");
			CoreFunctionForVideo ();
			timerForRecord = 0;

		}

	}

	void Start()
	{

		//uIController = GetComponent<UIController> ();
		/*  if (uploadStatusLabel != null)
        {
            Everyplay.UploadDidStart += UploadDidStart;
            Everyplay.UploadDidProgress += UploadDidProgress;
            Everyplay.UploadDidComplete += UploadDidComplete;
        }*/
		Everyplay.Initialize ();
		Debug.Log ("start up everyplay");
		Everyplay.RecordingStarted += RecordingStarted;
		Everyplay.RecordingStopped += RecordingStopped;

	
	
	
	}


	void onOffUi (bool status){

		foreach(GameObject ui in  ListOfCloseUiForVideoRecord)
		{
			ui.SetActive (status);
		}

	}

	/*void muteAudio (bool status)
	{
		Debug.Log ("Call mute" + allAudio.Length);
		if (allAudio == null || allAudio.Length == 0) {
			allAudio = CoreAudioSetting.ReturnAllAudio ();
		}
		foreach (AudioSource singleAudio in allAudio){
			Debug.Log (singleAudio.gameObject.name + " : " + this.gameObject.name);
			if (!singleAudio.gameObject.name.Equals (this.gameObject.name)) {
				//Debug.Log (singleAudio.gameObject.name);
				singleAudio.mute = status;
			}
		}

	}*/

	void Destroy()
	{
		/* if (uploadStatusLabel != null)
        {
            Everyplay.UploadDidStart -= UploadDidStart;
            Everyplay.UploadDidProgress -= UploadDidProgress;
            Everyplay.UploadDidComplete -= UploadDidComplete;
        }*/

		Everyplay.RecordingStarted -= RecordingStarted;
		Everyplay.RecordingStopped -= RecordingStopped;

	}

	private void RecordingStarted()
	{
		Debug.Log ("start record " + isRecording);
		isRecording = true;
		isPaused = false;
		isRecordingFinished = false;

	}

	private void RecordingStopped()
	{
		Debug.Log ("stop record " + isRecording);
		isRecording = false;
		isRecordingFinished = true;

	}



	private void UploadDidStart(int videoId)
	{
		// uploadStatusLabel.text = "Upload " + videoId + " started.";
	}

	private void UploadDidProgress(int videoId, float progress)
	{
		//  uploadStatusLabel.text = "Upload " + videoId + " is " + Mathf.RoundToInt((float) progress * 100) + "% completed.";
	}

	private void UploadDidComplete(int videoId)
	{
		// uploadStatusLabel.text = "Upload " + videoId + " completed.";

		//StartCoroutine(ResetUploadStatusAfterDelay(2.0f));
	}

	private IEnumerator ResetUploadStatusAfterDelay(float time)
	{
		yield return new WaitForSeconds(time);
		// uploadStatusLabel.text = "Not uploading";
	}

	#region button Function
	public void StartRecordFunction ()
	{

		if (!isRecording) {
			//StartMicroPhone = true;
			//Everyplay.SetLowMemoryDevice( true );
			Everyplay.SetTargetFPS(20);
			Everyplay.StartRecording ();
			Debug.Log("Start recording function");
			//recordButtom.text = "StopRecordAndShare";
	
		}
		Debug.Log("start record" + isRecording);
		#if UNITY_EDITOR
		Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
		#endif

	}

	public void CoreFunctionForVideo ()
	{
		if (mode == 1) {
			//muteAudio (true);
			onOffUi(false);
			Debug.Log ("on click start record");
			uIController.SwitchToStopMode ();
			timerForRecord = 0;
			mode = 2;
			audioSource.PlayOneShot(onClickAudioClip[2]);
			StartRecordFunction ();
		} else
			if (mode == 2) {
				Debug.Log ("on click stop record");
				//StopRecordAndShareFunction ();
				audioSource.PlayOneShot(onClickAudioClip[3]);
				StopRecord();
				uIController.SwitchToVideo ();
				onOffUi(true);
				mode = 1;
			}else
				if (mode == 0) {
					Debug.Log ("on click take picture");
					audioSource.PlayOneShot(onClickAudioClip[0]);
					takePicture ();
				}
	}


	public void StopRecordFunction ()
	{
		Debug.Log ("stop Recording " + isRecording);
		if (isRecording) {
			//StartMicroPhone = false;
			Everyplay.StopRecording ();
			Debug.Log("finish stop recording function");
			/*if (logicForRecord != null) {
				logicForRecord.SetActive (false);
			}*/
		}
		//muteAudio (false);
		#if UNITY_EDITOR
		Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
		#endif

	}

	void OnApplicationFocus(bool hasFocus)
	{
		if (isRecording) {
			loadingPage.SetActive(true);
			Debug.Log ("reload :" +  hasFocus + "focus");
			Everyplay.StopRecording ();
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
			/*Everyplay.StopRecording ();

			uIController.SwitchToVideo ();
				onOffUi(true);
				mode = 1;*/
		}

	}



	void OnApplicationPause(bool hasFocus)
	{
		if (isRecording) {
			Debug.Log ("Reload :");
			loadingPage.SetActive(true);
			//uIController.SwitchToVideo ();
			//onOffUi(true);
			//mode = 1;
			Everyplay.StopRecording ();
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		
		

		} 
			//Debug.Log ("Pause :" + hasFocus);
			//Debug.Log ("reload :" +  hasFocus + "scene name " + SceneManager.GetActiveScene ().name);

			//SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		//}
	}



	public void StopRecord ()
	{
	Debug.Log("stop and share function finish" + isRecording);
		if (isRecording) {

		//	StartMicroPhone = false;
			Everyplay.StopRecording ();
			Debug.Log("stop and share function finish");
			uIController.TurnOnOffAllUI (false);
			StartCoroutine (TakeSS());
			/*if (logicForRecord != null) {
				logicForRecord.SetActive (false);
			}*/
			//recordButtom.text = "Record";
		}
		#if UNITY_EDITOR
		Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
		#endif

	}

	public void StopRecordAndShareFunction ()
	{

		if (isRecording) {

			//StartMicroPhone = false;
			Everyplay.StopRecording ();
			StartCoroutine(WaitForEndFrameForShareVideo ());
			Debug.Log("stop and share function finish");
		/*	if (logicForRecord != null) {
				logicForRecord.SetActive (false);
			}*/
			//recordButtom.text = "Record";
		}
		#if UNITY_EDITOR
		Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
		#endif

	}

	/*public void PauseRecordFunction ()
	{
		if (isRecording && !isPaused) {
			Everyplay.PauseRecording ();
			isPaused = true;
		}
		#if UNITY_EDITOR
		Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
		#endif

	}

	public void ResumeRecordFunction ()
	{
		if (isRecording && isPaused) {
			Everyplay.ResumeRecording ();
			isPaused = false;
		}
		#if UNITY_EDITOR
		Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
		#endif

	}


	public void ReplayFunction ()
	{
		if (isRecordingFinished) {
			Everyplay.PlayLastRecording ();
			Debug.Log("Replay");
		}
		#if UNITY_EDITOR
		Debug.Log("The video playback is not available in the Unity editor. Please compile and run on a device.");
		#endif

	}*/

	private IEnumerator WaitForEndFrameForShareVideo()
	{

		yield return new WaitForEndOfFrame();
		string path = GetVideoPath();
		Debug.Log (path);
		Debug.Log (" Do file exist : " +File.Exists (path));
		if(!string.IsNullOrEmpty(path))
		{
			NativeShare.Share( path, false, "com.Unicorn.Toys" );
			StartCoroutine(MoveFileMp4 (path , false));
		}
	}
	/// <summary>
	/// share video
	/// </summary>
	/*public void OnClickShareVideo ()
	{
		Debug.Log ("on Click");
		string path = GetVideoPath();
		Debug.Log (path);
		Debug.Log (" Do file exist : " +File.Exists (path));
		if(!string.IsNullOrEmpty(path))
		{
			NativeShare.Share( path, false, "com.Unicorn.Toys" );
			StartCoroutine(MoveFileMp4 (path));
		}

	}*/

	/// <summary>
	/// Takes the picture function
	/// </summary>
	public void takePicture ()
	{
		uIController.TurnOnOffAllUI (false);
		GlobalStaticVariable.IsStarDiplay = false;
	/*	if (logicForRecord != null) {
			logicForRecord.SetActive (true);
		}*/
		StartCoroutine (TakeSSAndShare());
	}
	#endregion 

	#region move file Function
	/// <summary>
	/// Takes the photo.
	/// </summary>
	/// <returns>The SS and share.</returns>
	private IEnumerator TakeSSAndShare()
	{
		yield return new WaitForSeconds (0.02f);
		photoReference = new Texture2D( Screen.width, Screen.height, TextureFormat.RGB24, false );
		photoReference.ReadPixels( new Rect( 0, 0, Screen.width, Screen.height ), 0, 0 );
		photoReference.Apply();
		pathForImageCapture = Path.Combine( Application.persistentDataPath, "shared img.png" );
		File.WriteAllBytes( pathForImageCapture, photoReference.EncodeToPNG() );
		/*if (logicForRecord  != null) {
			logicForRecord.SetActive (false);
		}*/
		//    StartCoroutine(MoveFileImage (filePath));
		GlobalStaticVariable.IsStarDiplay = true;
		DisplayImage ();
	}

	/// <summary>
	/// Takes the photo.
	/// </summary>
	/// <returns.</returns>
	private IEnumerator TakeSS()
	{
		yield return new WaitForSeconds (0.02f);
		photoReference = new Texture2D( Screen.width, Screen.height, TextureFormat.RGB24, false );
		photoReference.ReadPixels( new Rect( 0, 0, Screen.width, Screen.height ), 0, 0 );
		photoReference.Apply();
		photoPreview.mainTexture = photoReference;
		GlobalStaticVariable.IsStarDiplay = true;
		uIController.OnOffPhotoPreview (true);
		uIController.SwitchUiForConfirmpage (mode);

	}
	#region On Click Image Function
	/// <summary>
	/// display image to photo preview object
	/// </summary>
	public void DisplayImage() {

		Texture2D tex = null;
		byte[] fileData;

		if (File.Exists(pathForImageCapture))     {
			//fileData = File.ReadAllBytes(pathForImageCapture);
			//tex = new Texture2D(2, 2);
			//tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
			photoPreview.mainTexture = photoReference;
			uIController.OnOffPhotoPreview (true);
			uIController.SwitchUiForConfirmpage (mode);
		}
		//return tex;
	}

	/// <summary>
	/// Finishs using image save to Album.
	/// </summary>
	public void FinishImage()
	{
		if (mode == 0) {
			StartCoroutine (MoveFileImage (pathForImageCapture));
		} else if (mode == 1){
			Debug.Log ("on Click");
			string path = GetVideoPath();
			Debug.Log (path);
			Debug.Log (" Do file exist : " +File.Exists (path));
			if(!string.IsNullOrEmpty(path))
			{
				//NativeShare.Share( path, false, "com.Unicorn.Toys" );
				StartCoroutine(MoveFileMp4 (path));
			}

		}
	}
	/// <summary>
	/// /share Image To Social
	/// </summary>
	public void ShareImage ()
	{
		if (mode == 0) {
			NativeShare.Share (pathForImageCapture, true, "com.Unicorn.Toys");
			//uIController.OnOffPhotoPreview (false);
			StartCoroutine (MoveFileImage (pathForImageCapture ,false));
		} else if (mode == 1) {
			Debug.Log("sshare function finish");
			StartCoroutine(WaitForEndFrameForShareVideo ());

		}
	}
	#endregion 


	/// <summary>
	/// copy file image to path for local storage.
	/// </summary>
	/// <returns>The file image.</returns>
	/// <param name="filePath">File path.</param>
	private IEnumerator MoveFileImage(string filePath, bool turnon = true)
	{
		yield return new WaitForEndOfFrame();
		//NativeGallery.SaveToGallery (filePath,"Unicorn","mp4",true);
		string name = DateTime.Now.ToString("yyyy_MM_dd HH_mm_ss") + "_Unicorn.png";
		string myFolderLocation = Application.persistentDataPath;
	//	NativeGallery.SaveToGallery (filePath,true);
		#if UNITY_IOS
		//NativeGallery.SaveToGallery(filePath,false);
		//NativeGallery.SaveToGallery (filePath,"Unicorn","jpg",true);
		NativeGallery.SaveToGallery (filePath,"Unicorn","Unicorn{0}.png",true);

		//myFolderLocation = GetiPhoneDocumentsPath();
		//NativeShare.SaveToAblum(filePath);
		#elif UNITY_ANDROID
		Debug.Log("move Image");
		NativeGallery.SaveToGallery (filePath,"Unicorn","Unicorn{0}.png",true);
		//myFolderLocation = GetAndroidPicturePath();
		//Debug.Log ("Image Path : " + myFolderLocation);
		//File.Copy (filePath, myFolderLocation +name);
		#endif
		uIController.TurnOnOffAllUI (turnon);
		if (turnon) {
			uIController.OnOffPhotoPreview (false);
		} else {
			uIController.OnOffPhotoPreview (true);
		}
		//yield return new WaitForEndOfFrame();
		//RefreshGallery(myFolderLocation +name);
	}
	/// <summary>
	/// Moves Video file mp4 to local storage.
	/// </summary>
	/// <returns>The file mp4.</returns>
	/// <param name="filePath">File path.</param>
	private IEnumerator MoveFileMp4(string filePath , bool turnon = true)
	{
		yield return new WaitForEndOfFrame();
		//NativeGallery.SaveToGallery (filePath,"Unicorn","mp4",false);
		//NativeGallery.SaveToGallery (filePath,false);
		string name = DateTime.Now.ToString("yyyy_MM_dd HH_mm_ss") + "_Unicorn.mp4";
		string myFolderLocation = Application.persistentDataPath;
		#if UNITY_IOS
		//NativeGallery.SaveToGallery(filePath,true);
		//NativeGallery.SaveToGallery (filePath,"Unicorn","mp4",true);
		NativeGallery.SaveToGallery (filePath,"Unicorn","Unicorn{0}.mp4",false);

		//myFolderLocation = GetiPhoneDocumentsPath();
		//NativeShare.SaveToAblum(filePath);
		#elif UNITY_ANDROID
		Debug.Log("move Video");
		NativeGallery.SaveToGallery (filePath,"Unicorn","Unicorn{0}.mp4",false);
		//myFolderLocation = GetAndroidVideoPath();
		//Debug.Log ("mp4 Path : " + myFolderLocation);
		//File.Copy (filePath, myFolderLocation +name);
		#endif
		uIController.TurnOnOffAllUI (turnon);
		if (turnon) {
			uIController.OnOffPhotoPreview (false);
		} else {
			uIController.OnOffPhotoPreview (true);
		}
		//yield return new WaitForEndOfFrame();
		//RefreshGallery(myFolderLocation +name);
	}
	#endregion 



	#region path
	/// <summary>
	/// Gets the video path.
	/// </summary>
	/// <returns>The video path.</returns>
	private string GetVideoPath ()
	{
		#if UNITY_IOS

		var root = new DirectoryInfo(Application.persistentDataPath).Parent.FullName;
		var everyplayDir = root + "/tmp/Everyplay/session";

		#elif UNITY_ANDROID
		var root = new DirectoryInfo(Application.temporaryCachePath).FullName;
		//var root = new DirectoryInfo(Application.persistentDataPath).FullName;
		var everyplayDir = root + "/sessions";
		Debug.Log(everyplayDir);
		#endif

		var files = new DirectoryInfo(everyplayDir).GetFiles("*.mp4", SearchOption.AllDirectories);
		var videoLocation = "";

		// Should only be one video, if there is one at all
		foreach (var file in files) {
			#if UNITY_ANDROID
			//videoLocation = "file://" + file.FullName;
			videoLocation = file.FullName;
			#else
			videoLocation = file.FullName;
			#endif
			break;
		}

		return videoLocation;
	}

	/// <summary>
	/// Get the phone documents path.
	/// </summary>
	/// <returns>The phone documents path.</returns>
	/*private string GetiPhoneDocumentsPath()
	{ 
		string path = Application.dataPath.Substring (0, Application.dataPath.Length - 5);
		path = path.Substring(0, path.LastIndexOf('/'));  
		return path + "/Documents";
	}

	/// <summary>
	/// Get android documents path.
	/// </summary>
	/// <returns>The android documents path.</returns>
	private string GetAndroidVideoPath()
	{ 
		//string path = Application.persistentDataPath;
		string path = "/mnt/sdcard/DCIM/Camera/";
		return path;
	}

	private string GetAndroidPicturePath()
	{ 
		//string path = Application.persistentDataPath;
		string path = "/mnt/sdcard/DCIM/Camera/";
		return path;
	}*/

	#endregion 
}