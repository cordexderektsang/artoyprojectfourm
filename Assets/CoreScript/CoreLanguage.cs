﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CCARPlatformFramework.CCDataHandling;
using CCARPlatformFramework.CCTools;

public class CoreLanguage : MonoBehaviour {

	private const string Language = "Language";
	public const string DefaultLanguage = "English";
	public const string FontBond = "-Bold";
	public const string FontRegular = "-Regular";
	public static Font FontTypeBond;
	public static Font FontTypeRegular;
	public static string DefaultFontTypeName = "Montserrat";
	public static UILabel[] label;



	public static string LanguagePref
	{
		get {
			if (CCLocalStorageAPI.Contian (Language)) {
		
				return  CCLocalStorageAPI.GET <string> (Language); 
			} else {
				return "";
			}
		}
		set {
			Debug.Log ("set Language To :" + value);
			CCLocalStorageAPI.SET<string> (Language, value);
		}
	}

	public static void SearchForLanguageBundle(string bundfontName)
	{
		string languageLocalPathDatWithoutFileName = Application.persistentDataPath + ToyCoreDownLoadScript.LanguageFolder + "/";
		AssetBundle currentBuddle = CCAssetBundleManager.GetAssetBundle(languageLocalPathDatWithoutFileName, bundfontName + GlobalStaticVariable.AssetBundle_EXT);
		Debug.Log("===Load Language :====" + bundfontName);
		FontTypeBond = CCAssetBundleManager.GetFontFromAssetBundle(currentBuddle, bundfontName + FontBond);
		FontTypeRegular = CCAssetBundleManager.GetFontFromAssetBundle(currentBuddle, bundfontName + FontRegular);
		//Debug.Log (LanguageController.FontType);
		ChangeLanguage();

	}

	public static void ChangeLanguage(){
		//	string saveLanguagePath = PersistentPath + LanguageFolder + "/" + languageName;
		//	UILabel[] label = FindObjectsOfType(typeof(UILabel)) as UILabel[]); 
		//UILabel[] label = FindObjectsOfTypeAll(typeof(UILabel))as UILabel[];
		//UILabel[] label = Resources.FindObjectsOfTypeAll(typeof(UILabel))as UILabel[];
		Debug.Log ("number of label in scene :" + label.Length + " LanguagePref " + LanguagePref);
		foreach (UILabel font in label) {
			Debug.Log(FontTypeRegular + " : " + font.gameObject.tag + " : "+ font.gameObject.name);
			if (font.gameObject.tag == "Regular" && FontTypeRegular != null)
			{
				font.trueTypeFont = FontTypeRegular;

			}
			else if (font.gameObject.tag == "Blond" && FontTypeBond != null)
			{
				font.trueTypeFont = FontTypeBond;

			}
		}

	}


	public static void ChangeText(string topicName , string cmsText)
	{
		//UILabel[] label = Resources.FindObjectsOfTypeAll(typeof(UILabel)) as UILabel[];
		foreach (UILabel textLabel in label)
		{
			if (textLabel.gameObject.name == topicName )
			{
				//Debug.Log(cmsText + " : " + textLabel.gameObject.name);
				textLabel.text = cmsText;
			}
		
		}

	}

	public static void SearchForLanguagePack()
	{
		//Debug.Log (LoadJson.LocalLanguageJsonDataReference.Count + " " + LanguagePref);
		foreach(LanguageJsonData data in LoadJson.LocalLanguageJsonDataReference)
		{
			if(data.languageName.Equals(LanguagePref)){
				if (data.isLocal) {
					//Debug.Log("Font local Search : " + data.fontPackageBoldName + " : " + data.fontPackageRegularName);
					FontTypeBond = Resources.Load (data.fontPackageBoldName) as Font;
					FontTypeRegular = Resources.Load (data.fontPackageRegularName) as Font;
					//Debug.Log("Font local Search : " + FontTypeBond + " : " + FontTypeRegular);
					ChangeLanguage ();
				} else {
					//Debug.Log("Search For Bundle" + data.fontPackageName + " end " );
					SearchForLanguageBundle(data.fontPackageName);
				}
				break;
			}

		}
	}

}
