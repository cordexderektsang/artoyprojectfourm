﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CCARPlatformFramework.CCConstants;
using CCARPlatformFramework.CCDataHandling;
using CCARPlatformFramework.CCTools;

public class DownLoadScript : MonoBehaviour {

	public string downLoadURL ="";
	public bool isNetworkError = false;
	public string path = "";
	public string fileName = "";
	public string bundlePath = "";
	public string bundleFile = "";

	public void DownloadExampleFunction ()
	{

		string downloadURL = downLoadURL;
		Debug.Log (downloadURL);
		CCDownloadController downloadController = CCDownloadController.Instance;
		downloadController.DownloadSingleFile (downloadURL, CCLoadDataManager.DownloadProgress, (WWW www) => {

			if(string.IsNullOrEmpty(www.error))
			{
				CCWriteDataManager.WriteToPath (path, fileName, www.bytes);

				www.Dispose ();
				isNetworkError = false;

			}
			else
			{
				Debug.Log("error");
				isNetworkError = true;
			}



		},
			() =>
			{
				Debug.Log("fail Download");
				isNetworkError = true;

			});

	}

	public void LoadAssetBundle ()
	{

		AssetBundle currentBuddle = CCAssetBundleManager.GetAssetBundle(bundlePath, bundleFile);
		string objectName = "";
		GameObject augmentationObject = (CCAssetBundleManager.GetGameObjectFromAssetBundle(currentBuddle, objectName));
	
	}
}
