﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NetworkCoreCheckHandler : MonoBehaviour
{

	//public string errorPopUpObjectName = "";
	public float moveGap = 100;
	public float moveSpeed = 0.1f;
	public  GameObject errorPopup = null;
	private static UIButton errorPopUpButton = null;
	private float yPoistion ;
	public static bool isNetworkError = false;
	public ToyCoreDownLoadScript toyCoreDownLoadScript;
	public ButtomFlow buttonFlowScript;
	public static  NetworkCoreCheckHandler NetWorkInstance;

	private float endyPosition;

	//public delegate void NetworkErrorCallbackDelegate(string trackableName, List<string> failedDownloadObjects);
	//public NetworkErrorCallbackDelegate NetworkErrorCallbackAction;

	void Awake ()
	{
		//SetNetworkErrorCallbackAction();
		NetWorkInstance = this;

			//errorPopup.SetActive (false);
			yPoistion = errorPopup.transform.localPosition.y;
			endyPosition = yPoistion - moveGap; 

		//}
	}

	public void Update ()
	{
		if (!GlobalStaticVariable.finishDownload && toyCoreDownLoadScript.isDownloadStart) {
			if (!isNetworkError) {
				
			//	moveUp ();
			} else {
				//moveDown ();
				if (buttonFlowScript.downloadLangaugePopUp.activeSelf)
				{
					buttonFlowScript.downloadLangaugePopUp.SetActive(false);
				}
				errorPopup.SetActive(true);
			}
			//CheckInternet ();
		}
	}

	public void moveDown ()
	{
		if (errorPopup.transform.localPosition.y > endyPosition) {

			errorPopup.transform.localPosition = new Vector3 (errorPopup.transform.localPosition.x, errorPopup.transform.localPosition.y - moveSpeed);

		} else {
			errorPopup.transform.localPosition = new Vector3 (errorPopup.transform.localPosition.x, endyPosition);

		}
	}



	public void moveUp ()
	{
		if (errorPopup.transform.localPosition.y < yPoistion) {

			errorPopup.transform.localPosition = new Vector3 (errorPopup.transform.localPosition.x, errorPopup.transform.localPosition.y + moveSpeed);

		} else {

			errorPopup.transform.localPosition = new Vector3 (errorPopup.transform.localPosition.x, yPoistion);
		}
	}


	/*public void SetNetworkErrorCallbackAction()
		{
			if (NetworkErrorCallbackAction == null)
			{
				NetworkErrorCallbackAction = NetworkErrorCallback;
			}
		}*/

	/*public void NetworkErrorCallback(string trackableName, List<string> failedDownloadObjects)
		{
			Debug.Log("Network error!");
			//isNetworkError = true;
			failedDownloadObjects.Add(trackableName);

ßß
		}*/


	public static void startNetworkCheck ()
	{
		NetWorkInstance.StartCoroutine("checkForNetwork");
	}

	public  IEnumerator checkForNetwork ()
	{
		string url = toyCoreDownLoadScript.downLoadURL + "/" + "Vaildation" + GlobalStaticVariable.TXT_EXT;
		Debug.Log ("===== Network Vaildation check ====");
		WWW www = new WWW(url);
		yield return www;
		if (string.IsNullOrEmpty (www.error)) {
			if (www.text.Equals ("Vaildation")) {
				Debug.Log ("Fine Network");
				isNetworkError = false;
			} else {
				isNetworkError = true;
				Debug.Log (" no Fine Network");

			}

		} else {
			isNetworkError = true;
			Debug.Log (" no Fine Network");

		}




	}

	public static bool CheckInternet ()
	{
		NetworkReachability networkStatus = Application.internetReachability;
		if (networkStatus.Equals (NetworkReachability.NotReachable) ) {
			isNetworkError = true;
			return false;
		}
		isNetworkError = false;
		return true;
	}

	/*public static void DisableNetworkPrompt ()
	{
		if (errorPopup != null) {
			errorPopup.SetActive (false);
		} else {

			Debug.Log ("can't find error Pop up Object");
		}
	}*/

	public static void AddOnClickFunctionToPrompt(MonoBehaviour Script , object [] Parameter  , string methodName)
	{
		if (errorPopUpButton != null && Script!=null && !string.IsNullOrEmpty(methodName)) {
			CoreNguiButtonAddAndRemove.AddParameterFunction (Script, Parameter, errorPopUpButton, methodName);
		} else {
			Debug.Log ("faill to add function");

		}

	}

	public static void RemoveOnClickFunctionToPrompt(MonoBehaviour Script , object [] Parameter , string methodName)
	{
		if (errorPopUpButton != null && Script!=null && !string.IsNullOrEmpty(methodName)) {
			CoreNguiButtonAddAndRemove.RemoveParameterFunction (Script, Parameter, errorPopUpButton, methodName);
		} else {
			Debug.Log ("faill to add function");

		} 
	}
}

